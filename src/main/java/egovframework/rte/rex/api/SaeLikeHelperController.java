package egovframework.rte.rex.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.LikeHelperVO;
import egovframework.rte.rex.service.ResultVO;
import egovframework.rte.rex.service.SaeHelperService;
import egovframework.rte.rex.service.SaeLikeHelperService;
import egovframework.rte.rex.service.SaeUserService;
import egovframework.rte.rex.service.UserVO;

/**
 * 선호 도우미 정보를 관리하는 컨트롤러 클래스를 정의한다.
 * @see
 * 151023 | KSM | Create
 */

@Controller
public class SaeLikeHelperController{
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeLikeHelperController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** common */
	public TokenManager tokenMng = new TokenManager();
	
	/** likeHeplerService */
	@Resource(name="likeHelperService")
	private SaeLikeHelperService likeHelperService;
	/** userService */
	@Resource(name="userService")
	private SaeUserService userService;
	/** helperService */
	@Resource(name="helperService")
	private SaeHelperService helperService;
	
	/** 
	 * Like Helper 목록
	 */
	@RequestMapping(value="/springrest/api/like/helper", method=RequestMethod.GET)
	public void likHelperList(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="user_idx", required=false) String userIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Like Helper List ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER")){//USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Like Helper List ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Like Helper List ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					if(userIdx == null){
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1000000008);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1000000008);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Like Helper List ] Fail " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						List<LikeHelperVO> userlikeHelperList = likeHelperService.findLikeHelperByUser(userIdx);
						
						List likeHelperList = new ArrayList();
						
						for( int i=userlikeHelperList.size()-1; i >=0; i--){
							LikeHelperVO originLikeHelper = userlikeHelperList.get(i);
							
							HashMap<String, Object> tempLikeHelper = new HashMap<String, Object>();	//data set
							
							tempLikeHelper.put("idx", originLikeHelper.getIdx());
							tempLikeHelper.put("helper_name", originLikeHelper.getHelper().getName());
							tempLikeHelper.put("helper_nickname", originLikeHelper.getHelper().getNickname());
							tempLikeHelper.put("helper_like_num", originLikeHelper.getHelper().getLikeNum());
							
							likeHelperList.add(tempLikeHelper);
						}
						
						//set data
						data.put("likeHelperList", likeHelperList);
						
						//set result
						result.setRes(true);
						result.setStatus(200);
						result.setCode(1000200003);
						if(isDebugEnabled){
							result.setStatusMsg(200);
							result.setCodeMsg(1000200003);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Like Helper List ] OK " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Like Helper List ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Like Helper List ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * Like Helper 추가 - 시각 장애인이 도우미 통화 후 좋아요 누르는 순간
	 */
	@RequestMapping(value="/springrest/api/like/helper", method=RequestMethod.POST)
	public void likHelperAdd(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="user_idx", required=false) String userIdx,
			@RequestParam(value="helper_idx", required=false) String helperIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Like Helper Add ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER")){//USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Like Helper Add ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Like Helper Add ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					UserVO userVO = new UserVO();
					userVO.setIdx(userIdx);
					UserVO oldUser = userService.getUser(userVO);//get user
					
					HelperVO helperVO = new HelperVO();
					helperVO.setIdx(helperIdx);
					HelperVO oldHelper = helperService.getHelper(helperVO);//get helper
					
					if(oldUser.getIdx() == null){//user 미등록
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1200200001);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1200200001);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Like Helper Add ] FAIL " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else if(oldHelper.getIdx() == null){//helper 미등록
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1100200001);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1100200001);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Like Helper Add ] FAIL " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						LikeHelperVO oldLikeHelper = likeHelperService.findLikeHelper(userIdx, helperIdx);
						
						if(oldLikeHelper.getIdx() == null){//선호 도우미로 미등록 상태
							
							int likeNum = oldHelper.getLikeNum();
							likeNum = likeNum + 1;//좋아요 1 증가
							
							oldHelper.setLikeNum(likeNum);
							
							helperService.updateHelper(oldHelper);//도우미 좋아요 업데이트
							
							LikeHelperVO newLikeHelperVO = new LikeHelperVO();
							
							newLikeHelperVO.setCreatedDate(new Date());
							newLikeHelperVO.setHelperFk(helperIdx);
							newLikeHelperVO.setUserFk(userIdx);
							newLikeHelperVO.setUser(oldUser);
							newLikeHelperVO.setHelper(oldHelper);
							
							String no = likeHelperService.insertLikeHelper(newLikeHelperVO);//선호 도우미 등록
							
							//set result
							result.setRes(true);
							result.setStatus(200);
							result.setCode(1000200001);
							if(isDebugEnabled){
								result.setStatusMsg(200);
								result.setCodeMsg(1000200001);
							}
							
							//set data
							data.put("like_helper_idx", no);
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Like Helper Add ] OK : " + accessType);
								logger.debug("=============================================================");
							}

						}
						else{//이미 선호 도우미로 등록된 상태
							
							//set result
							result.setRes(false);
							result.setStatus(400);
							result.setCode(1000200002);
							if(isDebugEnabled){
								result.setStatusMsg(400);
								result.setCodeMsg(1000200002);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Like Helper Add ] FAIL " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Like Helper Add ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Like Helper Add ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * Like Helper 수정
	 */
	@RequestMapping(value="/springrest/api/like/helper/{helper_idx}", method=RequestMethod.PUT)
	public void likHelperMod(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@PathVariable("helper_idx") String helperIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Like Helper Mod ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER") || accessType.equals("HELPER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Like Helper Mod ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Like Helper Mod ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Like Helper Mod ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Like Helper Mod ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
}