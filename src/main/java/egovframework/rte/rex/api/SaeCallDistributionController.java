package egovframework.rte.rex.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.LikeHelperVO;
import egovframework.rte.rex.service.ResultVO;
import egovframework.rte.rex.service.SaeHelperService;
import egovframework.rte.rex.service.SaeLikeHelperService;
import egovframework.rte.rex.service.SaeSystemService;
import egovframework.rte.rex.service.SaeUserService;
import egovframework.rte.rex.service.SystemVO;

/**
 * 콜 분배 정보를 관리하는 컨트롤러 클래스를 정의한다.
 * @see
 * 151023 | KSM | Create
 */

@Controller
public class SaeCallDistributionController{
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeCallDistributionController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** common */ 
	public TokenManager tokenMng = new TokenManager();
	
	/** systemService */
	@Resource(name="systemService")
	private SaeSystemService systemService;
	/** likeHeplerService */
	@Resource(name="likeHelperService")
	private SaeLikeHelperService likeHelperService;
	/** userService */
	@Resource(name="userService")
	private SaeUserService userService;
	/** helperService */
	@Resource(name="helperService")
	private SaeHelperService helperService;
	
	/** 
	 * 호분배 경과를 목록
	 */
	@RequestMapping(value="/springrest/api/call", method=RequestMethod.GET)
	public void callList(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="user_idx", required=false) String userIdx,
			@RequestParam(value="system_idx", required=false, defaultValue="SYS-00000000001") String systemIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Call Distribute ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER")){//USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Call Distribute ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Call Distribute ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else{//valid token
					
					if(userIdx == null){
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1000000008);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1000000008);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Call Distribute ] FAIL : " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						List likeHelperList = new ArrayList();
						List helperList = new ArrayList(); 
						List agentList = new ArrayList();
						
						//confirm config rule
						SystemVO vo = new SystemVO();
						vo.setIdx(systemIdx);
						
						SystemVO oldSystem = systemService.getSystem(vo);
						
						int likeHelperLimit = oldSystem.getLikeHelperNum();
						int helperLimit = oldSystem.getHelperNum();
						int agentLimit = oldSystem.getAgentNum();
						
						List<LikeHelperVO> userLikeHelperList = likeHelperService.findCallLikeHelper(userIdx, likeHelperLimit);
						
						for( int i=userLikeHelperList.size()-1; i >=0; i--){
							
							LikeHelperVO originLikeHelper = userLikeHelperList.get(i);
							
							HashMap<String, Object> tempLikeHelper = new HashMap<String, Object>();	//data set
							
							tempLikeHelper.put("idx", originLikeHelper.getHelper().getIdx());
							tempLikeHelper.put("helper_name", originLikeHelper.getHelper().getName());
							tempLikeHelper.put("helper_nickname", originLikeHelper.getHelper().getNickname());
							
							likeHelperList.add(tempLikeHelper);
							
						}
						
						//set like helper list
						data.put("like_helper_list", likeHelperList);
						
						int matchRule = oldSystem.getRoutingRule();
						//matchRule = 1;//hard coding for TEST
						
						List tempHelperList = new ArrayList();
						
						Gson gson = new Gson();
						
						if(matchRule == SaeConfig.RULE_RANDOM){
							
							tempHelperList = helperService.findHelperByRule0(userIdx, helperLimit);
							
							for( int i=tempHelperList.size()-1; i >=0; i--){
								
								HelperVO originHelper = (HelperVO) tempHelperList.get(i);
								HashMap<String, Object> tempHelper = new HashMap<String, Object>();	//data set
								
								tempHelper.put("idx", originHelper.getIdx());
								tempHelper.put("helper_name", originHelper.getName());
								tempHelper.put("helper_nickname", originHelper.getNickname());
								
								helperList.add(tempHelper);
								
							}
							
						}
						else{
							
							switch(matchRule){
							
								case SaeConfig.RULE_TODAY_CONN_NUM_MIN_HELPER:	tempHelperList = helperService.findHelperByRule1(userIdx, helperLimit);		break;	//RULE 1
								case SaeConfig.RULE_TODAY_CONN_NUM_MAX_HELPER:	tempHelperList = helperService.findHelperByRule2(userIdx, helperLimit);		break;
								case SaeConfig.RULE_TODAY_CONN_TIME_MIN_HELPER:	tempHelperList = helperService.findHelperByRule3(userIdx, helperLimit);		break;
								case SaeConfig.RULE_TODAY_CONN_TIME_MAX_HELPER:	tempHelperList = helperService.findHelperByRule4(userIdx, helperLimit);		break;
								case SaeConfig.RULE_TODAY_WAIT_TIME_MAX_HELPER:	tempHelperList = helperService.findHelperByRule5(userIdx, helperLimit);		break;
								case SaeConfig.RULE_TODAY_LIKE_MAX_HELPER:		tempHelperList = helperService.findHelperByRule6(userIdx, helperLimit);		break;
								case SaeConfig.RULE_TOTAL_CONN_NUM_MIN_HELPER:	tempHelperList = helperService.findHelperByRule7(userIdx, helperLimit);		break;
								case SaeConfig.RULE_TOTAL_CONN_NUM_MAX_HELPER:	tempHelperList = helperService.findHelperByRule8(userIdx, helperLimit);		break;
								case SaeConfig.RULE_TOTAL_CONN_TIME_MIN_HELPER:	tempHelperList = helperService.findHelperByRule9(userIdx, helperLimit);		break;
								case SaeConfig.RULE_TOTAL_CONN_TIME_MAX_HELPER:	tempHelperList = helperService.findHelperByRule10(userIdx, helperLimit);	break;
								case SaeConfig.RULE_TOTAL_WAIT_TIME_MAX_HELPER:	tempHelperList = helperService.findHelperByRule11(userIdx, helperLimit);	break;
								case SaeConfig.RULE_TOTAL_LIKE_MAX_HELPER:		tempHelperList = helperService.findHelperByRule12(userIdx, helperLimit);	break;
								
							}
							
							for( int i=tempHelperList.size()-1; i >=0; i--){
								
								Object originHelper = tempHelperList.get(i);
								HashMap<String, Object> tempHelper = new HashMap<String, Object>();	//data set
								
								String originHelperJsonString = gson.toJson(originHelper);
								
								JsonArray originHelperJsonArray = gson.fromJson(originHelperJsonString, JsonElement.class).getAsJsonArray();
								JsonObject helperObject = originHelperJsonArray.get(0).getAsJsonObject();
								
								tempHelper.put("idx", helperObject.get("idx").getAsString());
								tempHelper.put("helper_name", helperObject.get("name").getAsString());
								tempHelper.put("helper_nickname", helperObject.get("nickname").getAsString());
								
								helperList.add(tempHelper);
								
							}
							
						}
						
						//set like helper list
						data.put("helper_list", helperList);
						
						List userAgentList = helperService.agentList(agentLimit);
						
						for( int i=userAgentList.size()-1; i >=0; i--){
							
							HelperVO originAgent = (HelperVO) userAgentList.get(i);
							
							HashMap<String, Object> tempAgent = new HashMap<String, Object>();	//data set
							
							tempAgent.put("idx", originAgent.getIdx());
							tempAgent.put("helper_name", originAgent.getName());
							tempAgent.put("helper_nickname", originAgent.getNickname());
							
							agentList.add(tempAgent);
						}
						
						//set agent list
						data.put("agent_list", agentList);
						
						//set result
						result.setRes(true);
						result.setStatus(200);
						result.setCode(1000500001);
						if(isDebugEnabled){
							result.setStatusMsg(200);
							result.setCodeMsg(1000500001);
						}
						
						
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("RULE : " + matchRule);
							logger.debug("=============================================================");
							
							logger.debug("=============================================================");
							logger.debug("[ Call Distribute ] OK : " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Call Distribute ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Call Distribute ] END : " + accessType);
			logger.debug("=============================================================");
		}
		
	}
	
}