package egovframework.rte.rex.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;
import egovframework.rte.rex.service.FamilyVO;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.ResultVO;
import egovframework.rte.rex.service.SaeFamilyService;
import egovframework.rte.rex.service.SaeHelperService;
import egovframework.rte.rex.service.SaeUserService;
import egovframework.rte.rex.service.UserVO;

/**
 * 사용자 가족 정보를 관리하는 컨트롤러 클래스를 정의한다.
 * @see
 * 151023 | KSM | Create
 */

@Controller
public class SaeFamilyController{
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeFamilyController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** common */
	public TokenManager tokenMng = new TokenManager();
	
	/** familyService */
	@Resource(name="familyService")
	private SaeFamilyService familyService;
	/** userService */
	@Resource(name="userService")
	private SaeUserService userService;
	/** helperService */
	@Resource(name="helperService")
	private SaeHelperService helperService;
	
	/** 
	 * Family 목록
	 */
	@RequestMapping(value="/springrest/api/family", method=RequestMethod.GET)
	public void familyList(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="user_idx", required=false) String userIdx,
			@RequestParam(value="helper_idx", required=false) String helperIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Family List ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER") || accessType.equals("HELPER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Family List ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Family List ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else{//valid token
					
					if((userIdx == null) && (helperIdx == null)){
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1000000008);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1000000008);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Family List ] Fail " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						//set result
						result.setRes(true);
						result.setStatus(200);
						result.setCode(1000100003);
						if(isDebugEnabled){
							result.setStatusMsg(200);
							result.setCodeMsg(1000100003);
						}
						
						if(accessType.equals("USER")){
							List<FamilyVO> userFamilyList = familyService.findFamilyByUser(userIdx);
							
							List waitList = new ArrayList();
							List familyList = new ArrayList();
							
							for( int i=userFamilyList.size()-1; i >=0; i--){
								FamilyVO originFamily = userFamilyList.get(i);
								
								HashMap<String, Object> tempFamily = new HashMap<String, Object>();	//data set
								
								tempFamily.put("idx", originFamily.getIdx());
								tempFamily.put("helper_idx", originFamily.getHelper().getIdx());
								tempFamily.put("helper_name", originFamily.getHelper().getName());
								tempFamily.put("helper_nickname", originFamily.getHelper().getNickname());
								tempFamily.put("user_idx", originFamily.getUser().getIdx());
								tempFamily.put("user_name", originFamily.getUser().getName());
								tempFamily.put("user_nickname", originFamily.getUser().getNickname());
								tempFamily.put("request_type", originFamily.getRequestType());
								tempFamily.put("is_Family", originFamily.isIsfamily());//가족 수락 여부
								
								if( (!originFamily.getRequestType().equals("USER")) && (!originFamily.isIsfamily()) ){//요청 받은 리스트 중에서 수락 대기중인 목록
									waitList.add(tempFamily);
								}
								else{
									familyList.add(tempFamily);
								}
							}
							
							//set data
							data.put("wait_list", waitList);
							data.put("family_list", familyList);
							
						} 
						else if(accessType.equals("HELPER")){//Helper의 가족 리스트 중에서
							List<FamilyVO> helperFamilyList = familyService.findFamilyByHelper(helperIdx);
							
							List waitList = new ArrayList();
							List familyList = new ArrayList();
							
							for( int i=helperFamilyList.size()-1; i >=0; i--){
								FamilyVO originFamily = helperFamilyList.get(i);
								
								HashMap<String, Object> tempFamily = new HashMap<String, Object>();	//data set
								
								tempFamily.put("idx", originFamily.getIdx());
								tempFamily.put("helper_idx", originFamily.getHelper().getIdx());
								tempFamily.put("helper_name", originFamily.getHelper().getName());
								tempFamily.put("helper_nickname", originFamily.getHelper().getNickname());
								tempFamily.put("user_idx", originFamily.getUser().getIdx());
								tempFamily.put("user_name", originFamily.getUser().getName());
								tempFamily.put("user_nickname", originFamily.getUser().getNickname());
								tempFamily.put("request_type", originFamily.getRequestType());
								tempFamily.put("is_Family", originFamily.isIsfamily());//가족 수락 여부
								
								if( (!originFamily.getRequestType().equals("HELPER")) && (!originFamily.isIsfamily()) ){//요청 받은 리스트 중에서 수락 대기중인 목록
									waitList.add(tempFamily);
								}
								else{
									familyList.add(tempFamily);
								}
							}
							
							//set data
							data.put("wait_list", waitList);
							data.put("family_list", familyList);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Family List ] OK " + accessType);
							logger.debug("=============================================================");
						}
						
					}//end of : 필수 Param
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Family List ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Family List ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * Family 가족 요청
	 */
	@RequestMapping(value="/springrest/api/family", method=RequestMethod.POST)
	public void familyAdd(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="user_idx", required=false) String userIdx,
			@RequestParam(value="helper_idx", required=false) String helperIdx,
			@RequestParam(value="phone", required=false) String phone,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Family Add ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER") || accessType.equals("HELPER")){//HELPER or USER 경우 접근 절차
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Family Add ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Family Add ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					if(phone == null){
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1000000008);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1000000008);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Family Add ] Fail " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						HelperVO oldHelper = new HelperVO();
						UserVO oldUser = new UserVO();
						
						if(accessType.equals("USER")){
							
							oldHelper = helperService.findHelperByPhone(phone);
							
							UserVO userVO = new UserVO();
							userVO.setIdx(userIdx);
							
							oldUser = userService.getUser(userVO);//get user
							
						}
						else if(accessType.equals("HELPER")){
							
							oldUser = userService.findUserByPhone(phone);
							
							HelperVO helperVO = new HelperVO();
							helperVO.setIdx(helperIdx);
							
							oldHelper = helperService.getHelper(helperVO);//get helper
							
						}
						
						if(oldHelper.getIdx() == null){
							
							//set result
							result.setRes(false);
							result.setStatus(400);
							result.setCode(1100200001);
							if(isDebugEnabled){
								result.setStatusMsg(400);
								result.setCodeMsg(1100200001);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Family Add ] Fail " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else if(oldUser.getIdx() == null){
							
							//set result
							result.setRes(false);
							result.setStatus(400);
							result.setCode(1200200001);
							if(isDebugEnabled){
								result.setStatusMsg(400);
								result.setCodeMsg(1200200001);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Family Add ] Fail " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else{
							
							userIdx = oldUser.getIdx();
							helperIdx = oldHelper.getIdx();
							
							FamilyVO oldFamily = familyService.findFamily(userIdx, helperIdx);
							
							if(oldFamily.getIdx() == null){// 등록된 가족 요청이 없을 경우
								
								
								if(oldUser.getIdx() == null){//user 미등록
									
									//set result
									result.setRes(false);
									result.setStatus(400);
									result.setCode(1200200001);
									if(isDebugEnabled){
										result.setStatusMsg(400);
										result.setCodeMsg(1200200001);
									}
									
									if(isDebugEnabled){
										logger.debug("=============================================================");
										logger.debug("[ Family Add ] FAIL " + accessType);
										logger.debug("=============================================================");
									}
									
								}
								else if(oldHelper.getIdx() == null){//helper 미등록
									
									//set result
									result.setRes(false);
									result.setStatus(400);
									result.setCode(1100200001);
									if(isDebugEnabled){
										result.setStatusMsg(400);
										result.setCodeMsg(1100200001);
									}
									
									if(isDebugEnabled){
										logger.debug("=============================================================");
										logger.debug("[ Family Add ] FAIL " + accessType);
										logger.debug("=============================================================");
									}
									
								}
								else if(!oldHelper.isRegisterStatus()){//helper 승인 상태가 아닐때
									
									//set result
									result.setRes(false);
									result.setStatus(401);
									result.setCode(1100200002);
									if(isDebugEnabled){
										result.setStatusMsg(401);
										result.setCodeMsg(1100200002);
									}
									
									if(isDebugEnabled){
										logger.debug("=============================================================");
										logger.debug("[ Family Add ] FAIL " + accessType);
										logger.debug("=============================================================");
									}
									
								}
								else{//user와 helper 모두 등록 되어 있을때
									
									FamilyVO newFamily = new FamilyVO();
									
									newFamily.setCreatedDate(new Date());
									newFamily.setUserFk(userIdx);
									newFamily.setHelperFk(helperIdx);
									newFamily.setRequestType(accessType);
									newFamily.setIsfamily(false);//수락 여부 ( false : 대기, true : 수락 )
									newFamily.setUser(oldUser);
									newFamily.setHelper(oldHelper);
									
									String no = familyService.insertFamily(newFamily);
									
									//set result
									result.setRes(true);
									result.setStatus(200);
									result.setCode(1000100002);
									if(isDebugEnabled){
										result.setStatusMsg(200);
										result.setCodeMsg(1000100002);
									}
									
									//set data
									data.put("family_idx", no);
									
									if(isDebugEnabled){
										logger.debug("=============================================================");
										if(accessType.equals("USER"))
											logger.debug("[ Family Add ] Request OK ( USER -> HELPER ) : " + accessType);
										else
											logger.debug("[ Family Add ] Request OK ( HELPER -> USER ) : " + accessType);
										logger.debug("=============================================================");
									}
									
								}
								
							}
							else{// 등록된 가족 요청이 이미 존재 할 경우
								
								//set result
								result.setRes(false);
								result.setStatus(503);
								result.setCode(1000100001);
								if(isDebugEnabled){
									result.setStatusMsg(503);
									result.setCodeMsg(1000100001);
								}
								
								if(isDebugEnabled){
									logger.debug("=============================================================");
									logger.debug("[ Family Add ] DENY : " + accessType);
									logger.debug("=============================================================");
								}
								
							}//end of : 가족 요청 등록 여부
							
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Family Add ] DENY " + accessType);
				logger.debug("=============================================================");
			}
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Family Add ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * Family 가족 수락
	 */
	@RequestMapping(value="/springrest/api/family/{family_idx}", method=RequestMethod.PUT)
	public void familyMod(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@PathVariable("family_idx") String familyIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Family Mod ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER") || accessType.equals("HELPER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Family Mod ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000003);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000003);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Family Mod ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					FamilyVO vo = new FamilyVO();
					vo.setIdx(familyIdx);
					
					FamilyVO familyVO = familyService.getFamily(vo);
					familyVO.setIsfamily(true);//수락 여부 ( false : 대기, true : 수락 )
					
					familyService.updateFamily(familyVO);
					
					//set result
					result.setRes(true);
					result.setStatus(200);
					result.setCode(1000100004);
					
					if(isDebugEnabled){
						result.setStatusMsg(200);
						result.setCodeMsg(1000100004);
					}
					
					//set data
					data.put("family_idx", familyIdx);
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Family Mod ] OK : " + accessType);
						logger.debug("=============================================================");
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Family Mod ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Family Mod ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * Family 가족 거절
	 */
	@RequestMapping(value="/springrest/api/family/{family_idx}", method=RequestMethod.DELETE)
	public void familyDel(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@PathVariable("family_idx") String familyIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Family Delete ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER") || accessType.equals("HELPER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Family Delete ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Family Delete ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					FamilyVO vo = new FamilyVO();
					vo.setIdx(familyIdx);
					
					FamilyVO familyVO = familyService.getFamily(vo);
					familyService.deleteFamily(familyVO);
					
					//set result
					result.setRes(true);
					result.setStatus(200);
					result.setCode(1000100005);
					
					if(isDebugEnabled){
						result.setStatusMsg(200);
						result.setCodeMsg(1000100005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Family Delete ] OK : " + accessType);
						logger.debug("=============================================================");
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Family Delete ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Family Delete ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
}