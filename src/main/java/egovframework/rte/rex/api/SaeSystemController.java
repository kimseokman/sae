package egovframework.rte.rex.api;

import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;
import egovframework.rte.rex.service.ResultVO;
import egovframework.rte.rex.service.SaeSystemService;
import egovframework.rte.rex.service.SystemVO;

/**
 * 시스템 정보를 관리하는 컨트롤러 클래스를 정의한다.
 * @see
 * 151021 | KSM | Create
 */

@Controller
public class SaeSystemController{
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeSystemController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** common */
	public TokenManager tokenMng = new TokenManager();
	
	/** systemService */
	@Resource(name="systemService")
	private SaeSystemService systemService;
	
	/** 
	 * System 정보 얻기
	 */
	@RequestMapping(value="/springrest/api/system", method=RequestMethod.GET)
	public void helperSignSMS(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="system_idx", required=false, defaultValue="SYS-00000000001") String systemIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SYSTEM ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("HELPER") || accessType.equals("USER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ SYSTEM ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SYSTEM ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					SystemVO vo = new SystemVO();
					vo.setIdx(systemIdx);
					
					SystemVO oldSystem = systemService.getSystem(vo);
					
					data.put("sip_ip", oldSystem.getSipIp());
					data.put("sip_port", oldSystem.getSipPort());
					data.put("sip_codec", oldSystem.getSipCodec());
					data.put("sip_framerate", oldSystem.getSipFramerate());
					data.put("sip_bitrate", oldSystem.getSipBitrate());
					data.put("sip_resolution", oldSystem.getSipResolution());
					
					//set result
					result.setRes(true);
					result.setStatus(200);
					result.setCode(1000300001);
					if(isDebugEnabled){
						result.setStatusMsg(200);
						result.setCodeMsg(1000300001);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SYSTEM ] OK : " + accessType);
						logger.debug("=============================================================");
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ SYSTEM ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		//add result
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SYSTEM ] END : " + accessType);
			logger.debug("=============================================================");
		}
		
	}
	
}