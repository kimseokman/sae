package egovframework.rte.rex.api;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.ResultVO;
import egovframework.rte.rex.service.SMSVO;
import egovframework.rte.rex.service.SaeCallHistoryService;
import egovframework.rte.rex.service.SaeHelperService;
import egovframework.rte.rex.service.SaeSMSService;
import egovframework.rte.rex.service.SaeSystemService;
import egovframework.rte.rex.service.SystemVO;

/**
 * 도우미 정보를 관리하는 컨트롤러 클래스를 정의한다.
 * @see
 * 151111 | KSM | Create
 */

@Controller
public class SaeHelperController {
	/** Logger */
	private Logger logger = Logger.getLogger(SaeHelperController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** common */
	public TokenManager tokenMng = new TokenManager();
	
	/** helperService */
	@Resource(name="helperService")
	private SaeHelperService helperService;
	/** callHistoryService */
	@Resource(name="callHistoryService")
	private SaeCallHistoryService callHistoryService;
	/** systemService */
	@Resource(name="systemService")
	private SaeSystemService systemService;
	/** smsService */
	@Resource(name="smsService")
	private SaeSMSService smsService;
	
	/** 
	 * Helper Intro 정보
	 */
	@RequestMapping(value="/springrest/api/helper/intro", method=RequestMethod.GET)
	public void helperIntro(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="helper_idx", required=false) String helperIdx,
			@RequestParam(value="system_idx", required=false, defaultValue="SYS-00000000001") String systemIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Helper Intro ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("HELPER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Helper Intro ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Helper Intro ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else{//valid token
					
					if(helperIdx == null){
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1000000008);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1000000008);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Helper Intro ] FAIL " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						HelperVO vo = new HelperVO();
						vo.setIdx(helperIdx);
						HelperVO oldHelper = helperService.getHelper(vo);
						
						if(oldHelper.getIdx() == null){
							
							//set result
							result.setRes(false);
							result.setStatus(401);
							result.setCode(1100200001);
							if(isDebugEnabled){
								result.setStatusMsg(401);
								result.setCodeMsg(1100200001);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Helper Intro ] FAIL " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else{
							
							List totalConnList = callHistoryService.findCallHistoryByHelperConn(oldHelper);
							List totalLikeList = callHistoryService.findCallHistoryByHelperLikeFlag(oldHelper, true);
							int totalConnNum = totalConnList.size();
							int totalLikeNum = totalLikeList.size();
							List todayConnList = callHistoryService.findCallHistoryByHelperConnForToday(oldHelper);
							List todayLikeList = callHistoryService.findCallHistoryByHelperLikeFlagForToday(oldHelper, true);
							int todayConnNum = todayConnList.size();
							int todayLikeNum = todayLikeList.size();
							int rank = oldHelper.getRank();
							
							SystemVO sysVO = new SystemVO();
							sysVO.setIdx(systemIdx);
							SystemVO oldSystem = systemService.getSystem(sysVO);
							
							String rankName = "";
							int sysConnNum = 0;
							int sysLikeNum = 0;
							
							switch(rank){
								case SaeConfig.RANK_1:	//초보
									rankName = SaeConfig.RANK_1_NAME;
									sysConnNum = oldSystem.getExpertConnNum();
									sysLikeNum = oldSystem.getExpertLikeNum();
									break;
								case SaeConfig.RANK_2:	//숙련
									rankName = SaeConfig.RANK_2_NAME;
									sysConnNum = oldSystem.getMasterConnNum();
									sysLikeNum = oldSystem.getMasterLikeNum();
									break;
								case SaeConfig.RANK_3:	//마스터
									rankName = SaeConfig.RANK_3_NAME;
									sysConnNum = oldSystem.getVipConnNum();
									sysLikeNum = oldSystem.getVipLikeNum();
									break;
								case SaeConfig.RANK_4:	//VIP
									rankName = SaeConfig.RANK_4_NAME;
									break;
								default:				//초보
									rankName = SaeConfig.RANK_NAME_DEFAULT;
									sysConnNum = oldSystem.getExpertConnNum();
									sysLikeNum = oldSystem.getExpertLikeNum();
							}
							
							double rankScore = 0.0;
							double rankScoreRule = ( sysConnNum + sysLikeNum );
							if(rankScoreRule != 0){
								rankScore = 100.0 / rankScoreRule * ( totalConnNum + totalLikeNum );// 공식 : 다음 랭크의 100 / (설정 도움 횟수 + 좋아요) * (실제 도움 횟수 + 좋아요)
							}
							else{
								rankScore = 0;
							}
								
							
							//set result
							result.setRes(true);
							result.setStatus(200);
							result.setCode(1100300001);
							if(isDebugEnabled){
								result.setStatusMsg(200);
								result.setCodeMsg(1100300001);
							}
							
							data.put("total_conn_num", totalConnNum);
							data.put("total_like_num", totalLikeNum);
							data.put("conn_num", todayConnNum);
							data.put("like_num", todayLikeNum);
							data.put("rank", rank);
							data.put("rank_name", rankName);
							data.put("rank_score", rankScore);
							data.put("name", oldHelper.getName());
							data.put("nick_name", oldHelper.getNickname());
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Helper Intro ] OK : " + accessType );
								logger.debug("=============================================================");
							}
							
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Helper Intro ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Helper Intro ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * Helper 다른 시각 장애인 돕기 정보
	 */
	@RequestMapping(value="/springrest/api/helper/etc_help", method=RequestMethod.GET)
	public void helperFamily(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="helper_idx", required=false) String helperIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Helper ETC Help ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("HELPER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Helper ETC Help ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Helper ETC Help ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					if(helperIdx == null){
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1000000008);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1000000008);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Helper ETC Help ] FAIL " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						HelperVO vo = new HelperVO();
						vo.setIdx(helperIdx);
						HelperVO oldHelper = helperService.getHelper(vo);
						
						if(oldHelper.getIdx() == null){
							
							//set result
							result.setRes(false);
							result.setStatus(401);
							result.setCode(1100200001);
							if(isDebugEnabled){
								result.setStatusMsg(401);
								result.setCodeMsg(1100200001);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Helper ETC Help ] FAIL " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else{
							
							//set result
							result.setRes(true);
							result.setStatus(200);
							result.setCode(1100400001);
							if(isDebugEnabled){
								result.setStatusMsg(200);
								result.setCodeMsg(1100400001);
							}
							
							data.put("etc_help", oldHelper.isEtcHelp());
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Helper ETC Help ] OK : " + accessType );
								logger.debug("=============================================================");
							}
							
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Helper ETC Help ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Helper ETC Help ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * Helper 다른 시각 장애인 돕기 정보 수정
	 */
	@RequestMapping(value="/springrest/api/helper/etc_help/{helper_idx}", method=RequestMethod.POST)
	public void helperFamilyMod(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="etc_help", required=false) boolean etcHelp,
			@PathVariable("helper_idx") String helperIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Helper ETC Help Mod ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("HELPER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ Helper ETC Help Mod ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ Helper ETC Help Mod ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					if(helperIdx == null){
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1000000008);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1000000008);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ Helper ETC Help Mod ] FAIL " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						HelperVO vo = new HelperVO();
						vo.setIdx(helperIdx);
						HelperVO oldHelper = helperService.getHelper(vo);
						
						if(oldHelper.getIdx() == null){
							
							//set result
							result.setRes(false);
							result.setStatus(401);
							result.setCode(1100200001);
							if(isDebugEnabled){
								result.setStatusMsg(401);
								result.setCodeMsg(1100200001);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Helper ETC Help Mod ] FAIL " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else{
							
							oldHelper.setEtcHelp(etcHelp);
							helperService.updateHelper(oldHelper);
							
							//set result
							result.setRes(true);
							result.setStatus(200);
							result.setCode(1100500001);
							if(isDebugEnabled){
								result.setStatusMsg(200);
								result.setCodeMsg(1100500001);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ Helper ETC Help Mod ] OK : " + accessType );
								logger.debug("=============================================================");
							}
							
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ Helper ETC Help Mod ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ Helper ETC Help Mod ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * HELPER 본인 인증 SMS
	 */
	@RequestMapping(value="/springrest/api/helper/sms", method=RequestMethod.POST)
	public void helperSMS(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestParam(value="phone", required=false) String phone,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SMS ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("HELPER")){//HELPER 경우 접근 OK
			
			if(phone == null){
				
				//set result
				result.setRes(false);
				result.setStatus(400);
				result.setCode(1000000008);
				if(isDebugEnabled){
					result.setStatusMsg(400);
					result.setCodeMsg(1000000008);
				}

				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ SMS ] DENY : " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{
				
				SMSVO oldSMS = smsService.findSMS(phone);
				
				SecureRandom random = null;
				StringBuffer buf = new StringBuffer();
				
				try {
					random = SecureRandom.getInstance("SHA1PRNG");
				} catch (NoSuchAlgorithmException e) {
				   e.printStackTrace();
				}
				
				for(int i=0; i < SaeConfig.SMS_CODE_LENGTH; i++){
					buf.append(random.nextInt(10));
				}
				
				//String newCode = buf.toString();
				String newCode = "123456";//hard code

				if(oldSMS.getPhone() == null){//SMS 요청 없는 경우
					
					SMSVO newSMS = new SMSVO();	
					
					newSMS.setPhone(phone);
					newSMS.setCode(newCode);
					newSMS.setCreated(new Date());
					
					smsService.insertSMS(newSMS);
					
					//set result
					result.setRes(true);
					result.setStatus(200);
					result.setCode(1100700001);
					if(isDebugEnabled){
						result.setStatusMsg(200);
						result.setCodeMsg(1100700001);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("Create Code : " + newCode);
						logger.debug("[ SMS ] OK : " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else{//SMS 요청 이미 있는 경우
					
					oldSMS.setCode(newCode);
					oldSMS.setCreated(new Date());
					
					smsService.updateSMS(oldSMS);
					
					//set result
					result.setRes(true);
					result.setStatus(200);
					result.setCode(1100700002);
					if(isDebugEnabled){
						result.setStatusMsg(200);
						result.setCodeMsg(1100700002);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SMS ] OK : " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				
			}
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}

			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ SMS ] DENY : " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		//add result
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SMS ] END : " + accessType);
			logger.debug("=============================================================");
		}
		
	}
	
	/** 
	 * HELPER 본인 인증 SMS 절차
	 */
	@RequestMapping(value="/springrest/api/helper/sms/{phone}", method=RequestMethod.POST)
	public void helperSMSAuth(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestParam(value="code", required=false) String code,
			@PathVariable(value="phone") String phone,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SMS AUTH ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("HELPER")){//HELPER 경우 접근 OK
			
			if((phone == null) || (code == null)){
				
				//set result
				result.setRes(false);
				result.setStatus(400);
				result.setCode(1000000008);
				if(isDebugEnabled){
					result.setStatusMsg(400);
					result.setCodeMsg(1000000008);
				}

				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ SMS AUTH ] DENY : " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{
				
				SMSVO oldSMS = smsService.findSMS(phone);
				
				if(oldSMS.getPhone() == null){//SMS 요청 없는 경우
					
					//set result
					result.setRes(false);
					result.setStatus(400);
					result.setCode(1100600001);
					if(isDebugEnabled){
						result.setStatusMsg(400);
						result.setCodeMsg(1100600001);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SMS AUTH ] FAIL : " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else{//SMS 요청 이미 있는 경우
					
					if(oldSMS.getCode().equals(code)){
						
						//set result
						result.setRes(true);
						result.setStatus(200);
						result.setCode(1100600002);
						if(isDebugEnabled){
							result.setStatusMsg(200);
							result.setCodeMsg(1100600002);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SMS AUTH ] OK : " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						//set result
						result.setRes(false);
						result.setStatus(401);
						result.setCode(1100600001);
						if(isDebugEnabled){
							result.setStatusMsg(401);
							result.setCodeMsg(1100600001);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SMS AUTH ] FAIL : " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					
				}
				
			}
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}

			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ SMS AUTH ] DENY : " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		//add result
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SMS AUTH ] END : " + accessType);
			logger.debug("=============================================================");
		}
		
	}
	
}