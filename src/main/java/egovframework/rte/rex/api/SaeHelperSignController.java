package egovframework.rte.rex.api;

import java.util.Date;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;

import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.ResultVO;
import egovframework.rte.rex.service.SaeCallHistoryService;
import egovframework.rte.rex.service.SaeHelperService;
import egovframework.rte.rex.service.SaeSystemService;

/**
 * 도우미 인증 정보를 관리하는 컨트롤러 클래스를 정의한다.
 * @see
 * 151015 | KSM | Create
 */

@Controller
public class SaeHelperSignController {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeHelperSignController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** common */
	public TokenManager tokenMng = new TokenManager();
	
	/** helperService */
	@Resource(name="helperService")
	private SaeHelperService helperService;
	/** callHistoryService */
	@Resource(name="callHistoryService")
	private SaeCallHistoryService callHistoryService;
	/** systemService */
	@Resource(name="systemService")
	private SaeSystemService systemService;
	
	/** 
	 * HELPER 등록 처리
	 */
	@RequestMapping(value="/springrest/api/helper/sign_up", method=RequestMethod.POST)
	public void helperSignUp(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestParam(value="phone", required=false) String phone,
			@RequestParam(value="helper_pw", required=false) String helperPassword,
			@RequestParam(value="nickname", required=false) String nickname,
			@RequestParam(value="name", required=false) String name,
			@RequestParam(value="email", required=false) String email,
			@RequestParam(value="helper_type", required=false) int helperType,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SIGN UP ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();//result set
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(!accessType.equals("HELPER")){//HELPER 자격이 아니면 Deny 한다.
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ SIGN UP ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		else if(phone == null || helperPassword == null){
			
			//set result
			result.setRes(false);
			result.setStatus(400);
			result.setCode(1000000008);
			if(isDebugEnabled){
				result.setStatusMsg(400);
				result.setCodeMsg(1000000008);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ SIGN UP ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		else{//HELPER 자격일 경우
			boolean isHelper = helperService.checkHelperByPhone(phone);
			
			if(!isHelper){//등록된 Helper가 아니면 등록한다.
				HelperVO newHelper = new HelperVO();
				
				newHelper.setCreatedDate(new Date());
				newHelper.setPhone(phone);
				newHelper.setHelperPassword(helperPassword);
				newHelper.setNickname(nickname);
				newHelper.setName(name);
				newHelper.setEmail(email);
				newHelper.setRank(1);//최초 가입시 '초보' 도우미로 설정
				newHelper.setHelperType(helperType);
				newHelper.setHelperStatus(3);//최초 로그아웃 상태로 변경
				newHelper.setRegisterStatus(false);//최초 등록시 미승인 상태
				newHelper.setEtcHelp(true);//최초 등록시 다른 시각 장애인 돕기 ON
				newHelper.setAdminFk("ADMIN-00000000001");//hard coding
				
				String no = helperService.insertHelper(newHelper);//도우미 가입
				newHelper.setIdx(no);
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					Gson gson = new Gson();
					String helper_json = gson.toJson(newHelper);
					logger.debug("create helper : " + helper_json);				
					logger.debug("=============================================================");
				}
				
				//set result
				result.setRes(true);
				result.setStatus(200);
				result.setCode(1100100001);
				if(isDebugEnabled){
					result.setStatusMsg(200);
					result.setCodeMsg(1100100001);
				}
				
				//set data
				data.put("idx", newHelper.getIdx());
				data.put("phone", newHelper.getPhone());
				data.put("helper_type", newHelper.getHelperType());
				
			}
			else{//등록된 Helper이면 수정한다.
				HelperVO oldHelper = helperService.findHelperByPhone(phone);
				
				oldHelper.setHelperPassword(helperPassword);
				oldHelper.setNickname(nickname);
				oldHelper.setName(name);
				oldHelper.setEmail(email);
				oldHelper.setHelperType(helperType);
				oldHelper.setAdminFk("ADMIN-00000000001");//hard coding
				
				helperService.updateHelper(oldHelper);
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					Gson gson = new Gson();
					String helper_json = gson.toJson(oldHelper);
					logger.debug("update helper : " + helper_json);				
					logger.debug("=============================================================");
				}
				
				//set result
				result.setRes(true);
				result.setStatus(200);
				result.setCode(1100100002);
				if(isDebugEnabled){
					result.setStatusMsg(200);
					result.setCodeMsg(1100100002);
				}
				
				//set data
				data.put("idx", oldHelper.getIdx());
				data.put("phone", oldHelper.getPhone());
				data.put("helper_type", oldHelper.getHelperType());
				
			}//end of : 등록된 Helper인지 판별
			
		}//end of : HELPER 자격인지 판별
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SIGN UP ] END  : " + accessType);
			logger.debug("=============================================================");
		}
    }
	
	/** 
	 * HELPER 로그인
	 */
	@RequestMapping(value="/springrest/api/helper/sign_in", method=RequestMethod.POST)
	public void helperSignIn(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="phone", required=false) String phone,
			@RequestParam(value="helper_pw", required=false) String helper_pw,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SIGN IN ] START : " + accessType + " ( " + phone + " )");
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(!accessType.equals("HELPER")){//HELPER 자격이 아니면 Deny 한다.
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ SIGN IN ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		else{//HELPER 자격으로 시도할 경우
			
			if(accessToken == null){//header token 없음 : 수동 로그인
				
				boolean isHelper = helperService.checkHelperByPhone(phone);
				
				if(!isHelper){//등록된 Helper가 아니면 Deny 한다.
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					result.setCode(1100200001);
					if(isDebugEnabled){
						result.setStatusMsg(401);
						result.setCodeMsg(1100200001);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SIGN IN ] DENY " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else if(phone == null || helper_pw == null){
					
					//set result
					result.setRes(false);
					result.setStatus(400);
					result.setCode(1000000008);
					if(isDebugEnabled){
						result.setStatusMsg(400);
						result.setCodeMsg(1000000008);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SIGN IN ] DENY " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else{//등록된 Helper이면 수동 로그인 절차
					
					HelperVO oldHelper = helperService.signAuthHelper(phone, helper_pw);
					
					if(oldHelper.getIdx() == null){//로그인 실패 : 비밀번호 불일치시 
						
						//set result
						result.setRes(false);
						result.setStatus(401);
						result.setCode(1000000007);
						if(isDebugEnabled){
							result.setStatusMsg(401);
							result.setCodeMsg(1000000007);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SIGN IN ] FAIL " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else if(!oldHelper.isRegisterStatus()){//helper 승인 상태가 아닐때 
						
						//set result
						result.setRes(false);
						result.setStatus(401);
						result.setCode(1100200002);
						if(isDebugEnabled){
							result.setStatusMsg(401);
							result.setCodeMsg(1100200002);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SIGN IN ] FAIL " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{//로그인 성공
						
						oldHelper.setLastLoginDate(new Date());
						helperService.updateHelper(oldHelper);//last login time update
						
						String new_token = tokenMng.createToken(phone, SaeConfig.SAE_SERVER_NAME, SaeConfig.TOKEN_SECRET_KEY);//new token 생성
						
						//set result
						result.setRes(true);
						result.setStatus(200);
						result.setCode(1000000006);
						if(isDebugEnabled){
							result.setStatusMsg(200);
							result.setCodeMsg(1000000006);
						}
						
						//set data
						data.put("idx", oldHelper.getIdx());
						data.put("token", new_token);
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SIGN IN ] OK : " + accessType + " ( " + phone + " )");
							logger.debug("=============================================================");
						}
						
					}//end of : Login success / fail
					
				}//end of : Helper 가입 여부
				
			}
			else{//header token 있음 : 자동 로그인
				
				String phoneByToken = tokenMng.getPhoneByToken(accessToken);//token에서 phone 값을 추출한다
				HelperVO oldHelper = helperService.findHelperByPhone(phoneByToken);
				
				if(oldHelper.getIdx() == null){//등록된 Helper가 아니면 Deny 한다.
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					result.setCode(1100200001);
					if(isDebugEnabled){
						result.setStatusMsg(401);
						result.setCodeMsg(1100200001);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SIGN IN ] DENY " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else{//등록된 Helper이면 자동 로그인 절차
					
					boolean isExpreid = tokenMng.expirationToken(accessToken);
					boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
					boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
					
					if (!isVerify) {//invalid token
						//set result
						result.setRes(false);
						result.setStatus(401);
						result.setCode(1000000005);
						
						if(isDebugEnabled){
							result.setStatusMsg(401);
							result.setCodeMsg(1000000005);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SIGN IN ] DENY " + accessType);
							logger.debug("=============================================================");
						}
					}
					else{//valid token
						oldHelper.setLastLoginDate(new Date());
						helperService.updateHelper(oldHelper);//last login time update
						
						String new_token = tokenMng.createToken(phoneByToken, SaeConfig.SAE_SERVER_NAME, SaeConfig.TOKEN_SECRET_KEY);//new token 생성
						
						//set result
						result.setRes(true);
						result.setStatus(200);
						result.setCode(1000000005);
						if(isDebugEnabled){
							result.setStatusMsg(200);
							result.setCodeMsg(1000000005);
						}
						
						//set data
						data.put("idx", oldHelper.getIdx());
						data.put("token", new_token);
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SIGN IN ] OK : " + accessType + " ( " + phoneByToken + " )");
							logger.debug("=============================================================");
						}
						
					}//end of : valid token 여부
					
				}//end of : Helper 가입 여부
				
			}//end of : Phone, PW 로그인인지 Token 로그인 인지 판단 
			
		}//end of : Helper 인지 여부
		
		//add result
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SIGN IN ] END : " + accessType + " ( " + phone + " )");
			logger.debug("=============================================================");
		}
	}
	
}