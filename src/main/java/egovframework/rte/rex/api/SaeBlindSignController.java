package egovframework.rte.rex.api;

import java.util.Date;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;
import egovframework.rte.rex.service.ResultVO;
import egovframework.rte.rex.service.SaeUserService;
import egovframework.rte.rex.service.UserVO;

/**
 * 사용자 인증 정보를 관리하는 컨트롤러 클래스를 정의한다.
 * @see
 * 151021 | KSM | Create
 */

@Controller	
public class SaeBlindSignController{
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeBlindSignController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** common */
	public TokenManager tokenMng = new TokenManager();
	
	/** userService */
	@Resource(name="userService")
	private SaeUserService userService;
	
	/** 
	 * USER 로그인
	 */
	@RequestMapping(value="/springrest/api/user/sign_in", method=RequestMethod.POST)
	public void userSignIn(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="phone", required=false) String phone,
			@RequestParam(value="uuid", required=false) String uuid,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SIGN IN ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(!accessType.equals("USER")){//USER 자격이 아니면 Deny 한다.
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ SIGN IN ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		else{//USER 자격으로 시도 할 경우
			
			if(accessToken == null){//header token 없음 : 수동 로그인
				
				UserVO oldUser = userService.findUserByPhone(phone);
				
				if(oldUser.getIdx() == null){//User가 등록되어 있지 않을 경우 로그인 실패
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					result.setCode(1200200001);
					if(isDebugEnabled){
						result.setStatusMsg(401);
						result.setCodeMsg(1200200001);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SIGN IN ] DENY " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else if(uuid == null){
					
					//set result
					result.setRes(false);
					result.setStatus(400);
					result.setCode(1000000008);
					if(isDebugEnabled){
						result.setStatusMsg(400);
						result.setCodeMsg(1000000008);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SIGN IN ] DENY " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else {//User가 등록된 경우 수동 로그인 절차
					
					String new_token = tokenMng.createToken(phone, SaeConfig.SAE_SERVER_NAME, SaeConfig.TOKEN_SECRET_KEY);//new token 생성
					
					if(!oldUser.isRegisterStatus()){//User 기기 등록 전 로그인
						
						oldUser.setLastLoginDate(new Date());//last login time
						oldUser.setDeviceUUID(uuid);//기기 등록
						oldUser.setRegisterStatus(true);//기기 등록 플래그 수정
						
						userService.updateUser(oldUser);
						
						//set result
						result.setRes(true);
						result.setStatus(200);
						result.setCode(1000000006);
						if(isDebugEnabled){
							result.setStatusMsg(200);
							result.setCodeMsg(1000000006);
						}
						
						//set data
						data.put("idx", oldUser.getIdx());
						data.put("token", new_token);
						data.put("name", oldUser.getName());
						data.put("nick_name", oldUser.getNickname());
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SIGN IN ] OK : " + accessType + " ( " + phone + " )");
							logger.debug("=============================================================");
						}
						
					}
					else{//User 기기 등록 후 로그인
						
						if(!oldUser.getDeviceUUID().equals(uuid)){//등록 되지 않은 기기 경우 실패
							
							//set result
							result.setRes(false);
							result.setStatus(401);
							result.setCode(1200200002);
							if(isDebugEnabled){
								result.setStatusMsg(401);
								result.setCodeMsg(1200200002);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ SIGN IN ] DENY " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else{//등록된 기기 경우 성공
							
							oldUser.setLastLoginDate(new Date());//last login time
							userService.updateUser(oldUser);
							
							//set result
							result.setRes(true);
							result.setStatus(200);
							result.setCode(1000000006);
							if(isDebugEnabled){
								result.setStatusMsg(200);
								result.setCodeMsg(1000000006);
							}
							
							//set data
							data.put("idx", oldUser.getIdx());
							data.put("token", new_token);
							data.put("name", oldUser.getName());
							data.put("nick_name", oldUser.getNickname());
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ SIGN IN ] OK : " + accessType + " ( " + phone + " )");
								logger.debug("=============================================================");
							}
							
						}//end of : 등록된 기기 일치 여부
						
					}//end of : 기기 등록 여부
					
				}//user 등록 여부
				
			}
			else{//header token 있음 : 자동 로그인

				String phoneByToken = tokenMng.getPhoneByToken(accessToken);//token에서 phone 값을 추출한다
				UserVO oldUser = userService.findUserByPhone(phoneByToken);
				
				if(oldUser.getIdx() == null){//User가 등록되어 있지 않을 경우 로그인 실패
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					result.setCode(1200200001);
					if(isDebugEnabled){
						result.setStatusMsg(401);
						result.setCodeMsg(1200200001);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ SIGN IN ] DENY " + accessType);
						logger.debug("=============================================================");
					}
					
				}
				else{//User가 등록된 경우 자동 로그인 절차
					
					boolean isExpreid = tokenMng.expirationToken(accessToken);
					boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
					boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
					
					if (!isVerify) {//invalid token
						//set result
						result.setRes(false);
						result.setStatus(401);
						result.setCode(1000000003);
						
						if(isDebugEnabled){
							result.setStatusMsg(401);
							result.setCodeMsg(1000000003);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SIGN IN ] DENY " + accessType);
							logger.debug("=============================================================");
						}
					}
					else{
						
						oldUser.setLastLoginDate(new Date());//last login time
						userService.updateUser(oldUser);
						
						String new_token = tokenMng.createToken(phoneByToken, SaeConfig.SAE_SERVER_NAME, SaeConfig.TOKEN_SECRET_KEY);//new token 생성
						
						//set result
						result.setRes(true);
						result.setStatus(200);
						result.setCode(1000000005);
						if(isDebugEnabled){
							result.setStatusMsg(200);
							result.setCodeMsg(1000000005);
						}
						
						//set data
						data.put("idx", oldUser.getIdx());
						data.put("token", new_token);
						data.put("name", oldUser.getName());
						data.put("nick_name", oldUser.getNickname());
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ SIGN IN ] OK : " + accessType + " ( " + phoneByToken + " )");
							logger.debug("=============================================================");
						}
						
					}// token valid 여부
					
				}//user 등록 여부
				
			}//end of : 로그인 수동/자동 여부
			
		}
		
		//add result
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ SIGN IN ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
}