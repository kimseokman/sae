package egovframework.rte.rex.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;
import egovframework.rte.rex.comm.Util;
import egovframework.rte.rex.service.CallHistoryVO;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.ResultVO;
import egovframework.rte.rex.service.SaeCallHistoryService;
import egovframework.rte.rex.service.SaeHelperService;
import egovframework.rte.rex.service.SaeUserService;
import egovframework.rte.rex.service.UserVO;

/**
 * 지원기록 정보를 관리하는 컨트롤러 클래스를 정의한다.
 * @see
 * 151023 | KSM | Create
 */

@Controller
public class SaeCallHistoryController{
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeCallHistoryController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** common */
	public TokenManager tokenMng = new TokenManager();
	
	/** callHistoryService */
	@Resource(name="callHistoryService")
	private SaeCallHistoryService callHistoryService;
	/** userService */
	@Resource(name="userService")
	private SaeUserService userService;
	/** helperService */
	@Resource(name="helperService")
	private SaeHelperService helperService;
	
	/** 
	 * History 목록
	 */
	@RequestMapping(value="/springrest/api/history", method=RequestMethod.GET)
	public void historyList(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="user_idx", required=false) String userIdx,
			@RequestParam(value="helper_idx", required=false) String helperIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ History List ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER") || accessType.equals("HELPER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ History List ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ History List ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					
					if(accessType.equals("USER")){
						
						if(userIdx == null){
							
							//set result
							result.setRes(false);
							result.setStatus(400);
							result.setCode(1000000008);
							if(isDebugEnabled){
								result.setStatusMsg(400);
								result.setCodeMsg(1000000008);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ History List ] FAIL : " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else{
							
							UserVO vo = new UserVO();
							vo.setIdx(userIdx);
							List userCallHistoryList = callHistoryService.findCallHistoryByUser(vo, SaeConfig.HISTORY_LIMIT);

							List callHistoryList = new ArrayList();
							
							for( int i=userCallHistoryList.size()-1; i >=0; i--){
								CallHistoryVO originCallHistory = (CallHistoryVO) userCallHistoryList.get(i);
								
								HashMap<String, Object> tempHistory = new HashMap<String, Object>();	//data set
								
								tempHistory.put("idx", originCallHistory.getIdx());
								
								tempHistory.put("support_type", originCallHistory.getSupportType());//지원 타입
								tempHistory.put("call_status", originCallHistory.getStatus());//call 타입
								tempHistory.put("request_time", format.format(originCallHistory.getBeginTime()));
								
								String duringTime = "EMPTY";
								if(originCallHistory.getStatus() == 3){//정상 일때만
									duringTime = Util.getTimeDiff(originCallHistory.getBeginTime(), originCallHistory.getEndTime());
									
									tempHistory.put("during_time", duringTime);
								}
								
								tempHistory.put("helper_name", originCallHistory.getHelper().getName());
								tempHistory.put("helper_nickname", originCallHistory.getHelper().getNickname());
								tempHistory.put("user_name", originCallHistory.getUser().getName());
								tempHistory.put("user_nickname", originCallHistory.getUser().getNickname());
								
								callHistoryList.add(tempHistory);
							}
							
							data.put("call_history_list", callHistoryList);
							
							//set result
							result.setRes(true);
							result.setStatus(200);
							result.setCode(1000400006);
							if(isDebugEnabled){
								result.setStatusMsg(200);
								result.setCodeMsg(1000400006);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ History List ] OK : " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						
					}
					else if(accessType.equals("HELPER")){
						
						if(helperIdx == null){
							
							//set result
							result.setRes(false);
							result.setStatus(400);
							result.setCode(1000000008);
							if(isDebugEnabled){
								result.setStatusMsg(400);
								result.setCodeMsg(1000000008);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ History List ] FAIL : " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else{
							
							HelperVO vo = new HelperVO();
							vo.setIdx(helperIdx);
							List helperCallHistoryList = callHistoryService.findCallHistoryByHelper(vo, SaeConfig.HISTORY_LIMIT);
							
							List callHistoryList = new ArrayList();
							
							for( int i=helperCallHistoryList.size()-1; i >=0; i--){
								CallHistoryVO originCallHistory = (CallHistoryVO) helperCallHistoryList.get(i);
								
								HashMap<String, Object> tempHistory = new HashMap<String, Object>();	//data set
								
								tempHistory.put("idx", originCallHistory.getIdx());
								
								tempHistory.put("support_type", originCallHistory.getSupportType());//지원 타입
								tempHistory.put("call_status", originCallHistory.getStatus());//call 타입
								tempHistory.put("request_time", format.format(originCallHistory.getBeginTime()));
								
								String duringTime = "EMPTY";
								if(originCallHistory.getStatus() == 3){//정상 일때만
									duringTime = Util.getTimeDiff(originCallHistory.getBeginTime(), originCallHistory.getEndTime());
									
									tempHistory.put("during_time", duringTime);
								}
								
								tempHistory.put("helper_name", originCallHistory.getHelper().getName());
								tempHistory.put("helper_nickname", originCallHistory.getHelper().getNickname());
								tempHistory.put("user_name", originCallHistory.getUser().getName());
								tempHistory.put("user_nickname", originCallHistory.getUser().getNickname());
								
								callHistoryList.add(tempHistory);
							}
							
							data.put("call_history_list", callHistoryList);
							
							//set result
							result.setRes(true);
							result.setStatus(200);
							result.setCode(1000400006);
							if(isDebugEnabled){
								result.setStatusMsg(200);
								result.setCodeMsg(1000400006);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ History List ] OK : " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ History List ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ History List ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * History 추가 : call 시도
	 */
	@RequestMapping(value="/springrest/api/history", method=RequestMethod.POST)
	public void historyAdd(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@RequestParam(value="user_idx", required=false) String userIdx,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ History Add ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER")){//USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ History Add ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ History Add ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
					
					if(userIdx == null){
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1000000008);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1000000008);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ History Add ] FAIL : " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						UserVO vo = new UserVO();
						vo.setIdx(userIdx);
						UserVO oldUser = userService.getUser(vo);
						
						if(oldUser.getIdx() == null){
							
							//set result
							result.setRes(false);
							result.setStatus(400);
							result.setCode(1200200001);
							if(isDebugEnabled){
								result.setStatusMsg(400);
								result.setCodeMsg(1200200001);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ History Add ] FAIL : " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else{
							
							CallHistoryVO callHistoryVO = new CallHistoryVO();
							
							callHistoryVO.setUserId(oldUser.getIdx());
							callHistoryVO.setRequestTime(new Date());
							callHistoryVO.setLikeType(false);// 만족도 여부 : false
							callHistoryVO.setStatus(1);// 상태 : 연결 요청중
							
							callHistoryVO.setUser(oldUser);
							
							String no = callHistoryService.insertCallHistory(callHistoryVO);
							
							//set result
							result.setRes(true);
							result.setStatus(200);
							result.setCode(1000400001);
							if(isDebugEnabled){
								result.setStatusMsg(200);
								result.setCodeMsg(1000400001);
							}
							
							//set data
							data.put("history_idx", no);
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ History Add ] OK : " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ History Add ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ History Add ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
	
	/** 
	 * History 수정
	 */
	@RequestMapping(value="/springrest/api/history/{history_idx}", method=RequestMethod.POST)
	public void historyMod(
			@RequestHeader(value="Access-Type", required=false, defaultValue="GUEST") String accessType,
			@RequestHeader(value="Access-Token", required=false) String accessToken,
			@PathVariable("history_idx") String historyIdx,
			@RequestParam(value="helper_idx", required=false) String helperIdx,
			@RequestParam(value="support_type", required=false, defaultValue="0") int supportType,
			@RequestParam(value="like_type", required=false, defaultValue="0") boolean likeType,
			@RequestParam(value="call_status", required=false, defaultValue="0") int callStatus,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ History Mod ] START : " + accessType);
			logger.debug("=============================================================");
		}
		
		ResultVO result = new ResultVO();
		HashMap<String, Object> data = new HashMap<String, Object>();	//data set
		
		if(accessType.equals("USER") || accessType.equals("HELPER")){//HELPER or USER 경우 접근 OK
			
			if(accessToken == null){//Token을 보내지 않은 경우 Fail
				
				//set result
				result.setRes(false);
				result.setStatus(401);
				result.setCode(1000000002);
				if(isDebugEnabled){
					result.setStatusMsg(401);
					result.setCodeMsg(1000000002);
				}
				
				if(isDebugEnabled){
					logger.debug("=============================================================");
					logger.debug("[ History Mod ] DENY " + accessType);
					logger.debug("=============================================================");
				}
				
			}
			else{//Token을 보낸 경우 Token 검증 절차
				
				boolean isExpreid = tokenMng.expirationToken(accessToken);
				boolean isVerify = tokenMng.verifierToken(accessToken, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenMng.isValidToken(isExpreid, isVerify);
				
				if (!isPass) {//invalid token
					
					//set result
					result.setRes(false);
					result.setStatus(401);
					if(isExpreid)
						result.setCode(1000000004);
					else
						result.setCode(1000000005);
					
					if(isDebugEnabled){
						result.setStatusMsg(401);
						if(isExpreid)
							result.setCodeMsg(1000000004);
						else
							result.setCodeMsg(1000000005);
					}
					
					if(isDebugEnabled){
						logger.debug("=============================================================");
						logger.debug("[ History Mod ] DENY : " + accessType);
						logger.debug("=============================================================");
					}
				}
				else{//valid token
				
					if((historyIdx == null) || (helperIdx == null) || (supportType == 0) || (callStatus == 0)){
						
						//set result
						result.setRes(false);
						result.setStatus(400);
						result.setCode(1000000008);
						if(isDebugEnabled){
							result.setStatusMsg(400);
							result.setCodeMsg(1000000008);
						}
						
						if(isDebugEnabled){
							logger.debug("=============================================================");
							logger.debug("[ History Mod ] FAIL : " + accessType);
							logger.debug("=============================================================");
						}
						
					}
					else{
						
						CallHistoryVO vo = new CallHistoryVO();
						vo.setIdx(historyIdx);
						CallHistoryVO oldCallHistory = callHistoryService.getCallHistory(vo);
						
						HelperVO h_vo = new HelperVO();
						h_vo.setIdx(helperIdx);
						HelperVO oldHelper = helperService.getHelper(h_vo);
						
						if(oldCallHistory.getIdx() == null){
							
							//set result
							result.setRes(false);
							result.setStatus(400);
							result.setCode(1000400002);
							if(isDebugEnabled){
								result.setStatusMsg(400);
								result.setCodeMsg(1000400002);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ History Mod ] FAIL : " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else if(oldHelper.getIdx() == null){
							
							//set result
							result.setRes(false);
							result.setStatus(400);
							result.setCode(1100200001);
							if(isDebugEnabled){
								result.setStatusMsg(400);
								result.setCodeMsg(1100200001);
							}
							
							if(isDebugEnabled){
								logger.debug("=============================================================");
								logger.debug("[ History Mod ] FAIL : " + accessType);
								logger.debug("=============================================================");
							}
							
						}
						else{//call service start
							
							if(callStatus == 2){//init 2 : 연결중
								
								oldCallHistory.setHelperId(oldHelper.getIdx());
								oldCallHistory.setBeginTime(new Date());//시작 시간
								oldCallHistory.setSupportType(supportType);
								oldCallHistory.setStatus(callStatus);
								
								oldCallHistory.setHelper(oldHelper);
								
								callHistoryService.updateCallHistory(oldCallHistory);//update
								
								result.setRes(true);
								result.setStatus(200);
								result.setCode(1000400003);
								if(isDebugEnabled){
									result.setStatusMsg(200);
									result.setCodeMsg(1000400003);
								}
								
								data.put("history_idx", oldCallHistory.getIdx());
								
								if(isDebugEnabled){
									logger.debug("=============================================================");
									logger.debug("[ History Mod ] OK : " + accessType);
									logger.debug("=============================================================");
								}
								
							}
							else if(callStatus == 3){
								
								oldCallHistory.setEndTime(new Date());//끝 시간
								oldCallHistory.setSupportType(supportType);
								oldCallHistory.setLikeType(likeType);
								oldCallHistory.setStatus(callStatus);
								
								callHistoryService.updateCallHistory(oldCallHistory);//update
								
								result.setRes(true);
								result.setStatus(200);
								if(likeType){
									result.setCode(1000400007);
								}
								else{
									result.setCode(1000400004);
								}
								
								if(isDebugEnabled){
									result.setStatusMsg(200);
									if(likeType){
										result.setCodeMsg(1000400007);
									}
									else{
										result.setCodeMsg(1000400004);
									}
								}
								
								data.put("history_idx", oldCallHistory.getIdx());
								
								if(isDebugEnabled){
									logger.debug("=============================================================");
									logger.debug("[ History Mod ] OK : " + accessType);
									logger.debug("=============================================================");
								}
								
							}
							else{
								
								oldCallHistory.setStatus(callStatus);
								
								callHistoryService.updateCallHistory(oldCallHistory);//update
								
								result.setRes(true);
								result.setStatus(200);
								result.setCode(1000400004);
								if(isDebugEnabled){
									result.setStatusMsg(200);
									result.setCodeMsg(1000400004);
								}
								
								data.put("history_idx", oldCallHistory.getIdx());
								
								if(isDebugEnabled){
									logger.debug("=============================================================");
									logger.debug("[ History Mod ] OK : " + accessType);
									logger.debug("=============================================================");
								}
								
							}
							/*
							else{
								
								//set result
								result.setRes(false);
								result.setStatus(400);
								result.setCode(1000400005);
								if(isDebugEnabled){
									result.setStatusMsg(400);
									result.setCodeMsg(1000400005);
								}
								
								if(isDebugEnabled){
									logger.debug("=============================================================");
									logger.debug("[ History Mod ] FAIL : " + accessType);
									logger.debug("=============================================================");
								}
								
							}
							*/
							
						}
						
					}
					
				}//end of : valid token 여부
				
			}// end of : Header Token 존재 여부
			
		}
		else{//나머지 FAIL
			
			//set result
			result.setRes(false);
			result.setStatus(401);
			result.setCode(1000000001);
			if(isDebugEnabled){
				result.setStatusMsg(401);
				result.setCodeMsg(1000000001);
			}
			
			if(isDebugEnabled){
				logger.debug("=============================================================");
				logger.debug("[ History Mod ] DENY " + accessType);
				logger.debug("=============================================================");
			}
			
		}
		
		model.addAttribute("result", result);
		
		if(result.isRes())
			model.addAttribute("data", data);//OK
		else
			model.addAttribute("data", null);//FAIL
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ History Mod ] END : " + accessType);
			logger.debug("=============================================================");
		}
	}
}