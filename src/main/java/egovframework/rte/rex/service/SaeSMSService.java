package egovframework.rte.rex.service;

/**
 * SMS (인증) 정보 처리에 대한 Class 형태를 정의한다.
 * @see
 * 151111 | KSM | Create
 */

public interface SaeSMSService {

	public void insertSMS(SMSVO smsVO) throws Exception;
	public SMSVO findSMS(String phone) throws Exception;
	public void updateSMS(SMSVO smsVO) throws Exception;
	public void deleteSMS(SMSVO smsVO) throws Exception;
	
}