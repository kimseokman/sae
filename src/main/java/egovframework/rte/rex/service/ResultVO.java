package egovframework.rte.rex.service;

import java.io.Serializable;

import org.apache.log4j.Logger;

public class ResultVO implements Serializable{
	
	/** Logger */
	private Logger logger = Logger.getLogger(ResultVO.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	private static final long serialVersionUID = 9074777506148044298L;
	
	private int status;
	
	private String statusMsg;
	
	private int code;
	
	private String codeMsg;
	
	private boolean res;
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(int status) {
		
		switch(status){
			case 200:	this.statusMsg = "OK";						break;
			case 400:	this.statusMsg = "Bad Requeest";			break;
			case 401:	this.statusMsg = "Unauthorized";			break;
			case 404:	this.statusMsg = "Not Found";				break;
			case 500:	this.statusMsg = "Internal Server Error";	break;
			case 503:	this.statusMsg = "Service Unavailable";		break;
			default:	this.statusMsg = "UNKNOWN";
		}
		
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getCodeMsg() {
		return codeMsg;
	}

	public void setCodeMsg(int code) {
		
		switch(code){
			//common
			case 1000000001:	this.codeMsg = "Not qualified";			break;
			case 1000000002:	this.codeMsg = "No tokens";				break;
			case 1000000003:	this.codeMsg = "Invalid tokens";		break;
			case 1000000004:	this.codeMsg = "Expired tokens";		break;
			case 1000000005:	this.codeMsg = "Valid tokens";			break;
			case 1000000006:	this.codeMsg = "Created new tokens";	break;
			case 1000000007:	this.codeMsg = "Invalid password";		break;
			case 1000000008:	this.codeMsg = "Required parameter";	break;
			//common - family
			case 1000100001:	this.codeMsg = "Acceptance is pending";		break;
			case 1000100002:	this.codeMsg = "Family request OK";			break;
			case 1000100003:	this.codeMsg = "Family list OK";			break;
			case 1000100004:	this.codeMsg = "Family request accept";		break;
			case 1000100005:	this.codeMsg = "Family request delete";		break;
			//common - likehelper
			case 1000200001:	this.codeMsg = "Registered Like Helper";						break;
			case 1000200002:	this.codeMsg = "'like_num' is the maximum is limited to one";	break;
			case 1000200003:	this.codeMsg = "Like Helper List";								break;
			//common - system
			case 1000300001:	this.codeMsg = "System Call OK";	break;
			//common - history
			case 1000400001:	this.codeMsg = "Call History Add";	break;
			case 1000400002:	this.codeMsg = "Not a registered call history";	break;
			case 1000400003:	this.codeMsg = "Call history START record OK";	break;
			case 1000400004:	this.codeMsg = "Call history END record OK";	break;
			case 1000400005:	this.codeMsg = "Not defined call status";	break;
			case 1000400006:	this.codeMsg = "Call History List OK";	break;
			case 1000400007:	this.codeMsg = "Call history END record OK ( Like )";	break;
			
			//common - call
			case 1000500001:	this.codeMsg = "Call List OK";	break;
			
			//helper
			case 1100100001:	this.codeMsg = "Created New Helper";						break;
			case 1100100002:	this.codeMsg = "Updated Old Helper";						break;

			case 1100200001:	this.codeMsg = "Not a registered helper";					break;
			case 1100200002:	this.codeMsg = "Not allowed by Administrator ( helper )";	break;
			
			case 1100300001:	this.codeMsg = "Get Intro Information OK ( helper )";	break;
			
			case 1100400001:	this.codeMsg = "ETC Help Info OK";	break;
			
			case 1100500001:	this.codeMsg = "ETC Help Mod OK";	break;
			
			case 1100600001:	this.codeMsg = "SMS Auth Fail";	break;
			case 1100600002:	this.codeMsg = "SMS Auth OK";	break;
			
			case 1100700001:	this.codeMsg = "SMS Create Code OK";	break;
			case 1100700002:	this.codeMsg = "SMS Update Code OK";	break;
			
			//user
			case 1200200001:	this.codeMsg = "Not a registered user";				break;
			case 1200200002:	this.codeMsg = "Not a registered devices ( user )";	break;
			
			case 1200300002:	this.codeMsg = "Get Intro Information OK ( user )";	break;
			
			//case by unknown 
			default:
				this.codeMsg = "UNKNOWN";
		}
		
	}

	public boolean isRes() {
		return res;
	}

	public void setRes(boolean res) {
		this.res = res;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}