package egovframework.rte.rex.service;

import java.util.List;

public interface SaeAccessHistoryService {
	public List<?> selectAccessHistoryList() throws Exception;
}
