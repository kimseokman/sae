package egovframework.rte.rex.service;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Admin(시각 장애인) 정보 클래스를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

@Entity
@Table(name = "t_admin")
@XmlRootElement
public class AdminVO implements Serializable{

	private static final long serialVersionUID = -1468658077207414120L;
	
	@Id
	@Column(name="idx")
	private String idx;
	
	@Column(name="admin_id")
	private String adminId;
	
	@Column(name="admin_pw")
	private String adminPassword;
	
	public String getIdx(){
		return idx;
	}
	
	public void setIdx(String idx){
		this.idx = idx;
	}
	
	public String getAdminId(){
		return adminId;
	}
	
	public void setAdminId(String adminId){
		this.adminId = adminId;
	}
	
	public String getAdminPassword(){
		return adminPassword;
	}
	
	public void setAdminPassword(String adminPassword){
		this.adminPassword = adminPassword;
	}
	
}