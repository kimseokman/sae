package egovframework.rte.rex.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Family(도우미: 가족) 정보 클래스를 정의한다.
 * @see
 * 151015 | KSM | Create
 */

@Entity
@Table(name = "t_usernhelper")
@XmlRootElement
public class FamilyVO implements Serializable{

	private static final long serialVersionUID = 919855679038361951L;
	
	@Id
	@Column(name="idx")
	private String idx;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created")
	private Date createdDate;

	@Column(name="user_fk")
	private String userFk;
	
	@Column(name="helper_fk")
	private String helperFk;
	
	@Column(name="request_type")
	private String requestType;
	
	@Column(name="is_family")
	private boolean isfamily;
	
	@ManyToOne
	@JoinColumn(name="user_fk", referencedColumnName = "idx", insertable = false, updatable = false)
	private UserVO user;
	
	@ManyToOne
	@JoinColumn(name="helper_fk", referencedColumnName = "idx", insertable = false, updatable = false)
	private HelperVO helper;
	
	public String getIdx(){
		return idx;
	}
	
	public void setIdx(String idx){
		this.idx = idx;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getUserFk(){
		return userFk;
	}
	
	public void setUserFk(String userFk){
		this.userFk = userFk;
	}
	
	public String getHelperFk(){
		return helperFk;
	}
	
	public void setHelperFk(String helperFk){
		this.helperFk = helperFk;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public boolean isIsfamily() {
		return isfamily;
	}

	public void setIsfamily(boolean isfamily) {
		this.isfamily = isfamily;
	}
	
	//foreign data
	public UserVO getUser() {
        return user;
    }
 
    public void setUser(UserVO user) {
        this.user = user;
    }
    
    public HelperVO getHelper() {
        return helper;
    }
 
    public void setHelper(HelperVO helper) {
        this.helper = helper;
    }
	
}