package egovframework.rte.rex.service;

import java.util.List;

/**
 * Helper(도우미) 정보 처리에 대한 Class 형태를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

public interface SaeHelperService {
	/**
	 *	도우미 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장한다.
	 * @param helperVO
	 * @return String 목록화면
	 * @throws Exception
	 */
	public String insertHelper(HelperVO helperVO) throws Exception;
	
	/**
	 * 도우미 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 * @return List
	 * @throws Exception
	 */
	public List selectHelperList() throws Exception;
	public List<?> selectCounselorList() throws Exception;
	
	/**
	 * 도우미 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 * @param helperVO
	 * @return HelperVO
	 * @throws Exception
	 */
	public HelperVO getHelper(HelperVO helperVO) throws Exception;
	
	/**
	 * 화면에 조회된 도우미 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 * @param helperVO
	 * @throws Exception
	 */
	public void updateHelper(HelperVO helperVO) throws Exception;
	
	/**
	 * 선택된 도우미 정보를 데이터베이스에서 삭제한다.
	 * @param helperVO
	 * @throws Exception
	 */
	public void deleteHelper(HelperVO helperVO) throws Exception;
	
	/**
	 * Check Helper Phone
	 */
	public boolean checkHelperByPhone(String phone) throws Exception;
	
	/**
	 * Find Helper Phone
	 */
	public HelperVO findHelperByPhone(String phone) throws Exception;
	
	/**
	 * Sign Auth Helper
	 */
	public HelperVO signAuthHelper(String phone, String helper_pw) throws Exception;
	
	public List<HelperVO> agentList(int limit) throws Exception;
	
	public List<HelperVO> findHelperByRule0(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule1(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule2(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule3(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule4(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule5(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule6(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule7(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule8(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule9(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule10(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule11(String userIdx, int limit) throws Exception;
	public List<?> findHelperByRule12(String userIdx, int limit) throws Exception;
}