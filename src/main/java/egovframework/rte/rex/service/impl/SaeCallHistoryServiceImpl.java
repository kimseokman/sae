package egovframework.rte.rex.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.rex.service.CallHistoryVO;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.SaeCallHistoryService;
import egovframework.rte.rex.service.UserVO;

@Service("callHistoryService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeCallHistoryServiceImpl extends AbstractServiceImpl implements SaeCallHistoryService {
	@Resource(name="callHistoryDAO")
	private CallHistoryDAO callHistoryDAO;
	
	/** ID Generation */
    @Resource(name="egovIdGnrServiceCallHistory")    
    private EgovIdGnrService egovIdGnrService; // ID Generation
    
	public String insertCallHistory(CallHistoryVO callHistoryVO) throws Exception{
		
		/** ID Generation Service */
    	String no = egovIdGnrService.getNextStringId();
    	callHistoryVO.setIdx(no);
    	
    	callHistoryDAO.insertCallHistory(callHistoryVO);
		
		return no;
	}
	

	public CallHistoryVO getCallHistory(CallHistoryVO callHistoryVO) throws Exception{
		CallHistoryVO vo = callHistoryDAO.getCallHistory(callHistoryVO);
		if(vo==null)
			 throw processException("info.nodata.msg");
        return vo;
	}

	public void updateCallHistory(CallHistoryVO callHistoryVO) throws Exception{
		callHistoryDAO.updateCallHistory(callHistoryVO);
	}
	
	public void deleteCallHistory(CallHistoryVO callHistoryVO) throws Exception{
		callHistoryDAO.deleteCallHistory(callHistoryVO);
	}
	
	public int getMeanAskingTime() throws Exception{
		return callHistoryDAO.getMeanAskingTime();
	}
	
	public int getMeanHelpingTime() throws Exception{
		return callHistoryDAO.getMeanHelpingTime();
	}
	
	@Override
	public List<?> selectSuccessList() throws Exception {
		return callHistoryDAO.selectSuccessList();
	}

	@Override
	public List<?> selectSatisfactionList() throws Exception {
		return callHistoryDAO.selectSatisfactionList();
	}
	
	@Override
	public List<?> selectSuccessCallList() throws Exception {
		return callHistoryDAO.selectSuccessCallList();
	}
	
	@Override
	public List<?> selectAskingCallList() throws Exception {
		return callHistoryDAO.selectAskingCallList();
	}
	
	public List<?> askingUserList() throws Exception {
		return callHistoryDAO.askingUserList();
	}
	
	@Override
	public List<?> selectCallHistoryList() throws Exception {
		return callHistoryDAO.selectCallHistoryList();
	}
	
	@Override
	public List<?> findCallHistoryByUser(UserVO userVO) throws Exception {
		return callHistoryDAO.findCallHistoryByUser(userVO);
	}

	@Override
	public List<?> findCallHistoryByHelper(HelperVO helperVO) throws Exception {
		return callHistoryDAO.findCallHistoryByHelper(helperVO);
	}

	@Override
	public List<?> findCallHistoryByUser(UserVO userVO, int limit) throws Exception {
		return callHistoryDAO.findCallHistoryByUser(userVO, limit);
	}

	@Override
	public List<?> findCallHistoryByHelper(HelperVO helperVO, int limit) throws Exception {
		return callHistoryDAO.findCallHistoryByHelper(helperVO, limit);
	}
	
	@Override
	public List<?> findCallHistoryByHelperConn(HelperVO helperVO) throws Exception {
		return callHistoryDAO.findCallHistoryByHelperConn(helperVO);
	}
	
	@Override
	public List<?> findCallHistoryByHelperLikeFlag(HelperVO helperVO, boolean likeFlag) throws Exception {
		return callHistoryDAO.findCallHistoryByHelperLikeFlag(helperVO, likeFlag);
	}
	
	@Override
	public List<?> findCallHistoryByHelperConnForToday(HelperVO helperVO) throws Exception {
		return callHistoryDAO.findCallHistoryByHelperConnForToday(helperVO);
	}
	
	@Override
	public List<?> findCallHistoryByHelperLikeFlagForToday(HelperVO helperVO, boolean likeFlag) throws Exception {
		return callHistoryDAO.findCallHistoryByHelperLikeFlagForToday(helperVO, likeFlag);
	}
	
}
