package egovframework.rte.rex.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.rex.service.SaeUserService;
import egovframework.rte.rex.service.UserVO;

/**
 * User(시각 장애인) 관한 비즈니즈 클래스를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

@Service("userService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeUserServiceImpl extends AbstractServiceImpl implements SaeUserService {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeUserServiceImpl.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** UserDAO */
    @Resource(name="userDAO")
	private UserDAO userDAO; //데이터베이스 접근 클래스
    
    /** ID Generation */
    @Resource(name="egovIdGnrServiceUser")    
    private EgovIdGnrService egovIdGnrService; // ID Generation
    
    /**
	 *	사용자 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장한다.
	 * @param userVO
	 * @return String 목록화면
	 * @throws Exception
	 */
	public String insertUser(UserVO userVO) throws Exception{
		
		/** ID Generation Service */
    	String no = egovIdGnrService.getNextStringId();
    	userVO.setIdx(no);
    	
    	userDAO.insertUser(userVO);
		
		return no;
	}
	
	/**
	 * 사용자 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 * @return List
	 * @throws Exception
	 */
	public List selectUserList() throws Exception{
		return userDAO.selectUserList();
	}
	
	/**
	 * 사용자 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 * @param userVO
	 * @return UserVO
	 * @throws Exception
	 */
	public UserVO getUser(UserVO userVO) throws Exception{
		UserVO vo = userDAO.getUser(userVO);
		if(vo==null)
			 throw processException("info.nodata.msg");
        return vo;
	}
	
	/**
	 * 화면에 조회된 사용자 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 * @param userVO
	 * @throws Exception
	 */
	public void updateUser(UserVO userVO) throws Exception{
		userDAO.updateUser(userVO);
	}
	
	/**
	 * 선택된 사용자 정보를 데이터베이스에서 삭제한다.
	 * @param userVO
	 * @throws Exception
	 */
	public void deleteUser(UserVO userVO) throws Exception{
		userDAO.deleteUser(userVO);
	}
	
	/**
	 * 사용자 찾기
	 */
	public List<?> findUsers(String key, String value, int page) throws Exception {
		return userDAO.findUsers(key, value, page);
	}

	public UserVO findUserByPhone(String phone) throws Exception{
		return userDAO.findUserByPhone(phone);
	}

	public List<?> findAskingUsers() throws Exception {
		return userDAO.findAskingUsers();
	}
}