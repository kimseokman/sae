package egovframework.rte.rex.service.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.rex.service.AccessHistoryVO;

@Repository("accessHistoryDAO")
@Transactional
public class AccessHistoryDAO extends HibernateDaoSupport {
	@Autowired
	public void setHibernateDaoSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	public List<?> selectAccessHistoryList() throws Exception {
		return super.getHibernateTemplate().loadAll(AccessHistoryVO.class);
	}
}
