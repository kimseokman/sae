package egovframework.rte.rex.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.rex.service.SaeAccessHistoryService;

@Service("accessHistoryService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeAccessHistoryServiceImpl extends AbstractServiceImpl implements SaeAccessHistoryService {
	@Resource(name="accessHistoryDAO")
	private AccessHistoryDAO accessHistoryDAO;

	@Override
	public List<?> selectAccessHistoryList() throws Exception {
		return accessHistoryDAO.selectAccessHistoryList();
	}
}
