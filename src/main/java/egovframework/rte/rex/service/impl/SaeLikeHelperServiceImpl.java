package egovframework.rte.rex.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.rex.service.LikeHelperVO;
import egovframework.rte.rex.service.SaeLikeHelperService;

/**
 * Like Helper (도우미: 선호) 관한 비즈니즈 클래스를 정의한다.
 * @see
 * 151028 | KSM | Create
 */

@Service("likeHelperService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeLikeHelperServiceImpl extends AbstractServiceImpl implements SaeLikeHelperService {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeLikeHelperServiceImpl.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** LikeHelperDAO */
    @Resource(name="likeHelperDAO")
	private LikeHelperDAO likeHelperDAO; //데이터베이스 접근 클래스
    
    /** ID Generation */
    @Resource(name="egovIdGnrServiceLikeHelper")    
    private EgovIdGnrService egovIdGnrService; // ID Generation
	
	/**
	 * 선호 도우미 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장한다.
	 */
	public String insertLikeHelper(LikeHelperVO likeHelperVO) throws Exception{
		/** ID Generation Service */
    	String no = egovIdGnrService.getNextStringId();
    	likeHelperVO.setIdx(no);
    	
    	likeHelperDAO.insertLikeHelper(likeHelperVO);
		
		return no;
	}
	
	/**
	 * 선호 도우미 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 */
	public List selectLikeHelperListAll() throws Exception{
		return likeHelperDAO.selectLikeHelperListAll();
	}
	
	/**
	 * 선호 도우미 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 */
	public LikeHelperVO getLikeHelper(LikeHelperVO likeHelperVO) throws Exception{
		LikeHelperVO vo = likeHelperDAO.getLikeHelper(likeHelperVO);
		if(vo==null)
			 throw processException("info.nodata.msg");
        return vo;
	}
	
	/**
	 * 화면에 조회된 선호 도우미 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 */
	public void updateLikeHelper(LikeHelperVO likeHelperVO) throws Exception{
		likeHelperDAO.updateLikeHelper(likeHelperVO);
	}
	
	/**
	 * 선택된 선호 도우미 정보를 데이터베이스에서 삭제한다.
	 */
	public void deleteLikeHelper(LikeHelperVO likeHelperVO) throws Exception{
		likeHelperDAO.deleteLikeHelper(likeHelperVO);
	}
	
	/**
	 * 선호 도우미 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 */
	public List selectLikeHelperList(String accessType, String accessToken) throws Exception{
		return likeHelperDAO.selectLikeHelperList(accessType, accessToken);
	}
	
	/**
	 * 선호 도우미 구하기
	 */
	public LikeHelperVO findLikeHelper(String userIdx, String helperIdx) throws Exception{
		return likeHelperDAO.findLikeHelper(userIdx, helperIdx);
	}
	
	/**
	 * 시각 장애인 선호 도우미 목록 구하기
	 */
	public List<LikeHelperVO> findLikeHelperByUser(String userIdx) throws Exception{
		return likeHelperDAO.findLikeHelperByUser(userIdx);
	}
	
	public List<LikeHelperVO> findCallLikeHelper(String userIdx) throws Exception{
		return likeHelperDAO.findCallLikeHelper(userIdx);
	}
	
	public List<LikeHelperVO> findCallLikeHelper(String userIdx, int limit) throws Exception{
		return likeHelperDAO.findCallLikeHelper(userIdx, limit);
	}
}