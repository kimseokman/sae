package egovframework.rte.rex.service.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.rex.service.SystemVO;

/**
 * System(시스템) 정보에 관한 데이터 접근 클래스를 정의한다.
 * @see
 * 151030 | KSM | Create
 */

@Repository("systemDAO")
@Transactional
public class SystemDAO extends HibernateDaoSupport {
	
	@Autowired
	public void setHibernateDaoSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public void insertSystem(SystemVO systemVO) throws Exception{
		super.getHibernateTemplate().save(systemVO);
	}
	
	public List<SystemVO> selectSystemList() throws Exception{
		return super.getHibernateTemplate().loadAll(SystemVO.class);
	}
	
	public SystemVO getSystem(SystemVO vo) throws Exception{
		return (SystemVO) super.getHibernateTemplate().get(SystemVO.class, vo.getIdx());
	}
	
	public void updateSystem(SystemVO systemVO) throws Exception{
		super.getHibernateTemplate().update(systemVO);
	}

	public void deleteSystem(SystemVO systemVO) throws Exception{
		super.getHibernateTemplate().delete(systemVO);
	}
	
}