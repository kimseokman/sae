package egovframework.rte.rex.service.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.rex.service.UserVO;

/**
 * User(시각 장애인) 정보에 관한 데이터 접근 클래스를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

@Repository("userDAO")
@Transactional
public class UserDAO extends HibernateDaoSupport {
	
	/**
	 * hibernate를 사용하기 위해 sessionFactory객체를 설정한다.
	 * @param sessionFactory
	 */
	@Autowired
	public void setHibernateDaoSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	/** 
	 * INSERT user info
	 * @param userVO
	 * @throws Exception
	 */
	public void insertUser(UserVO userVO) throws Exception{
		super.getHibernateTemplate().save(userVO);
	}
	
	/** 
	 * SELECT user LIST
	 * @return List<UserVO>
	 * @throws Exception
	 */
	public List<UserVO> selectUserList() throws Exception{
		return super.getHibernateTemplate().loadAll(UserVO.class);
	}
	
	/**
	 * SELECT user info
	 * @param vo
	 * @return UserVO
	 * @throws Exception
	 */
	public UserVO getUser(UserVO vo) throws Exception{
		return (UserVO) super.getHibernateTemplate().get(UserVO.class, vo.getIdx());
	}
	
	/** 
	 * UPDATE user info
	 * @param userVO
	 * @throws Exception
	 */
	public void updateUser(UserVO userVO) throws Exception{
		super.getHibernateTemplate().update(userVO);
	}
	
	/**
	 * DELETE user info
	 * @param userVO
	 * @throws Exception
	 */
	public void deleteUser(UserVO userVO) throws Exception{
		super.getHibernateTemplate().delete(userVO);
	}
	
	/**
	 * User 찾기 Phone 으로
	 */
	public List<?> findUsers(String key, String value, int page) throws Exception {
		return super.getHibernateTemplate().find(String.format("FROM UserVO WHERE %s LIKE ?", key), "%" + value + "%");
	}

	public UserVO findUserByPhone(String phone) throws Exception{
		
		List user_list = super.getHibernateTemplate().find("FROM UserVO WHERE phone = ?", phone);

		UserVO userVO = new UserVO();
		
		if(user_list.size() != 0)//case by not empty
			userVO = (UserVO) user_list.get(0);
		
		return userVO;
	}

	public List<?> findAskingUsers() throws Exception {
		return super.getHibernateTemplate().find("FROM UserVO u WHERE u.idx IN (SELECT c.userId FROM CallHistoryVO c WHERE c.endTime = NULL)");
	}
}