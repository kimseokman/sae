package egovframework.rte.rex.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.rex.service.SMSVO;

/**
 * SMS(인증 관련) 정보에 관한 데이터 접근 클래스를 정의한다.
 * @see
 * 151111 | KSM | Create
 */

@Repository("smsDAO")
@Transactional
public class SMSDAO extends HibernateDaoSupport {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SMSDAO.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	@Autowired
	public void setHibernateDaoSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	/** 
	 * INSERT
	 */
	public void insertSMS(SMSVO smsVO) throws Exception{
		super.getHibernateTemplate().save(smsVO);
	}
	
	/** 
	 * SELECT
	 */
	public SMSVO findSMS(String phone) throws Exception{
		
		List sms_list = super.getHibernateTemplate()
						.find("FROM SMSVO WHERE phone = ?", phone);

		SMSVO smsVO = new SMSVO();
		
		if(sms_list.size() != 0)//case by not empty
			smsVO = (SMSVO) sms_list.get(0);
		
		return smsVO;
	}
	
	/** 
	 * UPDATE
	 */
	public void updateSMS(SMSVO smsVO) throws Exception{
		super.getHibernateTemplate().update(smsVO);
	}
	
	/**
	 * DELETE
	 */
	public void deleteSMS(SMSVO smsVO) throws Exception{
		super.getHibernateTemplate().delete(smsVO);
	}
	
}