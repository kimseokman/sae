package egovframework.rte.rex.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.rex.service.FamilyVO;

/**
 * Family(도우미: 가족) 정보에 관한 데이터 접근 클래스를 정의한다.
 * @see
 * 151015 | KSM | Create
 */

@Repository("familyDAO")
@Transactional
public class FamilyDAO extends HibernateDaoSupport {
	
	/** Logger */
	private Logger logger = Logger.getLogger(FamilyDAO.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/**
	 * hibernate를 사용하기 위해 sessionFactory객체를 설정한다.
	 * @param sessionFactory
	 */
	@Autowired
	public void setHibernateDaoSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	/** 
	 * INSERT family info
	 * @param familyVO
	 * @throws Exception
	 */
	public void insertFamily(FamilyVO familyVO) throws Exception{
		super.getHibernateTemplate().save(familyVO);
	}
	
	/** 
	 * SELECT family LIST All
	 * @return List<AdminVO>
	 * @throws Exception
	 */
	public List<FamilyVO> selectFamilyListAll() throws Exception{
		return super.getHibernateTemplate().loadAll(FamilyVO.class);
	}
	
	/**
	 * SELECT family info
	 * @param vo
	 * @return FamilyVO
	 * @throws Exception
	 */
	public FamilyVO getFamily(FamilyVO vo) throws Exception{
		return (FamilyVO) super.getHibernateTemplate().get(FamilyVO.class, vo.getIdx());
	}
	
	/** 
	 * UPDATE family info
	 * @param familyVO
	 * @throws Exception
	 */
	public void updateFamily(FamilyVO familyVO) throws Exception{
		super.getHibernateTemplate().update(familyVO);
	}
	
	/**
	 * DELETE family info
	 * @param familyVO
	 * @throws Exception
	 */
	public void deleteFamily(FamilyVO familyVO) throws Exception{
		super.getHibernateTemplate().delete(familyVO);
	}
	
	/** 
	 * SELECT family LIST
	 * @return List<AdminVO>
	 * @throws Exception
	 */
	public List<FamilyVO> selectFamilyList(String accessType, String accessToken) throws Exception{
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("USER-TOKEN : " + accessToken);
			logger.debug("USER-TYPE : " + accessType);
			logger.debug("=============================================================");
		}
		
		List<FamilyVO> family = null;
		
		if(accessType == "USER"){
			//List<FamilyVO> user = super.getHibernateTemplate()
			//		.find("from FamilyVO where user_fk = ? and helper_fk = ? ", vo.getAdminId()).loadAll(FamilyVO.class);
			//family = super.getHibernateTemplate()
			//		.find("from FamilyVO where user_fk = ? and helper_fk = ? ", vo.getAdminId()).loadAll(FamilyVO.class);
		}
		else if(accessType == "HELPER"){
			family = super.getHibernateTemplate().loadAll(FamilyVO.class);
		}
		
		return family;
	}
	
	/**
	 * 가족 가족 구하기
	 */
	public FamilyVO findFamily(String userIdx, String helperIdx) throws Exception{
		Object[] args = new Object[] {userIdx, helperIdx};
		
		List family_list = super.getHibernateTemplate()
								.find("FROM FamilyVO WHERE user_fk = ? AND helper_fk = ?", args);
		
		FamilyVO familyVO = new FamilyVO();
		
		if(family_list.size() != 0)//case by not empty
			familyVO = (FamilyVO) family_list.get(0);
		
		return familyVO;
	}
	
	/**
	 * 도우미 가족 가족 구하기
	 */
	public List<FamilyVO> findFamilyByHelper(String helperIdx) throws Exception{
		
		List family_list = super.getHibernateTemplate()
								.find("FROM FamilyVO WHERE helper_fk = ?", helperIdx);
		
		return family_list;
	}
	
	/**
	 * 시각 장애인 가족 가족 구하기
	 */
	public List<FamilyVO> findFamilyByUser(String userIdx) throws Exception{
		
		List family_list = super.getHibernateTemplate()
								.find("FROM FamilyVO WHERE user_fk = ?", userIdx);
		
		return family_list;
	}
}