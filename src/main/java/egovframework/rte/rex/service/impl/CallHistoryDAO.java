package egovframework.rte.rex.service.impl;

import java.util.Calendar;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.rex.service.CallHistoryVO;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.UserVO;

@Repository("callHistoryDAO")
@Transactional
public class CallHistoryDAO extends HibernateDaoSupport {
	@Autowired
	public void setHibernateDaoSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public void insertCallHistory(CallHistoryVO callHistoryVO) throws Exception{
		super.getHibernateTemplate().save(callHistoryVO);
	}
	
	public CallHistoryVO getCallHistory(CallHistoryVO vo) throws Exception{
		return (CallHistoryVO) super.getHibernateTemplate().get(CallHistoryVO.class, vo.getIdx());
	}

	public void updateCallHistory(CallHistoryVO callHistoryVO) throws Exception{
		super.getHibernateTemplate().update(callHistoryVO);
	}

	public void deleteCallHistory(CallHistoryVO callHistoryVO) throws Exception{
		super.getHibernateTemplate().delete(callHistoryVO);
	}
	
	public int getMeanAskingTime() throws Exception {
		super.getHibernateTemplate().find("FROM CallHistoryVO WHERE status in(2,3) and date(begin_time)=date(now())");
		//select DATEDIFF(second, begin_time, request_time) from t_callhistory where status in(2,3);
		//이걸 DAO상에서 쿼리로 어떻게 표현하나?
		return 3; //FIXME
	}
	
	public int getMeanHelpingTime() throws Exception {
		super.getHibernateTemplate().find("FROM CallHistoryVO WHERE status=3 and date(begin_time)=date(now())");
		return 4; //FIXME
	}
	
		
	public List<?> askingUserList() throws Exception {
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE status=1 order by request_time asc");
	}
	
	
	public List<?> selectSuccessList() throws Exception {
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE status=3 and date(begin_time)=date(now())");
	}
	
	public List<?> selectSatisfactionList() throws Exception {
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE status=3 AND like_type=1 and date(begin_time)=date(now())");
	}
	
	public List<?> selectSuccessCallList() throws Exception {
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE status in(2,3) and date(begin_time)=date(now())");
	}
	
	public List<?> selectAskingCallList() throws Exception {
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE status in(1,2,3,4) and date(begin_time)=date(now())");
	}
	

	public List<?> selectCallHistoryList() throws Exception {
		return super.getHibernateTemplate().loadAll(CallHistoryVO.class);
	}
	
	public List<?> findCallHistoryByUser(UserVO userVO) {
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE userId = ?", userVO.getIdx());
	}

	public List<?> findCallHistoryByHelper(HelperVO helperVO) {
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE helperId = ?", helperVO.getIdx());
	}

	public List<?> findCallHistoryByUser(UserVO userVO, int limit) {
		
		Object[] args = new Object[] {userVO.getIdx()};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List callHistoryList = super.getHibernateTemplate().find("FROM CallHistoryVO WHERE userId = ? "
				+ "AND requestTime IS NOT NULL "
				+ "AND beginTime IS NOT NULL "
				+ "AND endTime IS NOT NULL "
				, args);
		
		return callHistoryList; 
	}

	public List<?> findCallHistoryByHelper(HelperVO helperVO, int limit) {
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		Object[] args = new Object[] {helperVO.getIdx()};
		
		List callHistoryList = super.getHibernateTemplate().find("FROM CallHistoryVO WHERE helperId = ? "
				+ "AND requestTime IS NOT NULL "
				+ "AND beginTime IS NOT NULL "
				+ "AND endTime IS NOT NULL "
				, args);
		
		return callHistoryList;
	}
	
	public List<?> findCallHistoryByHelperConn(HelperVO helperVO) {
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE helperId = ?", helperVO.getIdx());
	}
	
	public List<?> findCallHistoryByHelperLikeFlag(HelperVO helperVO, boolean likeFlag) {
		Object[] args = new Object[] {helperVO.getIdx(), likeFlag};
		
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE helperId = ? AND likeType = ?", args);
	}
	
	public List<?> findCallHistoryByHelperConnForToday(HelperVO helperVO) {
		
		Calendar todayStartCal = Calendar.getInstance();
		todayStartCal.set(Calendar.HOUR_OF_DAY, 0);
		todayStartCal.set(Calendar.MINUTE, 0);
		todayStartCal.set(Calendar.SECOND, 0);
		todayStartCal.set(Calendar.MILLISECOND, 0);
		Calendar todayEndCal = Calendar.getInstance();
		todayEndCal.add(Calendar.DATE, 1);
		todayEndCal.set(Calendar.HOUR_OF_DAY, 0);
		todayEndCal.set(Calendar.HOUR, 0);
		todayEndCal.set(Calendar.MINUTE, 0);
		todayEndCal.set(Calendar.SECOND, 0);
		todayEndCal.set(Calendar.MILLISECOND, 0);
		
		Object[] args = new Object[] {helperVO.getIdx(), todayStartCal.getTime(), todayEndCal.getTime()};
		
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE helperId = ? AND beginTime >= ? AND endTime < ? ", args);
	}
	
	public List<?> findCallHistoryByHelperLikeFlagForToday(HelperVO helperVO, boolean likeFlag) {
		
		Calendar todayStartCal = Calendar.getInstance();
		todayStartCal.set(Calendar.HOUR_OF_DAY, 0);
		todayStartCal.set(Calendar.MINUTE, 0);
		todayStartCal.set(Calendar.SECOND, 0);
		todayStartCal.set(Calendar.MILLISECOND, 0);
		Calendar todayEndCal = Calendar.getInstance();
		todayEndCal.add(Calendar.DATE, 1);
		todayEndCal.set(Calendar.HOUR_OF_DAY, 0);
		todayEndCal.set(Calendar.HOUR, 0);
		todayEndCal.set(Calendar.MINUTE, 0);
		todayEndCal.set(Calendar.SECOND, 0);
		todayEndCal.set(Calendar.MILLISECOND, 0);
		
		Object[] args = new Object[] {helperVO.getIdx(), likeFlag, todayStartCal.getTime(), todayEndCal.getTime()};
		
		return super.getHibernateTemplate().find("FROM CallHistoryVO WHERE helperId = ? AND likeType = ? AND beginTime >= ? AND endTime < ? ", args);
	}
	
}
