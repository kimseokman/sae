package egovframework.rte.rex.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.rex.service.LikeHelperVO;

/**
 * Like Helper(도우미: 선호) 정보에 관한 데이터 접근 클래스를 정의한다.
 * @see
 * 151028 | KSM | Create
 */

@Repository("likeHelperDAO")
@Transactional
public class LikeHelperDAO extends HibernateDaoSupport {
	
	/** Logger */
	private Logger logger = Logger.getLogger(LikeHelperDAO.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/**
	 * hibernate를 사용하기 위해 sessionFactory객체를 설정한다.
	 * @param sessionFactory
	 */
	@Autowired
	public void setHibernateDaoSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	/** 
	 * INSERT
	 */
	public void insertLikeHelper(LikeHelperVO likeHelperVO) throws Exception{
		super.getHibernateTemplate().save(likeHelperVO);
	}
	
	/** 
	 * SELECT
	 */
	public List<LikeHelperVO> selectLikeHelperListAll() throws Exception{
		return super.getHibernateTemplate().loadAll(LikeHelperVO.class);
	}
	
	public LikeHelperVO getLikeHelper(LikeHelperVO vo) throws Exception{
		return (LikeHelperVO) super.getHibernateTemplate().get(LikeHelperVO.class, vo.getIdx());
	}
	
	/** 
	 * UPDATE
	 */
	public void updateLikeHelper(LikeHelperVO likeHelperVO) throws Exception{
		super.getHibernateTemplate().update(likeHelperVO);
	}
	
	/**
	 * DELETE
	 */
	public void deleteLikeHelper(LikeHelperVO likeHelperVO) throws Exception{
		super.getHibernateTemplate().delete(likeHelperVO);
	}
	
	/** 
	 * SELECT
	 */
	public List<LikeHelperVO> selectLikeHelperList(String accessType, String accessToken) throws Exception{
		List<LikeHelperVO> likeHelper = null;
		
		if(accessType == "USER"){
			
		}
		else if(accessType == "HELPER"){
			likeHelper = super.getHibernateTemplate().loadAll(LikeHelperVO.class);
		}
		
		return likeHelper;
	}
	
	/**
	 * 선호 도우미 구하기
	 */
	public LikeHelperVO findLikeHelper(String userIdx, String helperIdx) throws Exception{
		Object[] args = new Object[] {userIdx, helperIdx};
		
		List likeHelper_list = super.getHibernateTemplate()
								.find("FROM LikeHelperVO WHERE user_fk = ? AND helper_fk = ?", args);
		
		LikeHelperVO likeHelperVO = new LikeHelperVO();
		
		if(likeHelper_list.size() != 0)//case by not empty
			likeHelperVO = (LikeHelperVO) likeHelper_list.get(0);
		
		return likeHelperVO;
	}
	
	/**
	 * 선호 도우미 구하기
	 */
	public List<LikeHelperVO> findLikeHelperByUser(String userIdx) throws Exception{
		
		List likeHelper_list = super.getHibernateTemplate()
								.find("FROM LikeHelperVO WHERE user_fk = ?", userIdx);
		
		return likeHelper_list;
	}
	
	public List<LikeHelperVO> findCallLikeHelper(String userIdx) throws Exception{
		Object[] args = new Object[] {userIdx, 1};
		List likeHelper_list = super.getHibernateTemplate()
				.find("FROM LikeHelperVO l WHERE l.userFk = ? AND l.helperFk IN (SELECT h.idx FROM HelperVO h WHERE h.helperStatus = ?)", args);
		
		return likeHelper_list;
	}
	
	public List<LikeHelperVO> findCallLikeHelper(String userIdx, int limit) throws Exception{
		
		int helperStatus = 1;//로그인 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperStatus, etcHelp};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List likeHelper_list = super.getHibernateTemplate()
				.find("FROM LikeHelperVO l "
						+ "WHERE l.userFk = ? "
						+ "AND l.helperFk "
						+ "IN ("
						+ "SELECT h.idx FROM HelperVO h "
						+ "WHERE h.helperStatus = ? "
						+ "AND h.etcHelp = ? "
						+ ")", 
						args);
		
		return likeHelper_list;
	}
}