package egovframework.rte.rex.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.rex.service.FamilyVO;
import egovframework.rte.rex.service.SaeFamilyService;

/**
 * Family(도우미: 가족) 관한 비즈니즈 클래스를 정의한다.
 * @see
 * 151015 | KSM | Create
 */

@Service("familyService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeFamilyServiceImpl extends AbstractServiceImpl implements SaeFamilyService {
	/** Logger */
	private Logger logger = Logger.getLogger(SaeFamilyServiceImpl.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** FamilyDAO */
    @Resource(name="familyDAO")
	private FamilyDAO familyDAO; //데이터베이스 접근 클래스
    
    /** ID Generation */
    @Resource(name="egovIdGnrServiceFamily")    
    private EgovIdGnrService egovIdGnrService; // ID Generation
	
	/**
	 * 가족 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장한다.
	 * @param familyVO
	 * @return String 목록화면
	 * @throws Exception
	 */
	public String insertFamily(FamilyVO familyVO) throws Exception{
		/** ID Generation Service */
    	String no = egovIdGnrService.getNextStringId();
    	familyVO.setIdx(no);
    	
    	familyDAO.insertFamily(familyVO);
		
		return no;
	}
	
	/**
	 * 가족 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 * @return List
	 * @throws Exception
	 */
	public List selectFamilyListAll() throws Exception{
		return familyDAO.selectFamilyListAll();
	}
	
	/**
	 * 가족 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 * @param familyVO
	 * @return FamilyVO
	 * @throws Exception
	 */
	public FamilyVO getFamily(FamilyVO familyVO) throws Exception{
		FamilyVO vo = familyDAO.getFamily(familyVO);
		if(vo==null)
			 throw processException("info.nodata.msg");
        return vo;
	}
	
	/**
	 * 화면에 조회된 가족 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 * @param familyVO
	 * @throws Exception
	 */
	public void updateFamily(FamilyVO familyVO) throws Exception{
		familyDAO.updateFamily(familyVO);
	}
	
	/**
	 * 선택된 가족 정보를 데이터베이스에서 삭제한다.
	 * @param familyVO
	 * @throws Exception
	 */
	public void deleteFamily(FamilyVO familyVO) throws Exception{
		familyDAO.deleteFamily(familyVO);
	}
	
	/**
	 * 가족 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 * @return List
	 * @throws Exception
	 */
	public List selectFamilyList(String accessType, String accessToken) throws Exception{
		return familyDAO.selectFamilyList(accessType, accessToken);
	}
	
	/**
	 * 가족 구하기
	 */
	public FamilyVO findFamily(String userIdx, String helperIdx) throws Exception{
		return familyDAO.findFamily(userIdx, helperIdx);
	}
	
	/**
	 * 도우미 가족 목록 구하기
	 */
	public List<FamilyVO> findFamilyByHelper(String helperIdx) throws Exception{
		return familyDAO.findFamilyByHelper(helperIdx);
	}
	
	/**
	 * 시각 장애인 가족 목록 구하기
	 */
	public List<FamilyVO> findFamilyByUser(String userIdx) throws Exception{
		return familyDAO.findFamilyByUser(userIdx);
	}
	
}