package egovframework.rte.rex.service.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.rex.service.AdminVO;

/**
 * Admin(관리자) 정보에 관한 데이터 접근 클래스를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

@Repository("adminDAO")
@Transactional
public class AdminDAO extends HibernateDaoSupport {
	
	/**
	 * hibernate를 사용하기 위해 sessionFactory객체를 설정한다.
	 * @param sessionFactory
	 */
	@Autowired
	public void setHibernateDaoSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	/** 
	 * INSERT admin info
	 * @param adminVO
	 * @throws Exception
	 */
	public void insertAdmin(AdminVO adminVO) throws Exception{
		super.getHibernateTemplate().save(adminVO);
	}
	
	/** 
	 * SELECT admin LIST
	 * @return List<AdminVO>
	 * @throws Exception
	 */
	public List<AdminVO> selectAdminList() throws Exception{
		return super.getHibernateTemplate().loadAll(AdminVO.class);
	}
	
	/**
	 * SELECT admin info
	 * @param vo
	 * @return AdminVO
	 * @throws Exception
	 */
	public AdminVO getAdmin(AdminVO vo) throws Exception{
		return (AdminVO) super.getHibernateTemplate().get(AdminVO.class, vo.getIdx());
	}
	
	/** 
	 * UPDATE admin info
	 * @param adminVO
	 * @throws Exception
	 */
	public void updateAdmin(AdminVO adminVO) throws Exception{
		super.getHibernateTemplate().update(adminVO);
	}
	
	/**
	 * DELETE admin info
	 * @param adminVO
	 * @throws Exception
	 */
	public void deleteAdmin(AdminVO adminVO) throws Exception{
		super.getHibernateTemplate().delete(adminVO);
	}
	
	/**
	 * SELECT admin
	 * @param vo
	 * @throws Exception
	 */
	public AdminVO findAdminByIdPassword(AdminVO vo) throws Exception{
		List<?> list = super.getHibernateTemplate().find("from AdminVO where admin_id = ?", vo.getAdminId());
		if (list.size() == 0)
			return null;

		return (AdminVO) list.get(0);
	}
}
