package egovframework.rte.rex.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.rex.service.AdminVO;
import egovframework.rte.rex.service.SaeAdminService;

/**
 * Admin(관리자) 관한 비즈니즈 클래스를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

@Service("adminService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeAdminServiceImpl extends AbstractServiceImpl implements SaeAdminService {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeAdminServiceImpl.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** AdminDAO */
    @Resource(name="adminDAO")
	private AdminDAO adminDAO; //데이터베이스 접근 클래스
    
    /** ID Generation */
    @Resource(name="egovIdGnrServiceAdmin")    
    private EgovIdGnrService egovIdGnrService; // ID Generation
    
    /**
	 *	관리자 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장한다.
	 * @param adminVO
	 * @return String 목록화면
	 * @throws Exception
	 */
	public String insertAdmin(AdminVO adminVO) throws Exception{
		
		/** ID Generation Service */
    	String no = egovIdGnrService.getNextStringId();
    	adminVO.setIdx(no);
    	
    	adminDAO.insertAdmin(adminVO);
		
		return no;
	}
	
	/**
	 * 관리자 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 * @return List
	 * @throws Exception
	 */
	public List selectAdminList() throws Exception{
		return adminDAO.selectAdminList();
	}
	
	/**
	 * 관리자 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 * @param adminVO
	 * @return AdminVO
	 * @throws Exception
	 */
	public AdminVO getAdmin(AdminVO adminVO) throws Exception{
		AdminVO vo = adminDAO.getAdmin(adminVO);
		if(vo==null)
			 throw processException("info.nodata.msg");
        return vo;
	}
	
	/**
	 * 화면에 조회된 관리자 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 * @param adminVO
	 * @throws Exception
	 */
	public void updateAdmin(AdminVO adminVO) throws Exception{
		adminDAO.updateAdmin(adminVO);
	}
	
	/**
	 * 선택된 관리자 정보를 데이터베이스에서 삭제한다.
	 * @param adminVO
	 * @throws Exception
	 */
	public void deleteAdmin(AdminVO adminVO) throws Exception{
		adminDAO.deleteAdmin(adminVO);
	}
	
	/***********************************************************************************/
	
	/**
	 * 관리자의 ID, PW로 1차 인증
	 * @param adminVO
	 * @return AdminVO
	 * @throws Exception
	 */
	public AdminVO authAdminByIdPassword(AdminVO adminVO) throws Exception{
		AdminVO vo = adminDAO.findAdminByIdPassword(adminVO);
		if (vo == null) {
			 throw processException("info.nodata.msg");
		}

        return vo;
	}
}
