package egovframework.rte.rex.service.impl;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.rex.service.HelperVO;

/**
 * Helper(도우미) 정보에 관한 데이터 접근 클래스를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

@Repository("helperDAO")
@Transactional
public class HelperDAO extends HibernateDaoSupport {
	
	/** Logger */
	private Logger logger = Logger.getLogger(HelperDAO.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/**
	 * hibernate를 사용하기 위해 sessionFactory객체를 설정한다.
	 * @param sessionFactory
	 */
	@Autowired
	public void setHibernateDaoSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	/** 
	 * INSERT helper info
	 * @param helperVO
	 * @throws Exception
	 */
	public void insertHelper(HelperVO helperVO) throws Exception{
		super.getHibernateTemplate().save(helperVO);
	}
	
	/** 
	 * SELECT helper LIST
	 * @return List<HelperVO>
	 * @throws Exception
	 */
	public List<?> selectHelperList() throws Exception{
		return super.getHibernateTemplate().find("FROM HelperVO WHERE helperType != 3");
	}

	public List<?> selectCounselorList() throws Exception {
		return super.getHibernateTemplate().find("FROM HelperVO WHERE helperType = 3");
	}

	/**
	 * SELECT helper info
	 * @param vo
	 * @return HelperVO
	 * @throws Exception
	 */
	public HelperVO getHelper(HelperVO vo) throws Exception{
		return (HelperVO) super.getHibernateTemplate().get(HelperVO.class, vo.getIdx());
	}
	
	/** 
	 * UPDATE helper info
	 * @param helperVO
	 * @throws Exception
	 */
	public void updateHelper(HelperVO helperVO) throws Exception{
		super.getHibernateTemplate().update(helperVO);
	}
	
	/**
	 * DELETE helper info
	 * @param helperVO
	 * @throws Exception
	 */
	public void deleteHelper(HelperVO helperVO) throws Exception{
		super.getHibernateTemplate().delete(helperVO);
	}
	
	/**
	 * Helper 여부 검사
	 * @param phone
	 */
	public boolean checkHelperByPhone(String phone) throws Exception{
		List helper_list = super.getHibernateTemplate()
								.find("FROM HelperVO WHERE phone = ?", phone);
		
		if(helper_list.size() != 0)//case by not empty
			return true;
		return false;
	}
	
	/**
	 * Helper 여부 검사
	 * @param phone
	 */
	public HelperVO findHelperByPhone(String phone) throws Exception{
		List helper_list = super.getHibernateTemplate()
								.find("FROM HelperVO WHERE phone = ?", phone);
		
		HelperVO helperVO = new HelperVO();
		
		if(helper_list.size() != 0)//case by not empty
			helperVO = (HelperVO) helper_list.get(0);
		
		return helperVO;
	}
	
	/**
	 * Sign Auth Helper
	 * @param phone, helper_pw
	 */
	public HelperVO signAuthHelper(String phone, String helper_pw) throws Exception{
		
		Object[] args = new Object[] {phone, helper_pw};
		
		List helper_list = super.getHibernateTemplate()
								.find("FROM HelperVO WHERE phone = ? AND helper_pw = ?", args);
		
		HelperVO helperVO = new HelperVO();
		
		if(helper_list.size() != 0)//case by not empty
			helperVO = (HelperVO) helper_list.get(0);
		
		return helperVO;
	}
	
	public List<HelperVO> agentList(int limit) throws Exception{
		
		int helperType = 3; // 상담원
		int helperStatus = 1; // 로그인 상태 (로그인)
		Object[] args = new Object[] {helperType, helperStatus};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO WHERE helper_type = ? AND helper_status = ?", args);
		
		return helper_list;
	}
	
	/**
	 * Rule 0 : RANDOM
	 * @see
	 * 2015.11.06 | KSM | 매칭룰 사용 안함
	 */
	public List<HelperVO> findHelperByRule0(String userIdx, int limit) throws Exception{
		
		int helperType = 3; // 상담원
		int helperStatus = 1; // 로그인 상태 (로그인)
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {helperType, helperStatus, etcHelp, userIdx};
		// index 0 : '3' ( 상담원 )
		// index 1 : '2' ( 로그인 )
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h WHERE "
						+ "h.helperType <> ? "														// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND h.idx "
						+ "NOT IN (SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ?)", args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 1 : RULE_TODAY_CONN_NUM_MIN_HELPER
	 * @see
	 * 2015.11.13 | KSM | 오늘 상담 수 적은 도우미
	 */
	public List<?> findHelperByRule1(String userIdx, int limit) throws Exception{
		
		Calendar todayStartCal = Calendar.getInstance();
		todayStartCal.set(Calendar.HOUR_OF_DAY, 0);
		todayStartCal.set(Calendar.MINUTE, 0);
		todayStartCal.set(Calendar.SECOND, 0);
		todayStartCal.set(Calendar.MILLISECOND, 0);
		Calendar todayEndCal = Calendar.getInstance();
		todayEndCal.add(Calendar.DATE, 1);
		todayEndCal.set(Calendar.HOUR_OF_DAY, 0);
		todayEndCal.set(Calendar.HOUR, 0);
		todayEndCal.set(Calendar.MINUTE, 0);
		todayEndCal.set(Calendar.SECOND, 0);
		todayEndCal.set(Calendar.MILLISECOND, 0);
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus, todayStartCal.getTime(), todayEndCal.getTime()};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	//선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "AND c.beginTime >= ? AND c.endTime < ? "									// 오늘 기록 중에서
						+ "GROUP BY h.idx "
						+ "ORDER BY COUNT(h.idx) ASC"												// 상담수 적은 순으로
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 2 : RULE_TODAY_CONN_NUM_MAX_HELPER
	 * @see
	 * 2015.11.13 | KSM | 오늘 상담 수 많은 도우미
	 */
	public List<?> findHelperByRule2(String userIdx, int limit) throws Exception{
		
		Calendar todayStartCal = Calendar.getInstance();
		todayStartCal.set(Calendar.HOUR_OF_DAY, 0);
		todayStartCal.set(Calendar.MINUTE, 0);
		todayStartCal.set(Calendar.SECOND, 0);
		todayStartCal.set(Calendar.MILLISECOND, 0);
		Calendar todayEndCal = Calendar.getInstance();
		todayEndCal.add(Calendar.DATE, 1);
		todayEndCal.set(Calendar.HOUR_OF_DAY, 0);
		todayEndCal.set(Calendar.HOUR, 0);
		todayEndCal.set(Calendar.MINUTE, 0);
		todayEndCal.set(Calendar.SECOND, 0);
		todayEndCal.set(Calendar.MILLISECOND, 0);
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus, todayStartCal.getTime(), todayEndCal.getTime()};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	//선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "AND c.beginTime >= ? AND c.endTime < ? "									// 오늘 기록 중에서
						+ "GROUP BY h.idx "
						+ "ORDER BY COUNT(h.idx) DESC"												// 상담수 많은 순으로
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 3 : RULE_TODAY_CONN_TIME_MIN_HELPER
	 * @see
	 * 2015.11.13 | KSM | 오늘 상담 시간 적은 도우미
	 */
	public List<?> findHelperByRule3(String userIdx, int limit) throws Exception{
		
		Calendar todayStartCal = Calendar.getInstance();
		todayStartCal.set(Calendar.HOUR_OF_DAY, 0);
		todayStartCal.set(Calendar.MINUTE, 0);
		todayStartCal.set(Calendar.SECOND, 0);
		todayStartCal.set(Calendar.MILLISECOND, 0);
		Calendar todayEndCal = Calendar.getInstance();
		todayEndCal.add(Calendar.DATE, 1);
		todayEndCal.set(Calendar.HOUR_OF_DAY, 0);
		todayEndCal.set(Calendar.HOUR, 0);
		todayEndCal.set(Calendar.MINUTE, 0);
		todayEndCal.set(Calendar.SECOND, 0);
		todayEndCal.set(Calendar.MILLISECOND, 0);
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus, todayStartCal.getTime(), todayEndCal.getTime()};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	// 선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "AND c.beginTime >= ? AND c.endTime < ? "									// 오늘 기록 중에서
						+ "GROUP BY h.idx "
						+ "ORDER BY SUM(TIMEDIFF(c.endTime, c.beginTime)) ASC"						// 인터벌 합이 적은 순
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 4 : RULE_TODAY_CONN_TIME_MAX_HELPER
	 * @see
	 * 2015.11.13 | KSM | 오늘 상담 시간 많은 도우미
	 */
	public List<?> findHelperByRule4(String userIdx, int limit) throws Exception{
		
		Calendar todayStartCal = Calendar.getInstance();
		todayStartCal.set(Calendar.HOUR_OF_DAY, 0);
		todayStartCal.set(Calendar.MINUTE, 0);
		todayStartCal.set(Calendar.SECOND, 0);
		todayStartCal.set(Calendar.MILLISECOND, 0);
		Calendar todayEndCal = Calendar.getInstance();
		todayEndCal.add(Calendar.DATE, 1);
		todayEndCal.set(Calendar.HOUR_OF_DAY, 0);
		todayEndCal.set(Calendar.HOUR, 0);
		todayEndCal.set(Calendar.MINUTE, 0);
		todayEndCal.set(Calendar.SECOND, 0);
		todayEndCal.set(Calendar.MILLISECOND, 0);
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus, todayStartCal.getTime(), todayEndCal.getTime()};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	// 선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "AND c.beginTime >= ? AND c.endTime < ? "									// 오늘 기록 중에서
						+ "GROUP BY h.idx "
						+ "ORDER BY SUM(TIMEDIFF(c.endTime, c.beginTime)) DESC"						// 인터벌 합이 많은 순
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 5 : RULE_TODAY_WAIT_TIME_MAX_HELPER
	 * @see
	 * 2015.11.13 | KSM | 오늘 대기 시간 많은 도우미
	 */
	public List<?> findHelperByRule5(String userIdx, int limit) throws Exception{
		
		Calendar todayStartCal = Calendar.getInstance();
		todayStartCal.set(Calendar.HOUR_OF_DAY, 0);
		todayStartCal.set(Calendar.MINUTE, 0);
		todayStartCal.set(Calendar.SECOND, 0);
		todayStartCal.set(Calendar.MILLISECOND, 0);
		Calendar todayEndCal = Calendar.getInstance();
		todayEndCal.add(Calendar.DATE, 1);
		todayEndCal.set(Calendar.HOUR_OF_DAY, 0);
		todayEndCal.set(Calendar.HOUR, 0);
		todayEndCal.set(Calendar.MINUTE, 0);
		todayEndCal.set(Calendar.SECOND, 0);
		todayEndCal.set(Calendar.MILLISECOND, 0);
		
		Calendar currentCal = Calendar.getInstance();
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus, todayStartCal.getTime(), todayEndCal.getTime(), currentCal.getTime()};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	// 선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "AND c.beginTime >= ? AND c.endTime < ? "									// 오늘 기록 중에서
						+ "GROUP BY h.idx "
						+ "ORDER BY SUM(TIMEDIFF( ? , h.lastLoginDate)) DESC"						// 대기시간이 많은 순
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 6 : RULE_TODAY_LIKE_MAX_HELPER
	 * @see
	 * 2015.11.13 | KSM | 오늘 좋아요 많은 도우미
	 */
	public List<?> findHelperByRule6(String userIdx, int limit) throws Exception{
		
		Calendar todayStartCal = Calendar.getInstance();
		todayStartCal.set(Calendar.HOUR_OF_DAY, 0);
		todayStartCal.set(Calendar.MINUTE, 0);
		todayStartCal.set(Calendar.SECOND, 0);
		todayStartCal.set(Calendar.MILLISECOND, 0);
		Calendar todayEndCal = Calendar.getInstance();
		todayEndCal.add(Calendar.DATE, 1);
		todayEndCal.set(Calendar.HOUR_OF_DAY, 0);
		todayEndCal.set(Calendar.HOUR, 0);
		todayEndCal.set(Calendar.MINUTE, 0);
		todayEndCal.set(Calendar.SECOND, 0);
		todayEndCal.set(Calendar.MILLISECOND, 0);
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean likeType = true; // 만족도 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus, todayStartCal.getTime(), todayEndCal.getTime(), likeType};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	// 선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "AND c.beginTime >= ? AND c.endTime < ? "									// 오늘 기록 중에서
						+ "AND c.likeType = ? "														// 만족 상태 : 좋아요
						+ "GROUP BY h.idx "
						+ "ORDER BY COUNT(h.idx) DESC"												// 많은 순으로
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 7 : RULE_TOTAL_CONN_NUM_MIN_HELPER
	 * @see
	 * 2015.11.13 | KSM | 전체 상담 수 적은 도우미
	 */
	public List<?> findHelperByRule7(String userIdx, int limit) throws Exception{
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	//선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "GROUP BY h.idx "
						+ "ORDER BY COUNT(h.idx) ASC"												// 상담수 적은 순으로
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 8 : RULE_TOTAL_CONN_NUM_MAX_HELPER
	 * @see
	 * 2015.11.13 | KSM | 전체 상담 수 많은 도우미
	 */
	public List<?> findHelperByRule8(String userIdx, int limit) throws Exception{
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	//선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "GROUP BY h.idx "
						+ "ORDER BY COUNT(h.idx) DESC"												// 상담수 많은 순으로
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 9 : RULE_TOTAL_CONN_TIME_MIN_HELPER
	 * @see
	 * 2015.11.13 | KSM | 전체 상담 시간 적은 도우미
	 */
	public List<?> findHelperByRule9(String userIdx, int limit) throws Exception{
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	//선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "GROUP BY h.idx "
						+ "ORDER BY SUM(TIMEDIFF(c.endTime, c.beginTime)) ASC"						// 인터벌 합이 적은 순
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 10 : RULE_TOTAL_CONN_TIME_MAX_HELPER
	 * @see
	 * 2015.11.13 | KSM | 전체 상담 시간 많은 도우미
	 */
	public List<?> findHelperByRule10(String userIdx, int limit) throws Exception{
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	//선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "GROUP BY h.idx "
						+ "ORDER BY SUM(TIMEDIFF(c.endTime, c.beginTime)) DESC"						// 인터벌 합이 많은 순
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 11 : RULE_TOTAL_WAIT_TIME_MAX_HELPER
	 * @see
	 * 2015.11.13 | KSM | 전체 대기 시간 많은 도우미
	 */
	public List<?> findHelperByRule11(String userIdx, int limit) throws Exception{
		
		Calendar currentCal = Calendar.getInstance();
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus, currentCal.getTime()};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	// 선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "GROUP BY h.idx "
						+ "ORDER BY SUM(TIMEDIFF( ? , h.lastLoginDate)) DESC"						// 대기시간이 많은 순
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
	/**
	 * Rule 12 : RULE_TOTAL_LIKE_MAX_HELPER
	 * @see
	 * 2015.11.13 | KSM | 전체 좋아요 많은 도우미
	 */
	public List<?> findHelperByRule12(String userIdx, int limit) throws Exception{
		
		int helperType = 3; //상담원
		int helperStatus = 1; // 로그인 상태
		int callStatus = 3; // 도움 이력 상태
		boolean likeType = true; // 만족도 상태
		boolean etcHelp = true; // 다른 시각 장애인 돕기
		Object[] args = new Object[] {userIdx, helperType, helperStatus, etcHelp, callStatus, likeType};
		
		super.getHibernateTemplate().setMaxResults(limit);
		
		List helper_list = super.getHibernateTemplate()
				.find("FROM HelperVO h, CallHistoryVO c "
						+ "WHERE h.idx = c.helperId "
						+ "AND h.idx "
						+ "NOT IN ( SELECT l.helperFk FROM LikeHelperVO l WHERE l.userFk = ? ) "	//선호 도우미가 아니고
						+ "AND h.helperType <> ? "													// 상담원 아니고
						+ "AND h.helperStatus = ? "													// 로그인 상태이면서
						+ "AND h.etcHelp = ? "														// 다른 시각 장애인 도와주려는
						+ "AND c.status = ? "														// 정상종료 된
						+ "AND c.likeType = ? "														// 만족 상태 : 좋아요
						+ "GROUP BY h.idx "
						+ "ORDER BY COUNT(h.idx) DESC"												// 많은 순으로
						, args);//선호 도우미가 아닌 도우미
		
		return helper_list;
		
	}
	
}