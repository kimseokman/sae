package egovframework.rte.rex.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.rex.service.SaeSystemService;
import egovframework.rte.rex.service.SystemVO;

/**
 * System(시스템) 관한 비즈니즈 클래스를 정의한다.
 * @see
 * 151030 | KSM | Create
 */

@Service("systemService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeSystemServiceImpl extends AbstractServiceImpl implements SaeSystemService {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeSystemServiceImpl.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** systemDAO */
    @Resource(name="systemDAO")
	private SystemDAO systemDAO; //데이터베이스 접근 클래스
	
	/**
	 * 시스템 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 */
	public SystemVO getSystem(SystemVO systemVO) throws Exception{
		SystemVO vo = systemDAO.getSystem(systemVO);
		if(vo==null)
			 throw processException("info.nodata.msg");
        return vo;
	}
	
	/**
	 * 화면에 조회된 시스템 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 */
	public void updateSystem(SystemVO systemVO) throws Exception{
		systemDAO.updateSystem(systemVO);
	}
	

	public List<?> selectSystemList() throws Exception {
		return systemDAO.selectSystemList();
	}
	
}