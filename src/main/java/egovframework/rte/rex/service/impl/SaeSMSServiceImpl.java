package egovframework.rte.rex.service.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.rex.service.SMSVO;
import egovframework.rte.rex.service.SaeSMSService;

/**
 * Like Helper (도우미: 선호) 관한 비즈니즈 클래스를 정의한다.
 * @see
 * 151028 | KSM | Create
 */

@Service("smsService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeSMSServiceImpl extends AbstractServiceImpl implements SaeSMSService {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeLikeHelperServiceImpl.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** SMSDAO */
    @Resource(name="smsDAO")
	private SMSDAO smsDAO; //데이터베이스 접근 클래스
    
	
	public void insertSMS(SMSVO smsVO) throws Exception{
		smsDAO.insertSMS(smsVO);
	}
	
	public SMSVO findSMS(String phone) throws Exception{
		SMSVO vo = smsDAO.findSMS(phone);
		
        return vo;
	}

	public void updateSMS(SMSVO smsVO) throws Exception{
		smsDAO.updateSMS(smsVO);
	}
	
	public void deleteSMS(SMSVO smsVO) throws Exception{
		smsDAO.deleteSMS(smsVO);
	}
	
}