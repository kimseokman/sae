package egovframework.rte.rex.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.SaeHelperService;

/**
 * Helper(도우미) 관한 비즈니즈 클래스를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

@Service("helperService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeHelperServiceImpl extends AbstractServiceImpl implements SaeHelperService {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeHelperServiceImpl.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** HelperDAO */
    @Resource(name="helperDAO")
	private HelperDAO helperDAO; //데이터베이스 접근 클래스
    
    /** ID Generation */
    @Resource(name="egovIdGnrServiceHelper")    
    private EgovIdGnrService egovIdGnrService; // ID Generation
    
    /**
	 *	사용자 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장한다.
	 * @param helperVO
	 * @return String 목록화면
	 * @throws Exception
	 */
	public String insertHelper(HelperVO helperVO) throws Exception{
		
		/** ID Generation Service */
    	String no = egovIdGnrService.getNextStringId();
    	helperVO.setIdx(no);
    	
    	helperDAO.insertHelper(helperVO);
		
		return no;
	}
	
	/**
	 * 사용자 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 * @return List
	 * @throws Exception
	 */
	public List selectHelperList() throws Exception{
		return helperDAO.selectHelperList();
	}

	public List<?> selectCounselorList() throws Exception {
		return helperDAO.selectCounselorList();
	}

	/**
	 * 사용자 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 * @param helperVO
	 * @return HelperVO
	 * @throws Exception
	 */
	public HelperVO getHelper(HelperVO helperVO) throws Exception{
		HelperVO vo = helperDAO.getHelper(helperVO);
		if(vo==null)
			 throw processException("info.nodata.msg");
        return vo;
	}
	
	/**
	 * 화면에 조회된 사용자 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 * @param helperVO
	 * @throws Exception
	 */
	public void updateHelper(HelperVO helperVO) throws Exception{
		helperDAO.updateHelper(helperVO);
	}
	
	/**
	 * 선택된 사용자 정보를 데이터베이스에서 삭제한다.
	 * @param helperVO
	 * @throws Exception
	 */
	public void deleteHelper(HelperVO helperVO) throws Exception{
		helperDAO.deleteHelper(helperVO);
	}
	
	/**
	 * Check Helper Phone
	 */
	public boolean checkHelperByPhone(String phone) throws Exception{
		return helperDAO.checkHelperByPhone(phone);
	}
	
	/**
	 * Find Helper Phone
	 */
	public HelperVO findHelperByPhone(String phone) throws Exception{
		return helperDAO.findHelperByPhone(phone);
	}
	
	/**
	 * Sign Auth Helper
	 */
	public HelperVO signAuthHelper(String phone, String helper_pw) throws Exception{
		return helperDAO.signAuthHelper(phone, helper_pw);
	}
	
	public List<HelperVO> agentList(int limit) throws Exception{
		return helperDAO.agentList(limit);
	}
	
	public List<HelperVO> findHelperByRule0(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule0(userIdx, limit);
	}
	public List<?> findHelperByRule1(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule1(userIdx, limit);
	}
	public List<?> findHelperByRule2(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule2(userIdx, limit);
	}
	public List<?> findHelperByRule3(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule3(userIdx, limit);
	}
	public List<?> findHelperByRule4(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule4(userIdx, limit);
	}
	public List<?> findHelperByRule5(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule5(userIdx, limit);
	}
	public List<?> findHelperByRule6(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule6(userIdx, limit);
	}
	public List<?> findHelperByRule7(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule7(userIdx, limit);
	}
	public List<?> findHelperByRule8(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule8(userIdx, limit);
	}
	public List<?> findHelperByRule9(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule9(userIdx, limit);
	}
	public List<?> findHelperByRule10(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule10(userIdx, limit);
	}
	public List<?> findHelperByRule11(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule11(userIdx, limit);
	}
	public List<?> findHelperByRule12(String userIdx, int limit) throws Exception{
		return helperDAO.findHelperByRule12(userIdx, limit);
	}
	
}