package egovframework.rte.rex.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.rex.service.SaeCurrentCallService;

@Service("currentCallService")
@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
public class SaeCurrentCallServiceImpl extends AbstractServiceImpl implements SaeCurrentCallService {
	@Resource(name="currentCallDAO")
	private CurrentCallDAO currentCallDAO;
}
