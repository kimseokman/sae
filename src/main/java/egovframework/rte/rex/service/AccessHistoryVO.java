package egovframework.rte.rex.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "t_accesshistory")
public class AccessHistoryVO implements Serializable {
	private static final long serialVersionUID = 6230317603534564281L;

	@Id
	@Column(name="idx")
	private String idx;

	@Column(name="admin_id")
	private String adminId;

	@Column(name="access_type")
	private int accessType;

	@Temporal(TemporalType.DATE)
	@Column(name="access_time")
	private Date accessTime;
	
	@Column(name="ip_addr")
	private String ipAddress;

	@Column(name="os_ver")
	private String osVersion;

	@Column(name="browser_ver")
	private String browserVersion;

	public String getIdx(){
		return idx;
	}

	public void setIdx(String idx){
		this.idx = idx;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public int getAccessType() {
		return accessType;
	}

	public void setAccessType(int accessType) {
		this.accessType = accessType;
	}

	public Date getAccessTime() {
		return accessTime;
	}

	public void setAccessTime(Date accessTime) {
		this.accessTime = accessTime;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getBrowserVersion() {
		return browserVersion;
	}

	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}
}
