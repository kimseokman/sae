package egovframework.rte.rex.service;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * System(시스템) 정보 클래스를 정의한다.
 * 
 * @see 151012 | KSM | Create
 */

@Entity
@Table(name = "t_system")
@XmlRootElement
public class SystemVO implements Serializable {

	private static final long serialVersionUID = 8818092392886013219L;
	
	@Id
	@Column(name = "idx")
	private String idx;
	
	@Column(name = "routing_rule")
	private int routingRule;
	
	@Column(name = "beginner_conn_num")
	private int beginnerConnNum;
	
	@Column(name = "beginner_like_num")
	private int beginnerLikeNum;
	
	@Column(name = "expert_conn_num")
	private int expertConnNum;
	
	@Column(name = "expert_like_num")
	private int expertLikeNum;
	
	@Column(name = "master_conn_num")
	private int masterConnNum;
	
	@Column(name = "master_like_num")
	private int masterLikeNum;
	
	@Column(name = "vip_conn_num")
	private int vipConnNum;
	
	@Column(name = "vip_like_num")
	private int vipLikeNum;
	
	@Column(name = "sip_ip")
	private String sipIp;
	
	@Column(name = "sip_port")
	private int sipPort;
	
	@Column(name = "sip_codec")
	private String sipCodec;
	
	@Column(name = "sip_framerate")
	private int sipFramerate;
	
	@Column(name = "sip_bitrate")
	private int sipBitrate;
	
	@Column(name = "sip_resolution")
	private String sipResolution;
	
	@Column(name = "like_helper_num")
	private int likeHelperNum;
	
	@Column(name = "helper_num")
	private int helperNum;
	
	@Column(name = "agent_num")
	private int agentNum;

	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}

	public int getRoutingRule() {
		return routingRule;
	}

	public void setRoutingRule(int routingRule) {
		this.routingRule = routingRule;
	}

	public int getBeginnerConnNum() {
		return beginnerConnNum;
	}

	public void setBeginnerConnNum(int beginnerConnNum) {
		this.beginnerConnNum = beginnerConnNum;
	}

	public int getBeginnerLikeNum() {
		return beginnerLikeNum;
	}

	public void setBeginnerLikeNum(int beginnerLikeNum) {
		this.beginnerLikeNum = beginnerLikeNum;
	}

	public int getExpertConnNum() {
		return expertConnNum;
	}

	public void setExpertConnNum(int expertConnNum) {
		this.expertConnNum = expertConnNum;
	}

	public int getExpertLikeNum() {
		return expertLikeNum;
	}

	public void setExpertLikeNum(int expertLikeNum) {
		this.expertLikeNum = expertLikeNum;
	}

	public int getMasterConnNum() {
		return masterConnNum;
	}

	public void setMasterConnNum(int masterConnNum) {
		this.masterConnNum = masterConnNum;
	}

	public int getMasterLikeNum() {
		return masterLikeNum;
	}

	public void setMasterLikeNum(int masterLikeNum) {
		this.masterLikeNum = masterLikeNum;
	}

	public int getVipConnNum() {
		return vipConnNum;
	}

	public void setVipConnNum(int vipConnNum) {
		this.vipConnNum = vipConnNum;
	}

	public int getVipLikeNum() {
		return vipLikeNum;
	}

	public void setVipLikeNum(int vipLikeNum) {
		this.vipLikeNum = vipLikeNum;
	}

	public String getSipIp() {
		return sipIp;
	}

	public void setSipIp(String sipIp) {
		this.sipIp = sipIp;
	}

	public int getSipPort() {
		return sipPort;
	}

	public void setSipPort(int sipPort) {
		this.sipPort = sipPort;
	}

	public String getSipCodec() {
		return sipCodec;
	}

	public void setSipCodec(String sipCodec) {
		this.sipCodec = sipCodec;
	}

	public int getSipFramerate() {
		return sipFramerate;
	}

	public void setSipFramerate(int sipFramerate) {
		this.sipFramerate = sipFramerate;
	}

	public int getSipBitrate() {
		return sipBitrate;
	}

	public void setSipBitrate(int sipBitrate) {
		this.sipBitrate = sipBitrate;
	}

	public String getSipResolution() {
		return sipResolution;
	}

	public void setSipResolution(String sipResolution) {
		this.sipResolution = sipResolution;
	}

	public int getLikeHelperNum() {
		return likeHelperNum;
	}

	public void setLikeHelperNum(int likeHelperNum) {
		this.likeHelperNum = likeHelperNum;
	}

	public int getHelperNum() {
		return helperNum;
	}

	public void setHelperNum(int helperNum) {
		this.helperNum = helperNum;
	}

	public int getAgentNum() {
		return agentNum;
	}

	public void setAgentNum(int agentNum) {
		this.agentNum = agentNum;
	}
	
}