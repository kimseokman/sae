package egovframework.rte.rex.service;

import java.util.List;


/**
 * System(시스템) 정보 처리에 대한 Class 형태를 정의한다.
 * @see
 * 151030 | KSM | Create
 */

public interface SaeSystemService {
	
	/**
	 * 시스템 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 */
	public SystemVO getSystem(SystemVO systemVO) throws Exception;
	
	/**
	 * 화면에 조회된 시스템 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 */
	public void updateSystem(SystemVO systemVO) throws Exception;
	
	public List<?> selectSystemList() throws Exception;

	
}