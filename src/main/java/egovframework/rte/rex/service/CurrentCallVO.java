package egovframework.rte.rex.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "t_currentcall")
@XmlRootElement
public class CurrentCallVO implements Serializable {
	private static final long serialVersionUID = 753952434586305123L;

	@Id
	@Column(name="idx")
	private String idx;

	@Temporal(TemporalType.DATE)
	@Column(name="connected_time")
	private Date connectedDate;

	@Column(name="user_id")
	private String userId;

	@Column(name="helper_id")
	private String helperId;

	public String getIdx(){
		return idx;
	}

	public void setIdx(String idx){
		this.idx = idx;
	}

	public Date getConnectedDate() {
		return connectedDate;
	}

	public void setConnectedDate(Date connectedDate) {
		this.connectedDate = connectedDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getHelperId() {
		return helperId;
	}

	public void setHelperId(String helperId) {
		this.helperId = helperId;
	}
}
