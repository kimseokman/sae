package egovframework.rte.rex.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Like Helper (도우미: 선호) 정보 클래스를 정의한다.
 * @see
 * 151028 | KSM | Create
 */

@Entity
@Table(name = "t_usernhelper_like")
@XmlRootElement
public class LikeHelperVO implements Serializable{

	private static final long serialVersionUID = 8352336503181614631L;
	
	@Id
	@Column(name="idx")
	private String idx;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created")
	private Date createdDate;

	@Column(name="user_fk")
	private String userFk;
	
	@Column(name="helper_fk")
	private String helperFk;
	
	@ManyToOne
	@JoinColumn(name="user_fk", referencedColumnName = "idx", insertable = false, updatable = false)
	private UserVO user;
	
	@ManyToOne
	@JoinColumn(name="helper_fk", referencedColumnName = "idx", insertable = false, updatable = false)
	private HelperVO helper;

	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUserFk() {
		return userFk;
	}

	public void setUserFk(String userFk) {
		this.userFk = userFk;
	}

	public String getHelperFk() {
		return helperFk;
	}

	public void setHelperFk(String helperFk) {
		this.helperFk = helperFk;
	}

	public UserVO getUser() {
		return user;
	}

	public void setUser(UserVO user) {
		this.user = user;
	}

	public HelperVO getHelper() {
		return helper;
	}

	public void setHelper(HelperVO helper) {
		this.helper = helper;
	}
	
	
}