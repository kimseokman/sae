package egovframework.rte.rex.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Helper(도우미) 정보 클래스를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

@Entity
@Table(name = "t_helpers")
@XmlRootElement
public class HelperVO implements Serializable{

	private static final long serialVersionUID = 9148469410405345055L;
	
	@Id
	@Column(name="idx")
	private String idx;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lastlogin")
	private Date lastLoginDate;
	
	@Column(name="helper_id")
	private String helperId;
	
	@Column(name="helper_pw")
	private String helperPassword;
	
	@Column(name="name")
	private String name;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="nickname")
	private String nickname;

	@Column(name="email")
	private String email;

	@Column(name="helper_type")
	private int helperType;

	@Column(name="helper_group")
	private String helperGroup;
	
	@Column(name="helper_status")
	private int helperStatus;
	
	@Column(name="register_status")
	private boolean registerStatus;
	
	@Column(name="like_num")
	private int likeNum;
	
	@Column(name="conn_num")
	private int connNum;
	
	@Column(name="rank")
	private int rank;
	
	@Column(name="etc_help")
	private boolean etcHelp;

	@Column(name="admin_fk")
	private String adminFk;

	public String getIdx(){
		return idx;
	}
	
	public void setIdx(String idx){
		this.idx = idx;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	
	public String getHelperId(){
		return helperId;
	}
	
	public void setHelperId(String helperId){
		this.helperId = helperId;
	}
	
	public String getHelperPassword(){
		return helperPassword;
	}
	
	public void setHelperPassword(String helperPassword){
		this.helperPassword = helperPassword;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getNickname() {
		return nickname;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getHelperType() {
		return helperType;
	}

	public void setHelperType(int helperType) {
		this.helperType = helperType;
	}

	public String getHelperGroup() {
		return helperGroup;
	}
	
	public void setHelperGroup(String helperGroup) {
		this.helperGroup = helperGroup;
	}
	
	public int getHelperStatus() {
		return helperStatus;
	}

	public void setHelperStatus(int helperStatus) {
		this.helperStatus = helperStatus;
	}

	public boolean isRegisterStatus() {
		return registerStatus;
	}

	//public boolean getRegisterStatus() {
	//	return registerStatus;
	//}
	
	public void setRegisterStatus(boolean registerStatus) {
		this.registerStatus = registerStatus;
	}
	
	public int getLikeNum() {
		return likeNum;
	}

	public void setLikeNum(int likeNum) {
		this.likeNum = likeNum;
	}

	public int getConnNum() {
		return connNum;
	}

	public void setConnNum(int connNum) {
		this.connNum = connNum;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public boolean isEtcHelp() {
		return etcHelp;
	}

	public void setEtcHelp(boolean etcHelp) {
		this.etcHelp = etcHelp;
	}

	public String getAdminFk() {
		return adminFk;
	}

	public void setAdminFk(String adminFk) {
		this.adminFk = adminFk;
	}
	
}