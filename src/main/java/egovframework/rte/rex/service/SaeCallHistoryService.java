package egovframework.rte.rex.service;

import java.util.List;

public interface SaeCallHistoryService {
	public List<?> selectCallHistoryList() throws Exception;
	public List<?> findCallHistoryByUser(UserVO userVO) throws Exception;
	public List<?> findCallHistoryByHelper(HelperVO helperVO) throws Exception;
	public List<?> findCallHistoryByUser(UserVO userVO, int limit) throws Exception;
	public List<?> findCallHistoryByHelper(HelperVO helperVO, int limit) throws Exception;
	public List<?> findCallHistoryByHelperConn(HelperVO helperVO) throws Exception;
	public List<?> findCallHistoryByHelperLikeFlag(HelperVO helperVO, boolean likeFlag) throws Exception;
	public List<?> findCallHistoryByHelperConnForToday(HelperVO helperVO) throws Exception;
	public List<?> findCallHistoryByHelperLikeFlagForToday(HelperVO helperVO, boolean likeFlag) throws Exception;
	
	public List<?> selectSuccessList() throws Exception;
	public List<?> selectSatisfactionList() throws Exception;
	public List<?> selectSuccessCallList() throws Exception;
	public List<?> selectAskingCallList() throws Exception;
	public List<?> askingUserList() throws Exception;
	
	public int getMeanAskingTime() throws Exception;
	public int getMeanHelpingTime() throws Exception;
	
	public String insertCallHistory(CallHistoryVO callHistoryVO) throws Exception;
	public CallHistoryVO getCallHistory(CallHistoryVO callHistoryVO) throws Exception;
	public void updateCallHistory(CallHistoryVO callHistoryVO) throws Exception;
	public void deleteCallHistory(CallHistoryVO callHistoryVO) throws Exception;
	
	
}
