package egovframework.rte.rex.service;

import java.util.List;

/**
 * Like Helper (도우미: 선호) 정보 처리에 대한 Class 형태를 정의한다.
 * @see
 * 151028 | KSM | Create
 */

public interface SaeLikeHelperService {
	
	/**
	 * 선호 도우미 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장한다.
	 */
	public String insertLikeHelper(LikeHelperVO likeHelperVO) throws Exception;
	
	/**
	 * 선호 도우미 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 */
	public List selectLikeHelperListAll() throws Exception;
	
	/**
	 * 선호 도우미 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 */
	public LikeHelperVO getLikeHelper(LikeHelperVO likeHelperVO) throws Exception;
	
	/**
	 * 화면에 조회된 선호 도우미 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 */
	public void updateLikeHelper(LikeHelperVO likeHelperVO) throws Exception;
	
	/**
	 * 선택된 선호 도우미 정보를 데이터베이스에서 삭제한다.
	 */
	public void deleteLikeHelper(LikeHelperVO likeHelperVO) throws Exception;
	
	/**
	 * 선호 도우미 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 */
	public List selectLikeHelperList(String accessType, String accessToken) throws Exception;
	
	/**
	 * 선호 도우미 구하기
	 */
	public LikeHelperVO findLikeHelper(String userIdx, String helperIdx) throws Exception;
	
	/**
	 * 시각 장애인 선호 도우미 목록 구하기
	 */
	public List<LikeHelperVO> findLikeHelperByUser(String userIdx) throws Exception;
	
	public List<LikeHelperVO> findCallLikeHelper(String userIdx) throws Exception;
	
	public List<LikeHelperVO> findCallLikeHelper(String userIdx, int limit) throws Exception;
}