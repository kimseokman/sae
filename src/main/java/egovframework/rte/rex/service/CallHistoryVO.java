package egovframework.rte.rex.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "t_callhistory")
@XmlRootElement
public class CallHistoryVO implements Serializable{
	private static final long serialVersionUID = 4309237408776601865L;

	@Id
	@Column(name="idx")
	private String idx;

	@Column(name="user_id")
	private String userId;

	@Column(name="helper_id")
	private String helperId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="request_time")
	private Date requestTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="begin_time")
	private Date beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_time")
	private Date endTime;

	@Column(name="support_type")
	private int supportType;

	@Column(name="like_type")
	private boolean likeType;
	
	@Column(name="status")
	private int status;
	
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName = "idx", insertable = false, updatable = false)
	private UserVO user;
	
	@ManyToOne
	@JoinColumn(name="helper_id", referencedColumnName = "idx", insertable = false, updatable = false)
	private HelperVO helper;

	public String getIdx(){
		return idx;
	}

	public void setIdx(String idx){
		this.idx = idx;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getHelperId() {
		return helperId;
	}

	public void setHelperId(String helperId) {
		this.helperId = helperId;
	}

	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getSupportType() {
		return supportType;
	}

	public void setSupportType(int supportType) {
		this.supportType = supportType;
	}
	
	public boolean isLikeType() {
		return likeType;
	}

	public void setLikeType(boolean likeType) {
		this.likeType = likeType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	//foreign data
	public UserVO getUser() {
        return user;
    }
 
    public void setUser(UserVO user) {
        this.user = user;
    }
    
    public HelperVO getHelper() {
        return helper;
    }
 
    public void setHelper(HelperVO helper) {
        this.helper = helper;
    }
	
}
