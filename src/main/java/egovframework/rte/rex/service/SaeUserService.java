package egovframework.rte.rex.service;

import java.util.List;

/**
 * User(시각 장애인) 정보 처리에 대한 Class 형태를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

public interface SaeUserService {
	/**
	 *	사용자 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장한다.
	 * @param userVO
	 * @return String 목록화면
	 * @throws Exception
	 */
	public String insertUser(UserVO userVO) throws Exception;
	
	/**
	 * 사용자 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 * @return List
	 * @throws Exception
	 */
	public List selectUserList() throws Exception;
	
	/**
	 * 사용자 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 * @param userVO
	 * @return UserVO
	 * @throws Exception
	 */
	public UserVO getUser(UserVO userVO) throws Exception;
	
	/**
	 * 화면에 조회된 사용자 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 * @param userVO
	 * @throws Exception
	 */
	public void updateUser(UserVO userVO) throws Exception;
	
	/**
	 * 선택된 사용자 정보를 데이터베이스에서 삭제한다.
	 * @param userVO
	 * @throws Exception
	 */
	public void deleteUser(UserVO userVO) throws Exception;
	
	/**
	 * 사용자 찾기
	 */
	public List<?> findUsers(String key, String value, int page) throws Exception;
	public UserVO findUserByPhone(String phone) throws Exception;
	public List<?> findAskingUsers() throws Exception;
}