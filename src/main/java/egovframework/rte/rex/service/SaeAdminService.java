package egovframework.rte.rex.service;

import java.util.List;

/**
 * Admin(관리자) 정보 처리에 대한 Class 형태를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

public interface SaeAdminService {
	/**
	 *	관리자 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장한다.
	 * @param adminVO
	 * @return String 목록화면
	 * @throws Exception
	 */
	public String insertAdmin(AdminVO adminVO) throws Exception;
	
	/**
	 * 관리자 전체 목록을 데이터베이스에서 읽어와 화면에 출력한다.
	 * @return List
	 * @throws Exception
	 */
	public List selectAdminList() throws Exception;
	
	/**
	 * 관리자 정보를 데이터베이스에서 읽어와 화면에 출력한다.
	 * @param adminVO
	 * @return AdminVO
	 * @throws Exception
	 */
	public AdminVO getAdmin(AdminVO adminVO) throws Exception;
	
	/**
	 * 화면에 조회된 관리자 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영한다.
	 * @param adminVO
	 * @throws Exception
	 */
	public void updateAdmin(AdminVO adminVO) throws Exception;
	
	/**
	 * 선택된 관리자 정보를 데이터베이스에서 삭제한다.
	 * @param adminVO
	 * @throws Exception
	 */
	public void deleteAdmin(AdminVO adminVO) throws Exception;
	
	/***************************************************************************/
	
	/**
	 * 관리자의 ID, PW로 1차 인증
	 * @param adminVO
	 * @return AdminVO
	 * @throws Exception
	 */
	public AdminVO authAdminByIdPassword(AdminVO adminVO) throws Exception;
}
