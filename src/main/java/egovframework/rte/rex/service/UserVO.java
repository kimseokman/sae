package egovframework.rte.rex.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * User(시각 장애인) 정보 클래스를 정의한다.
 * 
 * @see 151012 | KSM | Create
 */

@Entity
@Table(name = "t_users")
@XmlRootElement
public class UserVO implements Serializable {

	private static final long serialVersionUID = -5169690342675184563L;

	@Id
	@Column(name = "idx")
	private String idx;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created")
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastlogin")
	private Date lastLoginDate;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "user_pw")
	private String userPassword;

	@Column(name = "name")
	private String name;
	
	@Column(name = "nickname")
	private String nickname;

	@Column(name = "phone")
	private String phone;

	@Column(name = "email")
	private String email;

	@Column(name = "sex")
	private boolean sex;

	@Column(name = "birth")
	private String birth;

	@Column(name = "disabled_num")
	private String disabledNumber;

	@Column(name = "device_sn")
	private String deviceSN;
	
	@Column(name = "device_uuid")
	private String deviceUUID;

	@Column(name = "user_group")
	private String userGroup;

	@Column(name = "user_status")
	private int userStatus;
	
	@Column(name = "register_status")
	private boolean registerStatus;

	@Column(name = "admin_fk")
	private String adminFk;

	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isSex() {
		return sex;
	}

	public void setSex(boolean sex) {
		this.sex = sex;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getDisabledNumber() {
		return disabledNumber;
	}

	public void setDisabledNumber(String disabledNumber) {
		this.disabledNumber = disabledNumber;
	}

	public String getDeviceSN() {
		return deviceSN;
	}

	public void setDeviceSN(String deviceSN) {
		this.deviceSN = deviceSN;
	}

	public String getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}

	public int getUserStatus() {
		return userStatus;
	}
	
	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}
	
	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public boolean isRegisterStatus() {
		return registerStatus;
	}

	public void setRegisterStatus(boolean registerStatus) {
		this.registerStatus = registerStatus;
	}

	public String getAdminFk() {
		return adminFk;
	}
	
	public void setAdminFk(String adminFk) {
		this.adminFk = adminFk;
	}

	public String getDeviceUUID() {
		return deviceUUID;
	}

	public void setDeviceUUID(String deviceUUID) {
		this.deviceUUID = deviceUUID;
	}
	
}