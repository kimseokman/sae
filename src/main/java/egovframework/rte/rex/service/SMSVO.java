package egovframework.rte.rex.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * SMS(인증 관련) 정보 클래스를 정의한다.
 * 
 * @see 151111 | KSM | Create
 */

@Entity
@Table(name = "t_sms")
@XmlRootElement
public class SMSVO implements Serializable {

	private static final long serialVersionUID = 2523159769692924826L;
	
	@Id
	@Column(name = "phone")
	private String phone;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created")
	private Date created;
	
	@Column(name = "code")
	private String code;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}