package egovframework.rte.rex.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import egovframework.rte.rex.service.SaeUserService;
import egovframework.rte.rex.service.UserVO;

/**
 * User(시각 장애인) 정보를 관리하는 컨트롤러 클래스를 정의한다.
 * @see
 * 151012 | KSM | Create
 */

@Controller
@SessionAttributes(types=UserVO.class)
public class SaeUserController {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeUserController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	/** UserService */
	@Resource(name="userService")
	private SaeUserService userService;
	
	/** 
	 * 사용자 목록을 출력한다.
	 */
	@RequestMapping(value="/springrest/user", method=RequestMethod.GET)
	public String selectUsersList(HttpServletRequest request, Model model) throws Exception {
		List userList = userService.selectUserList();
		
		List user1 = new ArrayList();
		
		for( int i=userList.size()-1; i >=0; i--){
			user1.add(userList.get(i));
		}
		
		model.addAttribute("userList", user1);
		
		return "user/saeUserList";
	}
	
	/** 
	 * 사용자 등록 화면으로 이동한다.
	 */
	@RequestMapping(value="/springrest/user/new", method=RequestMethod.GET)
    public String insertUserView(Model model) throws Exception {
    	model.addAttribute("userVO", new UserVO());
    	
    	return "user/saeUserRegister";
    }
	
	/**
	 * 사용자 정보 저장 후 목록조회 화면으로 이동한다.
	 * @param userVO 카테고리 정보
	 * @param results validation결과
	 * @param session 세션 정보
	 * @param model
	 * @return "redirect:/springrest/user.html"
	 * @throws Exception
	 */
	@RequestMapping(value="/springrest/user", method = RequestMethod.POST, headers = "Content-type=application/x-www-form-urlencoded")
	public String create(@Valid UserVO userVO, BindingResult results,
			HttpSession session, Model model) throws Exception {
		if (results.hasErrors()) {
			return "user/saeUserRegister";
		}
		
		//userVO.setAdminFk('admin_000000');//hard code
		
		userService.insertUser(userVO);

		return "redirect:/springrest/user.html";
	}
	
	/**
	 * 사용자 정보 수정 화면으로 이동한다.
	 * @param userId 사용자 ID
	 * @param model
	 * @return "user/saeUserRegister"
	 * @throws Exception
	 */
	@RequestMapping(value="/springrest/user/{id}", method = RequestMethod.GET)
	public String updtUserView(@PathVariable String id, Model model)
			throws Exception {
		UserVO vo = new UserVO();
		
		vo.setIdx(id);
		model.addAttribute(userService.getUser(vo));
		return "user/saeUserRegister";
	}
}