package egovframework.rte.rex.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import antlr.collections.List;
import egovframework.rte.rex.api.SaeBlindSignController;
import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;
import egovframework.rte.rex.service.AdminVO;
import egovframework.rte.rex.service.HelperVO;
import egovframework.rte.rex.service.SaeAccessHistoryService;
import egovframework.rte.rex.service.SaeAdminService;
import egovframework.rte.rex.service.SaeCallHistoryService;
import egovframework.rte.rex.service.SaeCurrentCallService;
import egovframework.rte.rex.service.SaeHelperService;
import egovframework.rte.rex.service.SaeSystemService;
import egovframework.rte.rex.service.SaeUserService;
import egovframework.rte.rex.service.UserVO;

@Controller
@SessionAttributes(value={"user", "volunteer", "counselor"})
public class SaeAdminController {
	
	/** Logger */
	private Logger logger = Logger.getLogger(SaeAdminController.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	@Resource
	private SaeAdminService adminService;

	@Resource(name="helperService")
	private SaeHelperService helperService;

	@Resource(name="userService")
	private SaeUserService userService;

	@Resource(name="currentCallService")
	private SaeCurrentCallService currentCallService;

	@Resource(name="callHistoryService")
	private SaeCallHistoryService callHistoryService;

	@Resource(name="accessHistoryService")
	private SaeAccessHistoryService accessHistoryService;
	
	@Resource(name="systemService")
	private SaeSystemService systemService;
	
	

	/** common */
	private TokenManager tokenManager = new TokenManager();

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		chain.doFilter(request, response);
	};

	@RequestMapping(value = "/springrest/admin/login", method = RequestMethod.GET)
	public String login() {
		return "admin/login";
	}

	@RequestMapping(value = "/springrest/admin/logout", method = RequestMethod.POST)
	public String logout(HttpServletResponse response) {
		Cookie cookie = new Cookie("token", "");
		cookie.setMaxAge(0);
		response.addCookie(cookie);
		return "redirect:/springrest/admin/login.html";
	}

	@RequestMapping(value = "/springrest/admin/access", method = RequestMethod.POST)
	public String access(HttpServletResponse response,
			@RequestParam("login_id") String id,
			@RequestParam("login_pw") String pw, Model model) throws Exception {
		AdminVO vo = new AdminVO();
		vo.setAdminId(id);
		vo.setAdminPassword(pw);
		try {
			vo = adminService.authAdminByIdPassword(vo);
		} catch (Exception e) {
			vo = null;
		}

		if (vo != null) {
			
			String newToken = tokenManager.createToken(vo.getIdx(), SaeConfig.SAE_SERVER_NAME, SaeConfig.TOKEN_SECRET_KEY);

			response.addCookie(new Cookie("token", newToken));
			return "redirect:/springrest/admin/monitoring/today.html";
		}

		return "redirect:/springrest/admin/login.html";
	}

	@RequestMapping(value = "/springrest/admin/monitoring/today", method = RequestMethod.GET)
	public String today(Model model) throws Exception {
		//성공율
		model.addAttribute("successCall", callHistoryService.selectSuccessCallList());
		model.addAttribute("askingCall", callHistoryService.selectAskingCallList());
		
		//만족율
		model.addAttribute("success", callHistoryService.selectSuccessList());
		model.addAttribute("satisfaction", callHistoryService.selectSatisfactionList());
		
		//평균 요청 시간
		model.addAttribute("meanAskingTime", callHistoryService.getMeanAskingTime());
		
		//평균 도움 시간
		model.addAttribute("meanHelpingTime", callHistoryService.getMeanHelpingTime());
		
		//대기중인 자원봉사자
		model.addAttribute("waitingHelpers", helperService.selectHelperList());
		
		//도움 요청 중인 시각장애인
		model.addAttribute("users", userService.selectUserList());
		model.addAttribute("askingUsers", callHistoryService.askingUserList()); //PROBLEM
		
		//연결 세션
		model.addAttribute("connectSession", callHistoryService.selectCallHistoryList()); //FIXME '연결중'인지 체크 안하고있음
		return "admin/monitoring/today";
	}

	@RequestMapping(value = "/springrest/admin/monitoring/by-period", method = RequestMethod.GET)
	public String byPeriod() {
		return "admin/monitoring/by-period";
	}

	@RequestMapping(value = "/springrest/admin/monitoring/help-history", method = RequestMethod.GET)
	public String helpHistory(
			@RequestParam(value="target", required=false) String target,
			Model model) throws Exception {
		
		if(isDebugEnabled){
			
			logger.debug("=================================");
			logger.debug("target : " + target);
			logger.debug("=================================");
			
		}
		
		model.addAttribute("helpHistories", callHistoryService.selectCallHistoryList());
		return "admin/monitoring/help-history";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/blind-people", method = RequestMethod.GET)
	public String blindPeople(@ModelAttribute("userVO") UserVO userVO, Model model) throws Exception {
		model.addAttribute("userVO", userVO);
		model.addAttribute("users", userService.selectUserList());
		return "admin/enrollment/blind-people";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/add-blind-people", method = RequestMethod.POST)
	public String addBlindPeople(@CookieValue("token") String token, @ModelAttribute("userVO") UserVO userVO, RedirectAttributes redirectAttributes) throws Exception {
		try {
			userVO.setAdminFk(tokenManager.getPhoneByToken(token));
			userVO.setCreatedDate(new Date());
			userVO.setUserStatus(2);

			userService.insertUser(userVO);
		} catch(Exception e) {
			redirectAttributes.addFlashAttribute("userVO", userVO);
		}

		return "redirect:/springrest/admin/enrollment/blind-people.html";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/search-blind-people")
	public String searchBlindPeople(String key, String value, int page, Model model) throws Exception {
		model.addAttribute("users", userService.findUsers(key, value, page));
		return "admin/enrollment/search-blind-people";
	}
	

	@RequestMapping(value = "/springrest/admin/enrollment/blind-people-detail")
	public String blindPeopleDetail(String id, Model model) throws Exception {
		UserVO userVO = new UserVO();
		userVO.setIdx(id);
		userVO = userService.getUser(userVO);
		model.addAttribute("user", userVO);
		model.addAttribute("helpHistories", callHistoryService.findCallHistoryByUser(userVO));
		return "admin/enrollment/blind-people-detail";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/update-blind-people", method = RequestMethod.POST)
	public String updateBlindPeople(@CookieValue("token") String token, @ModelAttribute("user") UserVO userVO, HttpServletRequest request, SessionStatus sessionStatus) throws Exception {
		try {
			userVO.setAdminFk(tokenManager.getPhoneByToken(token));
			userService.updateUser(userVO);
			sessionStatus.setComplete();
		} catch(Exception e) {
			return "redirect:" + request.getHeader("referer");
		}

		return "redirect:/springrest/admin/enrollment/blind-people.html";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/volunteer", method = RequestMethod.GET)
	public String volunteer(@ModelAttribute("helperVO") HelperVO helperVO, Model model) throws Exception {
		model.addAttribute("helperVO", helperVO);
		model.addAttribute("volunteers", helperService.selectHelperList());
		return "admin/enrollment/volunteer";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/add-volunteer", method = RequestMethod.POST)
	public String addVolunteer(@CookieValue("token") String token, @ModelAttribute("helperVO") HelperVO helperVO, RedirectAttributes redirectAttributes) throws Exception {
		try {
			helperVO.setAdminFk(tokenManager.getPhoneByToken(token));
			helperVO.setCreatedDate(new Date());
			helperVO.setHelperStatus(2);

			helperService.insertHelper(helperVO);
		} catch(Exception e) {
			redirectAttributes.addFlashAttribute("helperVO", helperVO);
		}

		return "redirect:/springrest/admin/enrollment/volunteer.html";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/volunteer-detail")
	public String volunteerDetail(String id, Model model) throws Exception {
		HelperVO helperVO = new HelperVO();
		helperVO.setIdx(id);
		helperVO = helperService.getHelper(helperVO);
		model.addAttribute("volunteer", helperVO);
		model.addAttribute("helpHistories", callHistoryService.findCallHistoryByHelper(helperVO));
		return "admin/enrollment/volunteer-detail";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/update-volunteer", method = RequestMethod.POST)
	public String updateVolunteer(@CookieValue("token") String token, @ModelAttribute("volunteer") HelperVO helperVO, HttpServletRequest request, SessionStatus sessionStatus) throws Exception {
		try {
			helperVO.setAdminFk(tokenManager.getPhoneByToken(token));
			helperService.updateHelper(helperVO);
			sessionStatus.setComplete();
		} catch(Exception e) {
			return "redirect:" + request.getHeader("referer");
		}

		return "redirect:/springrest/admin/enrollment/volunteer.html";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/counselor", method = RequestMethod.GET)
	public String counselor(@ModelAttribute("helperVO") HelperVO helperVO, Model model) throws Exception {
		model.addAttribute("helperVO", helperVO);
		model.addAttribute("counselors", helperService.selectCounselorList());
		return "admin/enrollment/counselor";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/add-counselor", method = RequestMethod.POST)
	public String addCounselor(@CookieValue("token") String token, @ModelAttribute("helperVO") HelperVO helperVO, RedirectAttributes redirectAttributes) throws Exception {
		try {
			helperVO.setAdminFk(tokenManager.getPhoneByToken(token));
			helperVO.setCreatedDate(new Date());
			helperVO.setHelperStatus(2);

			helperService.insertHelper(helperVO);
		} catch(Exception e) {
			redirectAttributes.addFlashAttribute("helperVO", helperVO);
		}

		return "redirect:/springrest/admin/enrollment/counselor.html";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/counselor-detail")
	public String counselorDetail(String id, Model model) throws Exception {
		HelperVO helperVO = new HelperVO();
		helperVO.setIdx(id);
		helperVO = helperService.getHelper(helperVO);
		model.addAttribute("counselor", helperVO);
		model.addAttribute("helpHistories", callHistoryService.findCallHistoryByHelper(helperVO));
		return "admin/enrollment/counselor-detail";
	}

	@RequestMapping(value = "/springrest/admin/enrollment/update-counselor", method = RequestMethod.POST)
	public String updateCounselor(@CookieValue("token") String token, @ModelAttribute("counselor") HelperVO helperVO, HttpServletRequest request, SessionStatus sessionStatus) throws Exception {
		try {
			helperVO.setAdminFk(tokenManager.getPhoneByToken(token));
			helperService.updateHelper(helperVO);
			sessionStatus.setComplete();
		} catch(Exception e) {
			return "redirect:" + request.getHeader("referer");
		}

		return "redirect:/springrest/admin/enrollment/counselor.html";
	}

	@RequestMapping(value = "/springrest/admin/management/rule", method = RequestMethod.GET)
	public String rule(Model model) throws Exception {
		model.addAttribute("systemList", systemService.selectSystemList());
		return "admin/management/rule";
	}

	@RequestMapping(value = "/springrest/admin/management/access-history", method = RequestMethod.GET)
	public String accessHistory(Model model) throws Exception {
		model.addAttribute("accessHistories", accessHistoryService.selectAccessHistoryList());
		return "admin/management/access-history";
	}
}
