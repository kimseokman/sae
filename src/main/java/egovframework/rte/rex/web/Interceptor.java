package egovframework.rte.rex.web;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import egovframework.rte.rex.comm.SaeConfig;
import egovframework.rte.rex.comm.TokenManager;

public class Interceptor extends HandlerInterceptorAdapter {
	
	/** logger */
	private Logger logger = Logger.getLogger(Interceptor.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();

	/** common */
	public TokenManager tokenManager = new TokenManager();

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
		if (request.getRequestURI().startsWith("/springrest/admin/login.html",
				request.getContextPath().length())) {
			return true;
		} 

		if (request.getRequestURI().startsWith("/springrest/admin/access.html",
				request.getContextPath().length())) {
			return true;
		}

		Cookie[] cookies = request.getCookies();
		String token = null;
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("token")) {
				token = cookie.getValue();
				break;
			}
		}

		if (token != null) {
			try {
				
				boolean isExpreid = tokenManager.expirationToken(token);
				boolean isVerify = tokenManager.verifierToken(token, SaeConfig.TOKEN_SECRET_KEY);
				boolean isPass = tokenManager.isValidToken(isExpreid, isVerify);
				
				if (isPass) {
					throw new Exception();
				}
				
			} catch (Exception e) {
				
				Cookie cookie = new Cookie("token", "");
				cookie.setMaxAge(0);
				response.addCookie(cookie);
				response.sendRedirect(request.getContextPath()
						+ "/springrest/admin/login.html");
				return false;
			}

			return true;
		}

		response.sendRedirect(request.getContextPath()
				+ "/springrest/admin/login.html");
		return false;
	}
}
