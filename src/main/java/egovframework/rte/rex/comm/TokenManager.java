package egovframework.rte.rex.comm;


import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

/**
 * Token에 관한 유틸 클래스를 정의한다.
 * @see
 * 151021 | KSM | Create
 */

public class TokenManager {
	
	private Logger logger = Logger.getLogger(TokenManager.class);
	private boolean isDebugEnabled = logger.isDebugEnabled();
	
	private String token;
	
	public String createToken(String phone, String site_name, byte[] sharedSecret) {
		
		// Create HMAC signer
		JWSSigner signer = new MACSigner(sharedSecret);
		
		// Prepare JWT with claims set
		JWTClaimsSet claimsSet = new JWTClaimsSet();
		claimsSet.setSubject(phone);
		claimsSet.setIssuer(site_name);
		claimsSet.setExpirationTime(new Date(new Date().getTime() + 86400 * 1000));
		
		SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);
        
        try {
            signedJWT.sign(signer);
            token = signedJWT.serialize();
            return token;
        } catch(JOSEException e) {
            e.printStackTrace();
            return null;
        }
        
	}
	
	public String getPhoneByToken(String token) throws ParseException{
		
		SignedJWT signedJWT = SignedJWT.parse(token);
		
		return signedJWT.getJWTClaimsSet().getSubject();
	}
	
	public String getSiteNameByToken(String token) throws ParseException{
		SignedJWT signedJWT = SignedJWT.parse(token);
		
		return signedJWT.getJWTClaimsSet().getIssuer();
	}
	
	public boolean expirationToken(String token) throws ParseException{		
		SignedJWT signedJWT = SignedJWT.parse(token);

		ReadOnlyJWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();
		if (new Date().after(claimsSet.getExpirationTime()))
			return true;

		return false;
	}
	
	public boolean verifierToken(String token, byte[] sharedSecret) throws JOSEException, ParseException{
		SignedJWT signedJWT = SignedJWT.parse(token);
		
		JWSVerifier verifier = new MACVerifier(sharedSecret);
		
		if(isDebugEnabled){
			logger.debug("=============================================================");
			logger.debug("[ TOKEN VALID INFO ]");
			logger.debug("phone : " + signedJWT.getJWTClaimsSet().getSubject());
			logger.debug("site_name : " + signedJWT.getJWTClaimsSet().getIssuer());
			logger.debug("expire : " + signedJWT.getJWTClaimsSet().getExpirationTime());
			logger.debug("valid result : " + signedJWT.verify(verifier));
			logger.debug("=============================================================");
		}
		
		//valid token
		if(signedJWT.verify(verifier))
			return true;
		return false;
	}
	
	public boolean isValidToken(boolean isExpreid, boolean isVerify){
		
		if(isExpreid || !isVerify)
			return false;
		
		return true;
	}
}