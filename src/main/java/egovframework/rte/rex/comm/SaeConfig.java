package egovframework.rte.rex.comm;

/**
 * Config에 관한 유틸 클래스를 정의한다.
 * @see
 * 151021 | KSM | Create
 */

public class SaeConfig {
	
	/**
	 * base url
	 */
	public static final String SAE_SERVER_NAME = "http://192.168.1.139:8080/sae/springrest/";
	
	// Generate 256-bit (32-byte) shared secret
	public static final byte[] TOKEN_SECRET_KEY = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	// call history row limit
	public static final int HISTORY_LIMIT = 20;
	
	public static final int SMS_CODE_LENGTH = 6;
	
	// rank
	public static final int RANK_1 = 1;						
	public static final int RANK_2 = 2;						
	public static final int RANK_3 = 3;						
	public static final int RANK_4 = 4;
	public static final String RANK_NAME_DEFAULT = "초보"+"도우미";
	public static final String RANK_1_NAME = "초보"+"도우미";						
	public static final String RANK_2_NAME = "숙련"+"도우미";						
	public static final String RANK_3_NAME = "마스터"+"도우미";						
	public static final String RANK_4_NAME = "VIP"+"도우미";						
	
	// match rule
	public static final int RULE_RANDOM = 0;						// 사용 안함 (랜덤)
	public static final int RULE_TODAY_CONN_NUM_MIN_HELPER = 1;		// 오늘 상담 수 적은 도우미
	public static final int RULE_TODAY_CONN_NUM_MAX_HELPER = 2;		// 오늘 상담 수 많은 도우미
	public static final int RULE_TODAY_CONN_TIME_MIN_HELPER = 3;	// 오늘 상담 시간 적은 도우미
	public static final int RULE_TODAY_CONN_TIME_MAX_HELPER = 4;	// 오늘 상담 시간 많은 도우미
	public static final int RULE_TODAY_WAIT_TIME_MAX_HELPER = 5;	// 오늘 대기 시간 많은 도우미
	public static final int RULE_TODAY_LIKE_MAX_HELPER = 6;			// 오늘 좋아요 많은 도우미
	public static final int RULE_TOTAL_CONN_NUM_MIN_HELPER = 7;		// 전체 상담 수 적은 도우미
	public static final int RULE_TOTAL_CONN_NUM_MAX_HELPER = 8;		// 전체 상담 수 많은 도우미
	public static final int RULE_TOTAL_CONN_TIME_MIN_HELPER = 9;	// 전체 상담 시간 적은 도우미
	public static final int RULE_TOTAL_CONN_TIME_MAX_HELPER = 10;	// 전체 상담 시간 많은 도우미
	public static final int RULE_TOTAL_WAIT_TIME_MAX_HELPER = 11;	// 전체 대기 시간 많은 도우미
	public static final int RULE_TOTAL_LIKE_MAX_HELPER = 12;		// 전체 좋아요 많은 도우미
	
}