package egovframework.rte.rex.comm;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.Duration;

public class Util {
	public static String getTimeDiff(Date from, Date to) {
		if (from == null || to == null) {
			return "";
		}

		Duration duration = new Duration(from.getTime(), to.getTime());
		return String.format("%d분 %d초", duration.getStandardMinutes(), duration.getStandardSeconds());  
	}
	
	public static Map ConverObjectToMap(Object obj){
		try {
			//Field[] fields = obj.getClass().getFields(); //private field는 나오지 않음.
			Field[] fields = obj.getClass().getDeclaredFields();
			Map resultMap = new HashMap();
			for(int i=0; i<=fields.length-1;i++){
				fields[i].setAccessible(true);
				resultMap.put(fields[i].getName(), fields[i].get(obj));
			}
			return resultMap;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
}
