<%@ tag pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="styles" fragment="true" %>
<%@ attribute name="scripts" fragment="true" %>
<%@ attribute name="header" fragment="true" %>
<%@ attribute name="sidebar" fragment="true" %>
<%@ attribute name="footer" fragment="true" %>

<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <title>엔젤아이즈</title>
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/reset.css' />">
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/main.css' />">
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/component.css' />">
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/color-set.css' />">  
    <jsp:invoke fragment="styles" />
  </head>
  <body>
    <div id="header">
      <jsp:invoke fragment="header" />
    </div>
    <div id="sidebar">
      <jsp:invoke fragment="sidebar" />
    </div>
    <div id="body">
      <jsp:doBody />
    </div>
    <div id="footer">
      <jsp:invoke fragment="footer" />
    </div>
    <script type="text/javascript" src="<c:url value='/node_modules/jquery/dist/jquery.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/frame.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/main.js' />"></script>
    <jsp:invoke fragment="scripts" />
  </body>
</html>
