<%@ tag pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="styles" fragment="true" %>
<%@ attribute name="scripts" fragment="true" %>

<t:admin>
  <jsp:attribute name="styles">
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/enrollment.css' />">
    <jsp:invoke fragment="styles" />
  </jsp:attribute>

  <jsp:attribute name="scripts">
    <jsp:invoke fragment="scripts" />
  </jsp:attribute>

  <jsp:attribute name="sidebar">
    <ul>
      <li><a href="<c:url value='/springrest/admin/enrollment/blind-people.html' />">시각장애인 관리</a></li>
      <li><a href="<c:url value='/springrest/admin/enrollment/volunteer.html' />">자원봉사자 관리</a></li>
      <li><a href="<c:url value='/springrest/admin/enrollment/counselor.html' />">상담원 관리</a></li>
    </ul>
  </jsp:attribute>

  <jsp:body>
    <jsp:doBody />
  </jsp:body>
</t:admin>
