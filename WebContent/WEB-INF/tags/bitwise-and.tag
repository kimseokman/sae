<%@ tag pageEncoding="utf-8" %>
<%@ attribute name="test" %>
<%@ attribute name="value" %>
<%@ attribute name="trueValue" %>
<%@ attribute name="falseValue" %>

<%
	if ((Integer.parseInt(value) & Integer.parseInt(test)) == Integer.parseInt(test)) {
		out.print(trueValue);
	} else {
		out.print(falseValue);
	}
%>
