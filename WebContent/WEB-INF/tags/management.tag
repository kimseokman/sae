<%@ tag pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="styles" fragment="true" %>
<%@ attribute name="scripts" fragment="true" %>

<t:admin>
  <jsp:attribute name="styles">
    <jsp:invoke fragment="styles" />
  </jsp:attribute>

  <jsp:attribute name="scripts">
    <jsp:invoke fragment="scripts" />
  </jsp:attribute>

  <jsp:attribute name="sidebar">
    <ul>
      <li><a href="<c:url value='/springrest/admin/management/rule.html' />">매칭률/등급 관리</a></li>
      <li><a href="<c:url value='/springrest/admin/management/access-history.html' />">접속이력</a></li>
    </ul>
  </jsp:attribute>

  <jsp:body>
    <jsp:doBody />
  </jsp:body>
</t:admin>
