<%@ tag pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="styles" fragment="true" %>

<t:admin>
  <jsp:attribute name="styles">
    <jsp:invoke fragment="styles" />
  </jsp:attribute>

  <jsp:attribute name="sidebar">
    <ul>
      <li><a href="<c:url value='/springrest/admin/monitoring/today.html' />">서비스 모니터링</a></li>
      <li><a href="<c:url value='/springrest/admin/monitoring/by-period.html' />">기간별 통계</a></li>
      <li><a href="<c:url value='/springrest/admin/monitoring/help-history.html' />">도움이력</a></li>
    </ul>
  </jsp:attribute>

  <jsp:body>
    <jsp:doBody />
  </jsp:body>
</t:admin>
