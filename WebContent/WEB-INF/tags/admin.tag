<%@ tag pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="styles" fragment="true" %>
<%@ attribute name="scripts" fragment="true" %>
<%@ attribute name="sidebar" fragment="true" %>

<t:genericpage>
  <jsp:attribute name="styles">
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/admin.css' />">
    <jsp:invoke fragment="styles" />
  </jsp:attribute>

  <jsp:attribute name="scripts">
  	<script type="text/javascript" src="<c:url value='/scripts/admin.js' />"></script>
    <jsp:invoke fragment="scripts" />
  </jsp:attribute>

  <jsp:attribute name="header">
  <div id="header-inner">
    <form style="display: none;" action="<c:url value='/springrest/admin/logout.html' />" method="post"></form>
    <div id="brand"></div>
    <ul>
      <li><a href="<c:url value='/springrest/admin/monitoring/today.html' />">모니터링/통계</a></li>
      <li><a href="<c:url value='/springrest/admin/enrollment/blind-people.html' />">승인/등록</a></li>
      <li><a href="<c:url value='/springrest/admin/management/rule.html' />">관리</a></li>      
    </ul>
    <div id="logout"><a href="#">로그아웃</a></div>
    </div>
  </jsp:attribute>

  <jsp:attribute name="sidebar">
    <jsp:invoke fragment="sidebar" />
  </jsp:attribute>

  <jsp:body>
    <jsp:doBody />
  </jsp:body>
</t:genericpage>
