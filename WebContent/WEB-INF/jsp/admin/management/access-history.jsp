<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:management>
  <jsp:body>
  	<div class="panel-heading-wrapper">
	    <div class="panel-heading">
	      <span class="panel-title">접속이력</span>
	    </div>
    </div>
    <div class="panel-body">
      <div class="row">
        <table class="table table-big">
          <thead>
            <tr class="table-heading background-5">
              <th colspan="7">
                <span class="table-title">관리자 접속이력</span>
                <span class="table-right">
                  <t:search-form>access-history.jsp</t:search-form>
                </span>
              </th>
            </tr>
            <tr>
              <th>번호</th>
              <th>아이디</th>
              <th>IP Address</th>
              <th>O/S</th>
              <th>Browser</th>
              <th>시간</th>
              <th>유형</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="accessHistory" items="${accessHistories}" varStatus="status">
            	<tr>
              		<td><c:out value="${status.count}" /></td>
              		<td><c:out value="${accessHistory.adminId}" /></td>
              		<td><c:out value="${accessHistory.ipAddress}" /></td>
              		<td><c:out value="${accessHistory.osVersion}" /></td>
              		<td><c:out value="${accessHistory.browserVersion}" /></td>
          	  		<td><fmt:formatDate value="${accessHistory.accessTime}" pattern="yyyy.MM.dd." /></td>
          	  		<td><c:out value="${accessHistory.accessType}" /></td>
            	</tr>
          	</c:forEach>
          </tbody>
        </table>
        <div class="row">
	        <div class="center pagination-wrapper">
	          <ul class="pagination">
	            <li>
	              <a href="#" aria-label="Previous">
	                <span aria-hidden="true">«</span>
	              </a>
	            </li>
	            <li><a href="#">1</a></li>
	            <li><a href="#">2</a></li>
	            <li><a href="#">3</a></li>
	            <li><a href="#">4</a></li>
	            <li><a href="#">5</a></li>
	            <li>
	              <a href="#" aria-label="Next">
	                <span aria-hidden="true">»</span>
	              </a>
	            </li>
	          </ul>
	       </div>
        </div>
      </div>
    </div>
  </jsp:body>
</t:management>
