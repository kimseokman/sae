<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:management>
  <jsp:attribute name="styles">
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/rule.css' />">
  </jsp:attribute>

  <jsp:attribute name="scripts">
    <script type="text/javascript" src="<c:url value='/scripts/rule.js' />"></script>
  </jsp:attribute>

  <jsp:body>
  	<div class="panel-heading-wrapper">
	    <div class="panel-heading">
	      <span class="panel-title">매칭룰/등급관리</span>
	    </div>
	</div>
    <div class="panel-body">
      <div class="row">

          <table class="table table-small table-fixed-height">
            <thead>
              <tr class="table-heading background-5">
                <th colspan="2">
                  <span class="table-title">매칭룰 설정</span>
                </th>
              </tr>
            </thead>
            <tbody>
            
            
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 1}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">오늘 상담 수가 적은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 2}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">오늘 상담 수가 많은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 3}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">오늘 상담 시간이 적은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 4}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">오늘 상담 시간이 많은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 5}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">오늘 대기 시간이 긴 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 6}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">오늘 좋아요가 많은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 7}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">전체 상담 수가 적은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 8}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">전체 상담 수가 많은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 9}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">전체 상담 시간이 적은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 10}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">전체 상담 시간이 많은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 11}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">전체 대기 시간이 긴 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 12}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">전체 좋아요가 많은 자원봉사자 우선</td>
              </tr>
              <tr class="radio <c:choose><c:when test="${systemList[0].routingRule == 0}"> selected</c:when></c:choose>">
                <td class="radio-button"><img src="<c:url value='/images/on.png' />" class="on"><img src="<c:url value='/images/off.png' />" class="off"></td>
                <td class="radio-label">사용 안 함(랜덤)</td>
              </tr>
            </tbody>
          </table>

          <table class="table table-small table-fixed-height">
            <thead>
              <tr class="table-heading background-4">
                <th colspan="3">
                  <span class="table-title">등급 설정</span>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr><td colspan="3"></td></tr>
              <tr style="vertical-align: middle;">
                <td>초보도우미</td>
                <td><input type="text" placeholder="도움횟수" style="width: 100px;" value="${systemList[0].beginnerConnNum}"/></td>
                <td><input type="text" placeholder="좋아요 수" style="width: 100px;" value="${systemList[0].beginnerLikeNum}" /></td>
              </tr>
              <tr style="vertical-align: middle;">
                <td>숙련도우미</td>
                <td><input type="text" placeholder="도움횟수" style="width: 100px;" value="${systemList[0].expertConnNum}" /></td>
                <td><input type="text" placeholder="좋아요 수" style="width: 100px;" value="${systemList[0].expertLikeNum}" /></td>
              </tr>
              <tr style="vertical-align: middle;">
                <td>마스터도우미</td>
                <td><input type="text" placeholder="도움횟수" style="width: 100px;" value="${systemList[0].masterConnNum}" /></td>
                <td><input type="text" placeholder="좋아요 수" style="width: 100px;" value="${systemList[0].masterLikeNum}" /></td>
              </tr>
              <tr style="vertical-align: middle;">
                <td>VIP도우미</td>
                <td><input type="text" placeholder="도움횟수" style="width: 100px;" value="${systemList[0].vipConnNum}" /></td>
                <td><input type="text" placeholder="좋아요 수" style="width: 100px;" value="${systemList[0].vipLikeNum}" /></td>
              </tr>
              <tr><td colspan="3"></td></tr>
              <tr><td colspan="3"></td></tr>
              <tr><td colspan="3"></td></tr>
              <tr><td colspan="3"></td></tr>
              <tr><td colspan="3"></td></tr>
              <tr><td colspan="3"></td></tr>
            </tbody>
          </table>
        
          <table class="table table-small table-fixed-height">
            <thead>
              <tr class="table-heading background-3">
                <th colspan="2">
                  <span class="table-title">매칭 자원봉사자 수</span>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr><td colspan="2"></td></tr>
              <tr style="vertical-align: middle;">
                <td>선호하는 자원봉사자</td>
                <td><input type="text" placeholder="N명" style="width: 100px;" value="${systemList[0].likeHelperNum}" /></td>
              </tr>
              <tr style="vertical-align: middle;">
                <td>자원봉사자</td>
                <td><input type="text" placeholder="N명" style="width: 100px;" value="${systemList[0].helperNum}" /></td>
              </tr>
              <tr style="vertical-align: middle;">
                <td>상담원</td>
                <td><input type="text" placeholder="N명" style="width: 100px;" value="${systemList[0].agentNum}" /></td>
              </tr>
              <tr><td colspan="2"></td></tr>
              <tr><td colspan="2"></td></tr>
              <tr><td colspan="2"></td></tr>
              <tr><td colspan="2"></td></tr>
              <tr><td colspan="2"></td></tr>
              <tr><td colspan="2"></td></tr>
              <tr><td colspan="2"></td></tr>
            </tbody>
          </table>
        
      </div>
    </div>
  </jsp:body>
</t:management>
