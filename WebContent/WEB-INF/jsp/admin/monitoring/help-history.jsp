<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%
//String test = request.getParameter("target");
//out.print(test);
%>
<t:monitoring>
  <jsp:body>
    <div class="panel-heading-wrapper">
      <div class="panel-heading">
        <span class="panel-title">도움이력</span>
        <span class="panel-right">
          <t:date-form>help-history.jsp</t:date-form>
        </span>
      </div>
    </div>
    <div class="panel-body">
      <div class="row">
        <table class="table table-big">
          <thead>
            <tr class="table-heading background-5">
              <th colspan="10">
                <span class="table-title">도움요청리스트</span>
                <span class="table-subtitle">${helpHistories.size()} 건 연결</span>
                <span class="table-right">
                  <t:search-form>help-history.jsp</t:search-form>
                </span>
              </th>
            </tr>
            <tr>
              <th>번호</th>
              <th>시각장애인</th>
              <th>도우미</th>
              <th>요청시간</th>
              <th>시작시간</th>
              <th>종료시간</th>
              <th>상담시간</th>
              <th>상담유형</th>
              <th>지원유형</th>
              <th>만족도</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="helpHistory" items="${helpHistories}" varStatus="status">
              <tr>
                <td><c:out value="${status.count}" /></td>
                <td><c:out value="${helpHistory.user.name}" /></td>
          	    <td><c:out value="${helpHistory.helper.name}" /></td>
          	    <td><fmt:formatDate value="${helpHistory.requestTime}" pattern="yyyy.MM.dd. HH:mm:ss" /></td>
          	    <td><fmt:formatDate value="${helpHistory.beginTime}" pattern="yyyy.MM.dd. HH:mm:ss" /></td>
          	    <td><fmt:formatDate value="${helpHistory.endTime}" pattern="yyyy.MM.dd. HH:mm:ss" /></td>
          	    <td><t:time-diff from="${helpHistory.beginTime}" to="${helpHistory.endTime}" /></td>
          	    <td><c:choose><c:when test="${helpHistory.status == 1}">연결요청중</c:when><c:when test="${helpHistory.status == 2}">연결중</c:when>
          	    <c:when test="${helpHistory.status == 3}">정상종료</c:when><c:when test="${helpHistory.status == 4}">비정상종료</c:when>
          	    <c:when test="${helpHistory.status == 5}">거절</c:when><c:when test="${helpHistory.status == 6}">요청자취소</c:when>
          	    <c:when test="${helpHistory.status == 7}">상대방없음</c:when></c:choose></td>
          	    <td>
          	      <img src="<c:url value="/images/" />icon_voice_<t:bitwise-and test="1" value="${helpHistory.supportType}" trueValue="on" falseValue="off" />.png">
          	      <img src="<c:url value="/images/" />icon_video_<t:bitwise-and test="2" value="${helpHistory.supportType}" trueValue="on" falseValue="off" />.png">
          	      <img src="<c:url value="/images/" />icon_gps_<t:bitwise-and test="4" value="${helpHistory.supportType}" trueValue="on" falseValue="off" />.png">
          	    </td>
          	    <td><c:if test="${helpHistory.likeType}"><img src="<c:url value="/images/icon_like.png" />"></c:if></td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </jsp:body>
</t:monitoring>
