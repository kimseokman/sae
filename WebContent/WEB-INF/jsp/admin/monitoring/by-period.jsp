<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:monitoring>
  <jsp:attribute name="styles">
    <link rel="stylesheet" type="text/css" href="/styles/by-period.css">
  </jsp:attribute>

  <jsp:body>
    <div class="panel-heading-wrapper">
      <div class="panel-heading">
        <span class="panel-title">기간별 통계</span>
        <span class="panel-right">
          <t:date-form>by-period.jsp</t:date-form>
        </span>
      </div>
    </div>    
    <div class="panel-body">
      <div class="row">
        <div class="chart">
          <span class="chart-title border-1">성공율</span>
          <div class="gauge-chart"></div>
        </div>
        <div class="chart">
          <span class="chart-title border-2">만족율</span>
          <div class="gauge-chart"></div>
        </div>
        <div class="chart">
          <div class="circle-chart"></div>
        </div>
        <div class="chart">
          <div class="circle-chart"></div>
        </div>
      </div>
      <div class="row">
        <div class="chart">
          <div class="chart-title-by-period">일자별 통계</div>
          <div class="bar-chart"></div>
        </div>
      </div>
    </div>
  </jsp:body>
</t:monitoring>
