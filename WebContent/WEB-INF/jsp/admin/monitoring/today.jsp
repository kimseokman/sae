<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<jsp:useBean id="now" class="java.util.Date" />

<t:monitoring>
  <div class="panel-heading-wrapper">
    <div class="panel-heading">
      <span class="panel-title">금일 통계</span>
      <span class="panel-subtitle"><fmt:formatDate value="${now}" pattern="yyyy.MM.dd" /></span>
    </div>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="chart">
        <span class="chart-title border-1">성공율</span>
        <span class="chart-subtitle">도움 요청 <span class="strong">${askingCall.size()}건</span> 중 도움 성공 <span class="strong">${successCall.size()}건</span></span>
        <span class="chart-subtitle">성공율 <span class="strong">${Math.round(successCall.size()/askingCall.size()*100)}%</span></span>
        <div class="gauge-chart"></div>
      </div>
      <div class="chart">
        <span class="chart-title border-2">만족율</span>
        <span class="chart-subtitle">도움 성공 <span class="strong">${success.size()}건</span> 중 도움 만족 <span class="strong">${satisfaction.size()}건</span></span>
        <span class="chart-subtitle">만족율 <span class="strong">${Math.round(satisfaction.size()/success.size()*100)}%</span></span>
        <div class="gauge-chart"></div>
      </div>
      <div class="chart">
        <div class="circle-chart">${meanAskingTime}초</div>
      </div>
      <div class="chart">
        <div class="circle-chart">${meanHelpingTime}초</div>
      </div>
    </div>
    <div class="row">
      <table class="table table-small">
        <thead>
          <tr class="table-heading background-3">
            <th colspan="4">
              <span class="table-title">대기 중인 자원봉사자</span>
              <span class="table-right">${waitingHelpers.size()} / ${waitingHelpers.size()}</span>
            </th>
          </tr>
          <tr>
            <th>번호</th>
            <th>닉네임</th>
            <th>이름</th>
            <th>등급</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="helper" items="${waitingHelpers}" varStatus="status">
            <tr>
              <td><c:out value="${status.count}" /></td>
              <td><c:out value="${helper.nickname}" /></td>
          	  <td><c:out value="${helper.name}" /></td>
          	  <td>
          	  <c:choose>
          	  	<c:when test="${helper.rank == 1}">초보</c:when>
          	  	<c:when test="${helper.rank == 2}">숙련</c:when>
          	  	<c:when test="${helper.rank == 3}">마스터</c:when>
          	  	<c:when test="${helper.rank == 4}">VIP</c:when>
          	  </c:choose>
          	  </td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
      <table class="table table-small">
        <thead>
          <tr class="table-heading background-4">
            <th colspan="3">
              <span class="table-title">도움 요청 중인 시각장애인</span>
              <span class="table-right">${askingUsers.size()} / ${users.size()}</span>
            </th>
          </tr>
          <tr>
            <th>번호</th>
            <th>이름</th>
            <th>그룹</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="askingUser" items="${askingUsers}" varStatus="status">
            <tr>
              <td><c:out value="${status.count}" /></td>
              <td><c:out value="${askingUser.user.name}" /></td>
              <td><c:out value="${askingUser.user.userGroup}" /></td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
      <table class="table table-small">
        <thead>
          <tr class="table-heading background-5">
            <th colspan="4">
              <span class="table-title">연결 세션</span>
              <span class="table-right">${connectSession.size()}건</span>
            </th>
          </tr>
          <tr>
            <th>번호</th>
            <th>장애인</th>
            <th>도우미</th>
            <th>연결시간</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="connectSession" items="${connectSession}" varStatus="status">
            <tr>
              <td><c:out value="${status.count}" /></td>
          	  <td><c:out value="${connectSession.user.name}" /></td>
          	  <td><c:out value="${connectSession.helper.name}" /></td>
          	  <td><fmt:formatDate value="${connectSession.beginTime}" pattern="yyyy.MM.dd. HH:mm:ss" /></td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>
  </div>
</t:monitoring>
