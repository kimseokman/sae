<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
  <jsp:attribute name="styles">
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/login.css' />">
  </jsp:attribute>

  <jsp:body>
    <div class="background" full-height="1">
      <form action="<c:url value='/springrest/admin/access.html' />" method="post" horizontal-center="1" vertical-center="1">
        <div id="logo"></div>
        <input id="id" name="login_id" type="text" placeholder="아이디" />
        <input id="password" name="login_pw" type="password" placeholder="패스워드" />
        <input id="ok" type="submit" value="로그인" />
        <a href="mailto:jss@koino.net" id="missed-password">비밀번호를 잊어버리셨나요?</a>
      </form>
    </div>
  </jsp:body>
</t:genericpage>
