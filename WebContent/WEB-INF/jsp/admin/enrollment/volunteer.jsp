<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:enrollment>
  <jsp:attribute name="styles">
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/volunteer.css' />">
  </jsp:attribute>

  <jsp:attribute name="scripts">
    <script type="text/javascript" src="<c:url value='/scripts/volunteer.js' />"></script>
  </jsp:attribute>

  <jsp:body>
	  <div class="panel-heading-wrapper">
	    <div class="panel-heading">
	      <span class="panel-title">자원봉사자 관리</span>
	      <span class="panel-right"><button class="excel-download"></button></span>
	    </div>
	  </div>
    <div class="panel-body">
      <div class="row">
        <form:form method="POST" action="add-volunteer.html" modelAttribute="helperVO" class="multiline-search-form">
          <div class="col">
            <div class="row basic">
              <div class="row">
                <form:label path="name" class="label">이름</form:label>
                <form:input path="name" />
              </div>
              <div class="row">
                <form:label path="helperPassword" class="label">비밀번호</form:label>
                <form:input path="helperPassword" />
              </div>
            </div>
          </div>
          <div class="col">
            <div class="row basic">
              <div class="row">
                <form:label path="nickname" class="label">닉네임</form:label>
                <form:input path="nickname" />
              </div>
              <div class="row">
                <form:label path="phone" class="label">전화번호</form:label>
                <form:input path="phone" type="phone" />
              </div>
            </div>
          </div>
          <div class="col">
            <div class="row basic">
              <div class="row">
                <form:label path="email" class="label">이메일</form:label>
                <form:input path="email" type="email" />
              </div>
              <div class="row">
                <form:label path="registerStatus">승인상태</form:label>
                <form:select path="registerStatus">
                  <form:option value="true" label="승인" />
                  <form:option value="false" label="미승인" />
                </form:select>
              </div>
            </div>
          </div>
          <div class="button-container">
            <div class="row">
              <button onClick="javascript: submit();">등록</button>
            </div>
          </div>
        </form:form>
      </div>
      <div class="row">
        <table class="table table-big">
          <thead>
            <tr class="table-heading background-3">
              <th colspan="10">
                <span class="table-title">자원봉사자 리스트</span>
                <span class="table-right"><t:search-form>blind-people.jsp</t:search-form></span>
              </th>
            </tr>
            <tr>
              <th>번호</th>
              <th>이름</th>
              <th>닉네임</th>
              <th>전화번호</th>
              <th>연결 수</th>
              <th>좋아요 수</th>
              <th>등록일</th>
              <th>상태</th>
              <th>최종접속시간</th>
              <th>승인상태</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="volunteer" items="${volunteers}" varStatus="status">
              <tr data-key="${volunteer.idx}">
                <td><c:out value="${status.count}" /></td>
                <td><c:out value="${volunteer.name}" /></td>
                <td><c:out value="${volunteer.nickname}" /></td>
                <td><c:out value="${volunteer.phone}" /></td>
                <td><c:out value="${volunteer.connNum}" /></td>
                <td><c:out value="${volunteer.likeNum}" /></td>
                <td><fmt:formatDate value="${volunteer.createdDate}" pattern="yyyy.MM.dd." /></td>
                <td><c:choose><c:when test="${volunteer.helperStatus == 1}">로그인</c:when><c:when test="${volunteer.helperStatus == 2}">로그아웃</c:when><c:when test="${volunteer.helperStatus == 3}">자리비움</c:when></c:choose></td>
                <td><fmt:formatDate value="${volunteer.lastLoginDate}" pattern="yyyy.MM.dd. hh:mm:ss" /></td>
                <td><c:choose><c:when test="${volunteer.registerStatus}">승인</c:when><c:otherwise>미승인</c:otherwise></c:choose></td>
              </tr>
          	</c:forEach>
          </tbody>
        </table>
        <div class="row">
	        <div class="center pagination-wrapper">
	          <ul class="pagination">
	            <li>
	              <a href="#" aria-label="Previous">
	                <span aria-hidden="true">«</span>
	              </a>
	            </li>
	            <li><a href="#">1</a></li>
	            <li><a href="#">2</a></li>
	            <li><a href="#">3</a></li>
	            <li><a href="#">4</a></li>
	            <li><a href="#">5</a></li>
	            <li>
	              <a href="#" aria-label="Next">
	                <span aria-hidden="true">»</span>
	              </a>
	            </li>
	          </ul>
	        </div>
	      </div>
      </div>
    </div>
  </jsp:body>
</t:enrollment>
