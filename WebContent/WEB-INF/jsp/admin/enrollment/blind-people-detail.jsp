<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:enrollment>
  <jsp:attribute name="scripts">
    <script type="text/javascript" src="<c:url value='/scripts/blind-people.js' />"></script>
  </jsp:attribute>
  
  <jsp:body>
  	<div class="panel-heading-wrapper">
	    <div class="panel-heading">
	      <span class="panel-title"><c:out value="${user.name}" />님 상세</span>
	      <span class="panel-right"><button class="excel-download"></button></span>
	    </div>
	  </div>
    <div class="panel-body">
      <div class="row">
        <form:form method="POST" action="update-blind-people.html" modelAttribute="user" class="multiline-search-form">
          <div class="col">
            <div class="row basic">
              <div class="row">
                <form:label path="name" class="label">이름</form:label>
                <form:input path="name" />
              </div>
              <div class="row">
                <form:label path="sex" class="label">성별</form:label>
                                           남자: <form:radiobutton path="sex" value="true" /> 여자: <form:radiobutton path="sex" value="false" />
              </div>
            </div>
            <div class="row advanced">
              <form:label path="disabledNumber" class="label">장애인등록번호</form:label>
              <form:input path="disabledNumber" />
            </div>
          </div>
          <div class="col">
            <div class="row basic">
              <div class="row">
                <form:label path="nickname" class="label">닉네임</form:label>
                <form:input path="nickname" />
              </div>            
              <div class="row">
                <form:label path="phone" class="label">전화번호</form:label>
                <form:input path="phone" type="tel" />
              </div>
            </div>
            <div class="row advanced">
              <form:label path="deviceSN" class="label">웨어러블 S/N</form:label>
              <form:input path="deviceSN" />
            </div>
          </div>
          <div class="col col-padding">
            <div class="row basic">
              <div class="row">
                <form:label path="userGroup" class="label">그룹</form:label>
                <form:input path="userGroup" />
              </div>
            </div>
            <div class="row advanced">
            </div>
          </div>
        </form:form>
      </div>
      
      <div class="row">
        <div class="section table-big center">
          <button class="big-button background-black" onClick="javascript: $('form').submit();">수정</button>
          <button class="big-button background-6" onClick="javascript: history.back();">취소</button>
        </div>
      </div>
      <div class="row">
        <table class="table table-big">
          <thead>
            <tr class="table-heading background-4">
              <th colspan="9">
                <span class="table-title">도움이력</span>
                <span class="table-right">${helpHistories.size()} 건</span>
              </th>
            </tr>
            <tr>
              <th>번호</th>
              <th>도우미</th>
              <th>요청시간</th>
              <th>시작시간</th>
              <th>종료시간</th>
              <th>상담시간</th>
              <th>상담유형</th>
              <th>지원유형</th>
              <th>좋아요</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="helpHistory" items="${helpHistories}" varStatus="status">
              <tr>
                <td><c:out value="${status.count}" /></td>
          	    <td><c:out value="${helpHistory.helper.name}" /></td>
          	    <td><fmt:formatDate value="${helpHistory.requestTime}" pattern="yyyy.MM.dd. HH:mm:ss" /></td>
          	    <td><fmt:formatDate value="${helpHistory.beginTime}" pattern="yyyy.MM.dd. HH:mm:ss" /></td>
          	    <td><fmt:formatDate value="${helpHistory.endTime}" pattern="yyyy.MM.dd. HH:mm:ss" /></td>
          	    <td><t:time-diff from="${helpHistory.beginTime}" to="${helpHistory.endTime}" /></td>
          	    <td><c:choose><c:when test="${helpHistory.status == 1}">연결요청중</c:when><c:when test="${helpHistory.status == 2}">연결중</c:when>
          	    <c:when test="${helpHistory.status == 3}">정상종료</c:when><c:when test="${helpHistory.status == 4}">비정상종료</c:when>
          	    <c:when test="${helpHistory.status == 5}">거절</c:when><c:when test="${helpHistory.status == 6}">요청자취소</c:when>
          	    <c:when test="${helpHistory.status == 7}">상대방없음</c:when></c:choose></td>
          	    <td>
          	      <img src="<c:url value="/images/" />icon_voice_<t:bitwise-and test="1" value="${helpHistory.supportType}" trueValue="on" falseValue="off" />.png">
          	      <img src="<c:url value="/images/" />icon_video_<t:bitwise-and test="2" value="${helpHistory.supportType}" trueValue="on" falseValue="off" />.png">
          	      <img src="<c:url value="/images/" />icon_gps_<t:bitwise-and test="4" value="${helpHistory.supportType}" trueValue="on" falseValue="off" />.png">
          	    </td>
          	    <td><c:if test="${helpHistory.likeType}"><img src="<c:url value="/images/icon_like.png" />"></c:if></td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </jsp:body>
</t:enrollment>
