<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:enrollment>
  <jsp:attribute name="scripts">
    <script type="text/javascript" src="<c:url value='/scripts/blind-people.js' />"></script>
  </jsp:attribute>

  <jsp:body>
  	<div class="panel-heading-wrapper">
	    <div class="panel-heading">
	      <span class="panel-title">시각장애인 관리</span>
	      <span class="panel-right"><button class="excel-download"></button></span>
	    </div>
	</div>
    <div class="panel-body">
      <div class="row">
        <form:form method="POST" action="add-blind-people.html" modelAttribute="userVO" class="multiline-search-form">
          <div class="col">
            <div class="row basic">
              <div class="row">
                <form:label path="name" class="label">이름</form:label>
                <form:input path="name" />
              </div>
               <div class="row">
                <form:label path="sex" class="label">성별</form:label>
                                           남자: <form:radiobutton path="sex" value="true" /> 여자: <form:radiobutton path="sex" value="false" />
              </div>
            </div>
            <div class="row advanced">
              <form:label path="disabledNumber" class="label">장애인등록번호</form:label>
              <form:input path="disabledNumber" />
            </div>
          </div>
          <div class="col">
            <div class="row basic">
              <div class="row">
                <form:label path="nickname" class="label">닉네임</form:label>
                <form:input path="nickname" />
              </div>
              <div class="row">
                <form:label path="phone" class="label">전화번호</form:label>
                <form:input path="phone" type="tel" />
              </div>
            </div>
            <div class="row advanced">
              <form:label path="deviceSN" class="label">웨어러블 S/N</form:label>
              <form:input path="deviceSN" />
            </div>            
          </div>
          <div class="col">
            <div class="row basic">
              <div class="row">
                <form:label path="userGroup" class="label">그룹</form:label>
                <form:input path="userGroup" />
              </div>
            </div>
            <div class="row advanced">
            </div>
            
          </div>
          <div class="button-container">
            <div class="row">
              <button onClick="javascript: submit();">등록</button>
            </div>
          </div>
        </form:form>
      </div>
      <div class="row">
        <table class="table table-big blind-people-list">
          <thead>
            <tr class="table-heading background-4">
              <th colspan="11">
                <span class="table-title">시각장애인 리스트</span>
                <span class="table-right">
                	<form class="search-form" action="search-blind-people.html">
  						<select name="key">
    						<option value="name">이름</option>
    						<option value="nickname">닉네임</option>
    						<option value="phone">전화번호</option>
    						<option value="disabled_num">장애인 등록번호</option>
    						<option value="device_sn">웨어러블장비 S/N</option>
  						</select>
  						<input type="text" name="value" />
  						<input type="submit" class="search background-black" value="검색" />
					</form>
                </span>
              </th>
            </tr>
            <tr>
              <th>번호</th>
              <th>그룹</th>
              <th>이름</th>
              <th>성별</th>
              <th>닉네임</th>
              <th>전화번호</th>
              <th>장애인등록번호</th>
              <th>웨어러블 S/N</th>
              <th>등록일</th>
              <th>상태</th>
              <th>최종접속시간</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="user" items="${users}" varStatus="status">
              <tr data-key="${user.idx}">
                <td><c:out value="${status.count}" /></td>
          	    <td><c:out value="${user.userGroup}" /></td>
          	    <td><c:out value="${user.name}" /></td>
          	    <td><c:choose><c:when test="${user.sex}">남자</c:when><c:otherwise>여자</c:otherwise></c:choose></td>
          	    <td><c:out value="${user.nickname}" /></td>
          	    <td><fmt:formatNumber value="${user.phone}" pattern="000,0000,0000" /></td>
          	    <td><c:out value="${user.disabledNumber}" /></td>
          	    <td><c:out value="${user.deviceSN}" /></td>
          	    <td><fmt:formatDate value="${user.createdDate}" pattern="yyyy.MM.dd." /></td>
          	    <td><c:choose><c:when test="${user.userStatus == 1}">로그인</c:when><c:when test="${user.userStatus == 2}">로그아웃</c:when><c:when test="${user.userStatus == 3}">자리비움</c:when><c:when test="${user.userStatus == 4}">요청중</c:when><c:when test="${user.userStatus == 5}">도움중</c:when></c:choose></td>
          	    <td><fmt:formatDate value="${user.lastLoginDate}" pattern="yyyy.MM.dd." /></td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
       </div>
       <div class="row">
        <div class="center pagination-wrapper">
          <ul class="pagination">
            <li>
              <a href="#" aria-label="Previous">
                <span aria-hidden="true">«</span>
              </a>
            </li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
              <a href="#" aria-label="Next">
                <span aria-hidden="true">»</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </jsp:body>
</t:enrollment>
