<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach var="user" items="${users}" varStatus="status">
	<tr data-key="${user.idx}">
		<td><c:out value="${status.count}" /></td>
		<td><c:out value="${user.userGroup}" /></td>
		<td><c:out value="${user.name}" /></td>
		<td><c:choose><c:when test="${user.sex}">남자</c:when><c:otherwise>여자</c:otherwise></c:choose></td>
		<td><c:out value="${user.nickname}" /></td>
		<td><fmt:formatNumber value="${user.phone}" pattern="000,0000,0000" /></td>
		<td><c:out value="${user.disabledNumber}" /></td>
		<td><c:out value="${user.deviceSN}" /></td>
		<td><fmt:formatDate value="${user.createdDate}" pattern="yyyy.MM.dd." /></td>
		<td><c:choose><c:when test="${user.userStatus == 1}">로그인</c:when><c:when test="${user.userStatus == 2}">로그아웃</c:when><c:when test="${user.userStatus == 3}">자리비움</c:when><c:when test="${user.userStatus == 4}">요청중</c:when><c:when test="${user.userStatus == 5}">도움중</c:when></c:choose></td>
		<td><fmt:formatDate value="${user.lastLoginDate}" pattern="yyyy.MM.dd." /></td>
	</tr>
</c:forEach>
