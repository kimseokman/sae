$('#logout a').click(function (e) {
	$('form').submit();
	return false;
});



$(function() {
	
	//header menu active
	var arrMonitoring = ["today", "by-period", "help-history"];
	var arrEnrollment = ["blind-people", "volunteer", "counselor", "blind-people-detail", "volunteer-detail", "counselor-detail"];
	var arrManagement = ["rule", "access-history"];
	
	var lastSlashIndex = window.location.href.lastIndexOf("/")+1;
	var lastDotIndex = window.location.href.lastIndexOf(".");
	
    var pgurl = window.location.href.substring(lastSlashIndex, lastDotIndex);

    if(arrMonitoring.indexOf(pgurl) != -1){
    	$("#header > div > ul > li:nth-child(1) > a").addClass("active");
    }
    if(arrEnrollment.indexOf(pgurl) != -1){
    	$("#header > div > ul > li:nth-child(2) > a").addClass("active");
    }
    if(arrManagement.indexOf(pgurl) != -1){
    	$("#header > div > ul > li:nth-child(3) > a").addClass("active");
    }
    
    //sidebar menu active    
	var sub1 = ["today", "blind-people", "blind-people-detail", "rule"];
	var sub2 = ["by-period", "volunteer", "volunteer-detail", "access-history"];
	var sub3 = ["counselor", "counselor-detail", "help-history"];
	
	lastSlashIndex = window.location.href.lastIndexOf("/")+1;
	lastDotIndex = window.location.href.lastIndexOf(".");
	
	pgurl = window.location.href.substring(lastSlashIndex, lastDotIndex);
	
    if(sub1.indexOf(pgurl) != -1){
    	$("#sidebar > ul > li:nth-child(1) > a").addClass("active");
    }
    if(sub2.indexOf(pgurl) != -1){
    	$("#sidebar > ul > li:nth-child(2) > a").addClass("active");
    }
    if(sub3.indexOf(pgurl) != -1){
    	$("#sidebar > ul > li:nth-child(3) > a").addClass("active");
    }    
    
});