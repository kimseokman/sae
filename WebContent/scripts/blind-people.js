$('tbody tr').on('click', function (event) {
  location.href = 'blind-people-detail.html?id=' + $(this).data('key');
});

$('.search-form').submit(function () {
	var data = $(this).serialize();
	data += '&page=1';

	$.ajax({
		url: $(this).prop('action'),
		data: data
	}).done(function (result) {
		console.log(result);
		$('.blind-people-list tbody').html(result);
	});

	return false;
});
