'use strict';

$(window).resize(function () {
  makeFullHeight();
  reframeCenter();
});

$(document).ready(function () {
  makeFullHeight();
  reframeCenter();
});
