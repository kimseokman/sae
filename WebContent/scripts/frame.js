'use strict';

function makeFullHeight() {
  var fullHeights = $('[full-height="1"]').toArray();
  var windowHeight = window.innerHeight + 'px';

  fullHeights.map(function (fullHeight) {
    fullHeight.style.height = windowHeight;
  });
}

function reframeCenter() {
  var centers = [];
  $.merge(centers, $('[horizontal-center="1"]').toArray());
  $.merge(centers, $('[vertical-center="1"]').toArray());
  $.unique(centers);

  centers.map(function (center) {
    var position = $(center).css('position');
    if (position != 'absolute' && position != 'fixed') {
      $(center).css('position', 'absolute');
    }

    var parentSize = {
      height: center.offsetParent.clientHeight,
      width: center.offsetParent.clientWidth
    }

    var isHorizontalCenter = center.hasAttribute('horizontal-center');
    if (isHorizontalCenter) {
      center.style.top = (parentSize.height - center.offsetHeight) / 2 + 'px';
    }

    var isVerticalCenter = center.hasAttribute('vertical-center');
    if (isVerticalCenter) {
      center.style.left = (parentSize.width - center.offsetWidth) / 2 + 'px';
    }
  });
}
