$('tr.radio').on('click', function (event) {
  if ($(this).hasClass('selected')) {
    return;
  }

  $('tr.selected').removeClass('selected');
  $(this).addClass('selected');
  return false;
});
