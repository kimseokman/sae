USE angeleyes;

SET SQL_SAFE_UPDATES = 0;

select * from t_ids;
update t_ids set next_id = 1 where table_name = 't_callhistory';
update t_ids set next_id = 28 where table_name = 't_helpers';
update t_ids set next_id = 1 where table_name = 't_usernhelper';
update t_ids set next_id = 1 where table_name = 't_usernhelper_like';
update t_ids set next_id = 9 where table_name = 't_users';
insert into t_ids (table_name, next_id) values ('t_usernhelper_like', 0);
delete from t_ids where table_name='t_tokens';

select * from t_system;
insert into t_system (idx, sip_ip, sip_port) values ('SYS-00000000001', '114.108.164.46', 8000);
delete from t_system;

select * from t_users;
delete from t_users;
insert into t_users (idx, created, name, nickname, phone, sex, user_status, register_status, admin_fk) values ('USER-00000000001', now(), '김효철', '김효철장애인', '01079428591', 1, 2, false, 'ADMIN-00000000001');
insert into t_users (idx, created, name, nickname, phone, sex, user_status, register_status, admin_fk) values ('USER-00000000002', now(), '박종혁', '박종혁장애인', '01081810931', 1, 2, false, 'ADMIN-00000000001');
insert into t_users (idx, created, name, nickname, phone, sex, user_status, register_status, admin_fk) values ('USER-00000000003', now(), '장석순', '장석순장애인', '01042650922', 1, 2, false, 'ADMIN-00000000001');
insert into t_users (idx, created, name, nickname, phone, sex, user_status, register_status, admin_fk) values ('USER-00000000004', now(), '김석만', '김석만장애인', '01064773706', 1, 2, false, 'ADMIN-00000000001');
insert into t_users (idx, created, name, nickname, phone, sex, user_status, register_status, admin_fk) values ('USER-00000000005', now(), '이성찬', '이성찬장애인', '01047540194', 1, 2, false, 'ADMIN-00000000001');
insert into t_users (idx, created, name, nickname, phone, sex, user_status, register_status, admin_fk) values ('USER-00000000006', now(), '윤준열', '윤준열장애인', '01027774950', 1, 2, false, 'ADMIN-00000000001');
insert into t_users (idx, created, name, nickname, phone, sex, user_status, register_status, admin_fk) values ('USER-00000000007', now(), '박석출', '박석출장애인', '01085708949', 1, 2, false, 'ADMIN-00000000001');
insert into t_users (idx, created, name, nickname, phone, sex, user_status, register_status, admin_fk) values ('USER-00000000008', now(), '박종수', '박종수장애인', '01052440857', 1, 2, false, 'ADMIN-00000000001');

select * from t_helpers;
delete from t_helpers;
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000001', now(), '김효철', '01079428592', '1234', 2, '김효철도우미1', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000002', now(), '김효철', '01079428593', '1234', 2, '김효철도우미2', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000003', now(), '김효철', '01079428594', '1234', 2, '김효철도우미3', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000004', now(), '박종혁', '01081810932', '1234', 2, '박종혁도우미1', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000005', now(), '박종혁', '01081810933', '1234', 2, '박종혁도우미2', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000006', now(), '박종혁', '01081810934', '1234', 2, '박종혁도우미3', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000007', now(), '장석순', '01042650923', '1234', 2, '장석순도우미1', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000008', now(), '장석순', '01042650924', '1234', 2, '장석순도우미2', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000009', now(), '장석순', '01042650925', '1234', 2, '장석순도우미3', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000010', now(), '김석만', '01064773707', '1234', 2, '김석만도우미1', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000011', now(), '김석만', '01064773708', '1234', 2, '김석만도우미2', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000012', now(), '김석만', '01064773709', '1234', 2, '김석만도우미3', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000013', now(), '이성찬', '01047540195', '1234', 2, '이성찬도우미1', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000014', now(), '이성찬', '01047540196', '1234', 2, '이성찬도우미2', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000015', now(), '이성찬', '01047540197', '1234', 2, '이성찬도우미3', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000016', now(), '윤준열', '01027774951', '1234', 2, '윤준열도우미1', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000017', now(), '윤준열', '01027774952', '1234', 2, '윤준열도우미2', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000018', now(), '윤준열', '01027774953', '1234', 2, '윤준열도우미3', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000019', now(), '박석출', '01085708950', '1234', 2, '박석출도우미1', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000020', now(), '박석출', '01085708951', '1234', 2, '박석출도우미2', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000021', now(), '박석출', '01085708952', '1234', 2, '박석출도우미3', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000022', now(), '박종수', '01052440858', '1234', 2, '박종수도우미1', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000023', now(), '박종수', '01052440859', '1234', 2, '박종수도우미2', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000024', now(), '박종수', '01052440860', '1234', 2, '박종수도우미3', 2, true, 0, 0, 1, 'ADMIN-00000000001');

insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000025', now(), '김태희', '01012342345', '1234', 3, '김태희상담원', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000026', now(), '하지원', '01023453456', '1234', 3, '하지원상담원', 2, true, 0, 0, 1, 'ADMIN-00000000001');
insert into t_helpers (idx, created, name, phone, helper_pw, helper_type, nickname, helper_status, register_status, like_num, conn_num, rank, admin_fk) values ('HELPER-00000000027', now(), '수지', '01034564567', '1234', 3, '수지상담원', 2, true, 0, 0, 1, 'ADMIN-00000000001');

select * from t_usernhelper_like;
delete from t_usernhelper_like;

select * from t_usernhelper;
delete from t_usernhelper;

select * from t_callhistory;
select * from t_callhistory where user_id = 'USER-00000000003' and request_time <> 0;
delete from t_callhistory;

select * from t_helpers;
select * from t_helpers as h where h.helper_type <> 3 AND h.helper_status = 2;
select * from t_helpers as h where h.helper_type <> 3 AND h.helper_status = 2 AND h.idx NOT IN ( SELECT l.helper_fk FROM t_usernhelper_like as l WHERE l.user_fk = 'USER-00000000004' ) ORDER BY h.conn_num DESC;
select * from t_helpers as h where h.helper_type <> 3 AND h.helper_status = 2 AND h.idx IN ( SELECT c.helper_id FROM t_callhistory as c WHERE c.begin_time >= '1446786400029' AND c.end_time < '446872800029' );
select * from t_helpers as h where h.helper_type <> 3 AND h.helper_status = 2 AND h.idx IN ( SELECT c.helper_id FROM t_callhistory as c WHERE c.begin_time >= '1446786400029' AND c.end_time < '1446872800029' ) AND h.idx NOT IN ( SELECT l.helper_fk FROM t_usernhelper_like as l WHERE l.user_fk = 'USER-00000000022' );
select * from t_helpers as h, t_callhistory as c where h.idx = c.helper_id and h.idx not in ( SELECT l.helper_fk FROM t_usernhelper_like as l WHERE l.user_fk = 'USER-00000000004' ) AND h.helper_type <> 3 AND h.helper_status = 2 AND c.begin_time >= '2015-11-13 00:00:00' AND c.end_time < '2015-11-14 00:00:00';
select * from t_helpers as h, t_callhistory as c where h.idx = c.helper_id and h.idx not in ( SELECT l.helper_fk FROM t_usernhelper_like as l WHERE l.user_fk = 'USER-00000000004' ) AND h.helper_type <> 3 AND h.helper_status = 2 AND c.begin_time >= '2015-11-13 00:00:00' AND c.end_time < '2015-11-14 00:00:00' GROUP BY h.idx ORDER BY COUNT(h.idx) asc;
select * from t_helpers as h, t_callhistory as c where h.idx = c.helper_id and h.idx not in ( SELECT l.helper_fk FROM t_usernhelper_like as l WHERE l.user_fk = 'USER-00000000004' ) AND h.helper_type <> 3 AND h.helper_status = 2 AND c.status = 3 AND c.begin_time >= '2015-11-13 00:00:00' AND c.end_time < '2015-11-14 00:00:00' GROUP BY h.idx ORDER BY SUM(TIMEDIFF(c.end_time, c.begin_time)) asc;

insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000001', 'USER-00000000004', 'HELPER-00000000010', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000002', 'USER-00000000004', 'HELPER-00000000010', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000003', 'USER-00000000004', 'HELPER-00000000010', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000004', 'USER-00000000004', 'HELPER-00000000010', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000005', 'USER-00000000004', 'HELPER-00000000010', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000006', 'USER-00000000004', 'HELPER-00000000011', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000007', 'USER-00000000004', 'HELPER-00000000011', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000008', 'USER-00000000004', 'HELPER-00000000011', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000009', 'USER-00000000004', 'HELPER-00000000011', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000010', 'USER-00000000004', 'HELPER-00000000012', now(), now(), now(), 3, 0, 3);
insert into t_callhistory (idx, user_id, helper_id, request_time, begin_time, end_time, support_type, like_type, status) values ('CALL-HISTORY-00000000011', 'USER-00000000004', 'HELPER-00000000012', now(), now(), now(), 3, 0, 3);

update t_helpers set register_status = true;
update t_users set register_status = true;

desc t_registered;
select * from t_registered;
select idx, helper_status from t_helpers;
select idx, user_status from t_users;
select idx, helper_status from t_helpers where idx='HELPER-00000000001';
insert into t_registered (skey, urlalias) values (0, 'sip:USER-00000000001');
insert into t_registered (skey, urlalias) values (1, 'sip:HELPER-00000000001');
delete from t_registered;
delete from t_registered where skey = 2;

DROP TRIGGER IF EXISTS `insert_sip`;

delimiter //
CREATE TRIGGER insert_sip AFTER INSERT ON t_registered
FOR EACH ROW
BEGIN
    SET @LABEL = (SELECT SUBSTRING_INDEX(NEW.urlalias, 'sip:', -1));
    
	IF @LABEL like '%HELPER%' THEN
		UPDATE t_helpers set helper_status=1 where idx=@LABEL;
	ELSE
		UPDATE t_users set user_status=1 where idx=@LABEL;
    END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER delete_sip AFTER DELETE ON t_registered
FOR EACH ROW
BEGIN
    SET @LABEL = (SELECT SUBSTRING_INDEX(OLD.urlalias, 'sip:', -1));
    
	IF @LABEL like '%HELPER%' THEN
		UPDATE t_helpers set helper_status=2 where idx=@LABEL;
	ELSE
		UPDATE t_users set user_status=2 where idx=@LABEL;
    END IF;
END; //
delimiter ;

select * from t_sms;