-- MySQL dump 10.13  Distrib 5.6.27, for Win32 (x86)
--
-- Host: localhost    Database: angeleyes
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_accesshistory`
--

DROP TABLE IF EXISTS `t_accesshistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_accesshistory` (
  `idx` varchar(255) NOT NULL COMMENT '인덱스',
  `admin_id` varchar(255) DEFAULT NULL COMMENT '관리자 아이디',
  `access_type` tinyint(4) DEFAULT NULL COMMENT '접속 유형(로그인/로그아웃)',
  `access_time` datetime DEFAULT NULL COMMENT '접속 시간',
  `ip_addr` varchar(20) DEFAULT NULL COMMENT '아이피',
  `os_ver` varchar(255) DEFAULT NULL COMMENT '운영체제 버전',
  `browser_ver` varchar(255) DEFAULT NULL COMMENT '브라우저 버전',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='관리자 웹콘솔에 접속한 이력을 관리한다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_accesshistory`
--

LOCK TABLES `t_accesshistory` WRITE;
/*!40000 ALTER TABLE `t_accesshistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_accesshistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_admin`
--

DROP TABLE IF EXISTS `t_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_admin` (
  `idx` varchar(255) NOT NULL COMMENT '인덱스',
  `created` datetime DEFAULT NULL COMMENT '등록 일자',
  `lastlogin` datetime DEFAULT NULL COMMENT '최근 접속 시간',
  `admin_id` varchar(255) DEFAULT NULL COMMENT '관리자 아이디',
  `admin_pw` varchar(255) DEFAULT NULL COMMENT '관리자 비밀번호',
  `name` varchar(255) DEFAULT NULL COMMENT '관리자 이름',
  `phone` varchar(30) DEFAULT NULL COMMENT '관리자 전화번호',
  `email` varchar(255) DEFAULT NULL COMMENT '관리자 이메일',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='관리자 정보를 관리한다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_admin`
--

LOCK TABLES `t_admin` WRITE;
/*!40000 ALTER TABLE `t_admin` DISABLE KEYS */;
INSERT INTO `t_admin` VALUES ('ADMIN-00000000001',NULL,NULL,'admin','1234',NULL,NULL,NULL);
/*!40000 ALTER TABLE `t_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_callhistory`
--

DROP TABLE IF EXISTS `t_callhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_callhistory` (
  `idx` varchar(255) NOT NULL COMMENT '인덱스',
  `user_id` varchar(255) DEFAULT NULL COMMENT '시각장애인 아이디',
  `helper_id` varchar(255) DEFAULT NULL COMMENT '도우미 아이디',
  `request_time` datetime DEFAULT NULL,
  `begin_time` datetime DEFAULT NULL COMMENT '시작시간',
  `end_time` datetime DEFAULT NULL COMMENT '종료시간',
  `support_type` tinyint(4) DEFAULT NULL COMMENT '지원유형',
  `like_type` tinyint(4) DEFAULT NULL COMMENT '만족도 여부',
  `status` tinyint(4) DEFAULT NULL COMMENT '상태',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='도움 이력을 관리한다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_callhistory`
--

LOCK TABLES `t_callhistory` WRITE;
/*!40000 ALTER TABLE `t_callhistory` DISABLE KEYS */;
INSERT INTO `t_callhistory` VALUES ('CALL-HISTORY-00000000001','USER-00000000004','HELPER-00000000010','2015-11-13 11:03:43','2015-11-13 11:03:43','2015-11-13 11:03:43',3,0,3),('CALL-HISTORY-00000000002','USER-00000000004','HELPER-00000000010','2015-11-13 11:03:44','2015-11-13 11:03:44','2015-11-13 11:03:44',3,0,3),('CALL-HISTORY-00000000003','USER-00000000004','HELPER-00000000010','2015-11-13 11:03:45','2015-11-13 11:03:45','2015-11-13 11:03:45',3,0,3),('CALL-HISTORY-00000000004','USER-00000000004','HELPER-00000000010','2015-11-13 11:03:45','2015-11-13 11:03:45','2015-11-13 11:03:45',3,0,3),('CALL-HISTORY-00000000005','USER-00000000004','HELPER-00000000010','2015-11-13 11:03:46','2015-11-13 11:03:46','2015-11-13 11:03:46',3,0,3),('CALL-HISTORY-00000000006','USER-00000000004','HELPER-00000000011','2015-11-13 11:03:47','2015-11-13 11:03:47','2015-11-13 11:03:47',3,0,3),('CALL-HISTORY-00000000007','USER-00000000004','HELPER-00000000011','2015-11-13 11:03:48','2015-11-13 11:03:48','2015-11-13 11:03:48',3,0,3),('CALL-HISTORY-00000000008','USER-00000000004','HELPER-00000000011','2015-11-13 11:03:48','2015-11-13 11:03:48','2015-11-13 11:03:48',3,0,3),('CALL-HISTORY-00000000009','USER-00000000004','HELPER-00000000011','2015-11-13 11:03:49','2015-11-13 11:03:49','2015-11-13 11:03:49',3,0,3),('CALL-HISTORY-00000000010','USER-00000000004','HELPER-00000000012','2015-11-13 11:03:50','2015-11-13 11:03:50','2015-11-13 11:03:50',3,0,3),('CALL-HISTORY-00000000011','USER-00000000004','HELPER-00000000012','2015-11-13 11:03:51','2015-11-13 11:03:51','2015-11-13 11:03:51',3,0,3);
/*!40000 ALTER TABLE `t_callhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_currentcall`
--

DROP TABLE IF EXISTS `t_currentcall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_currentcall` (
  `idx` varchar(255) NOT NULL COMMENT '인덱스',
  `connected_time` datetime DEFAULT NULL COMMENT '연결 시간',
  `user_id` varchar(255) DEFAULT NULL COMMENT '시각장애인 아이디',
  `helper_id` varchar(255) DEFAULT NULL COMMENT '도우미 아이디',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='현재 연결 정보를 관리한다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_currentcall`
--

LOCK TABLES `t_currentcall` WRITE;
/*!40000 ALTER TABLE `t_currentcall` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_currentcall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_helpers`
--

DROP TABLE IF EXISTS `t_helpers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_helpers` (
  `idx` varchar(255) NOT NULL COMMENT '인덱스',
  `created` datetime DEFAULT NULL COMMENT '등록 일자',
  `lastlogin` datetime DEFAULT NULL COMMENT '최근 로그인 시간',
  `helper_id` varchar(255) DEFAULT NULL COMMENT '도우미 아이디',
  `helper_pw` varchar(255) DEFAULT NULL COMMENT '도우미 비밀번호',
  `name` varchar(255) DEFAULT NULL COMMENT '도우미 이름',
  `phone` varchar(30) DEFAULT NULL COMMENT '도우미 전화번호',
  `email` varchar(255) DEFAULT NULL COMMENT '도우미 이메일',
  `helper_type` tinyint(4) DEFAULT NULL COMMENT '도우미 타입',
  `nickname` varchar(255) DEFAULT NULL COMMENT '도우미 닉네임',
  `birth` varchar(11) DEFAULT NULL COMMENT '도우미 생년월일',
  `device_sn` varchar(30) DEFAULT NULL COMMENT '웨어러블 장비 S/N',
  `helper_group` varchar(255) DEFAULT NULL COMMENT '그룹 정보',
  `helper_status` tinyint(4) DEFAULT NULL COMMENT '상태',
  `register_status` tinyint(4) DEFAULT NULL COMMENT '승인 상태',
  `like_num` int(11) DEFAULT '0' COMMENT '좋아요 횟수',
  `conn_num` int(11) DEFAULT '0' COMMENT '도움 횟수',
  `etc_help` tinyint(4) DEFAULT '1',
  `rank` int(11) DEFAULT '1' COMMENT '등급',
  `admin_fk` varchar(255) NOT NULL COMMENT '관리자 FK',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='도우미 정보를 관리한다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_helpers`
--

LOCK TABLES `t_helpers` WRITE;
/*!40000 ALTER TABLE `t_helpers` DISABLE KEYS */;
INSERT INTO `t_helpers` VALUES ('HELPER-00000000001','2015-11-13 16:48:13',NULL,NULL,'1234','김효철','01079428592',NULL,2,'김효철도우미1',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000002','2015-11-13 16:48:14',NULL,NULL,'1234','김효철','01079428593',NULL,2,'김효철도우미2',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000003','2015-11-13 16:48:15',NULL,NULL,'1234','김효철','01079428594',NULL,2,'김효철도우미3',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000004','2015-11-13 16:48:15',NULL,NULL,'1234','박종혁','01081810932',NULL,2,'박종혁도우미1',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000005','2015-11-13 16:48:16',NULL,NULL,'1234','박종혁','01081810933',NULL,2,'박종혁도우미2',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000006','2015-11-13 16:48:16',NULL,NULL,'1234','박종혁','01081810934',NULL,2,'박종혁도우미3',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000007','2015-11-13 16:48:17',NULL,NULL,'1234','장석순','01042650923',NULL,2,'장석순도우미1',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000008','2015-11-13 16:48:18',NULL,NULL,'1234','장석순','01042650924',NULL,2,'장석순도우미2',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000009','2015-11-13 16:48:18',NULL,NULL,'1234','장석순','01042650925',NULL,2,'장석순도우미3',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000010','2015-11-13 16:48:19',NULL,NULL,'1234','김석만','01064773707',NULL,2,'김석만도우미1',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000011','2015-11-13 16:48:19',NULL,NULL,'1234','김석만','01064773708',NULL,2,'김석만도우미2',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000012','2015-11-13 16:48:20',NULL,NULL,'1234','김석만','01064773709',NULL,2,'김석만도우미3',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000013','2015-11-13 16:48:21',NULL,NULL,'1234','이성찬','01047540195',NULL,2,'이성찬도우미1',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000014','2015-11-13 16:48:21',NULL,NULL,'1234','이성찬','01047540196',NULL,2,'이성찬도우미2',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000015','2015-11-13 16:48:22',NULL,NULL,'1234','이성찬','01047540197',NULL,2,'이성찬도우미3',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000016','2015-11-13 16:48:22',NULL,NULL,'1234','윤준열','01027774951',NULL,2,'윤준열도우미1',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000017','2015-11-13 16:48:23',NULL,NULL,'1234','윤준열','01027774952',NULL,2,'윤준열도우미2',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000018','2015-11-13 16:48:24',NULL,NULL,'1234','윤준열','01027774953',NULL,2,'윤준열도우미3',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000019','2015-11-13 16:48:24',NULL,NULL,'1234','박석출','01085708950',NULL,2,'박석출도우미1',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000020','2015-11-13 16:48:25',NULL,NULL,'1234','박석출','01085708951',NULL,2,'박석출도우미2',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000021','2015-11-13 16:48:25',NULL,NULL,'1234','박석출','01085708952',NULL,2,'박석출도우미3',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000022','2015-11-13 16:48:26',NULL,NULL,'1234','박종수','01052440858',NULL,2,'박종수도우미1',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000023','2015-11-13 16:48:27',NULL,NULL,'1234','박종수','01052440859',NULL,2,'박종수도우미2',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000024','2015-11-13 16:48:27',NULL,NULL,'1234','박종수','01052440860',NULL,2,'박종수도우미3',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000025','2015-11-13 16:48:29',NULL,NULL,'1234','김태희','01012342345',NULL,3,'김태희상담원',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000026','2015-11-13 16:48:30',NULL,NULL,'1234','하지원','01023453456',NULL,3,'하지원상담원',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001'),('HELPER-00000000027','2015-11-13 16:48:30',NULL,NULL,'1234','수지','01034564567',NULL,3,'수지상담원',NULL,NULL,NULL,2,1,0,0,1,1,'ADMIN-00000000001');
/*!40000 ALTER TABLE `t_helpers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_ids`
--

DROP TABLE IF EXISTS `t_ids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_ids` (
  `table_name` varchar(255) NOT NULL COMMENT '테이블 이름',
  `next_id` decimal(30,0) NOT NULL COMMENT '다음 id',
  PRIMARY KEY (`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='id 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_ids`
--

LOCK TABLES `t_ids` WRITE;
/*!40000 ALTER TABLE `t_ids` DISABLE KEYS */;
INSERT INTO `t_ids` VALUES ('t_admin',1),('t_callhistory',1),('t_helpers',28),('t_usernhelper',1),('t_usernhelper_like',1),('t_users',9);
/*!40000 ALTER TABLE `t_ids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_registered`
--

DROP TABLE IF EXISTS `t_registered`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_registered` (
  `skey` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '인덱스',
  `namealias` varchar(255) DEFAULT NULL COMMENT '닉네임',
  `nameoriginal` varchar(255) DEFAULT NULL COMMENT '이름',
  `urlalias` varchar(255) DEFAULT NULL COMMENT 'urlalias',
  `urloriginal` varchar(255) DEFAULT NULL COMMENT 'SIP URI',
  `acceptpattern` varchar(1000) DEFAULT NULL COMMENT '수락패턴',
  `requester` varchar(100) DEFAULT NULL COMMENT '요청자 정보',
  `expires` bigint(20) DEFAULT NULL COMMENT '만료시간',
  `priority` int(11) DEFAULT NULL COMMENT '우선순위',
  `timeupdate` bigint(20) DEFAULT NULL COMMENT '로그인 시간',
  `expirestime` bigint(20) DEFAULT NULL COMMENT '만료 시간',
  `mappedport` varchar(100) DEFAULT NULL COMMENT '매핑된 포트',
  `awake` int(11) DEFAULT NULL COMMENT 'awake',
  `useragent` varchar(255) DEFAULT NULL COMMENT 'useragent',
  `param` varchar(255) DEFAULT NULL COMMENT 'param',
  PRIMARY KEY (`skey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='화상 서버에 로그인 한 유저들의 정보를 실시간으로 관리한다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_registered`
--

LOCK TABLES `t_registered` WRITE;
/*!40000 ALTER TABLE `t_registered` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_registered` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER insert_sip AFTER INSERT ON t_registered
FOR EACH ROW
BEGIN
    SET @LABEL = (SELECT SUBSTRING_INDEX(NEW.urlalias, 'sip:', -1));
    
	IF @LABEL like '%HELPER%' THEN
		UPDATE t_helpers set helper_status=1 where idx=@LABEL;
	ELSE
		UPDATE t_users set user_status=1 where idx=@LABEL;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER delete_sip AFTER DELETE ON t_registered
FOR EACH ROW
BEGIN
    SET @LABEL = (SELECT SUBSTRING_INDEX(OLD.urlalias, 'sip:', -1));
    
	IF @LABEL like '%HELPER%' THEN
		UPDATE t_helpers set helper_status=2 where idx=@LABEL;
	ELSE
		UPDATE t_users set user_status=2 where idx=@LABEL;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `t_sms`
--

DROP TABLE IF EXISTS `t_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sms` (
  `phone` varchar(255) NOT NULL COMMENT '핸드폰 번호 ( 인덱스 )',
  `created` datetime DEFAULT NULL COMMENT '생성 시간',
  `code` varchar(40) DEFAULT NULL COMMENT '인증 코드',
  PRIMARY KEY (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='인증 정보를 관리한다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sms`
--

LOCK TABLES `t_sms` WRITE;
/*!40000 ALTER TABLE `t_sms` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_sms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_system`
--

DROP TABLE IF EXISTS `t_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_system` (
  `idx` varchar(255) NOT NULL COMMENT '인덱스',
  `routing_rule` int(11) DEFAULT '0' COMMENT '매칭 룰 설정',
  `beginner_conn_num` int(11) DEFAULT '0' COMMENT '초보도우미 도움횟수 설정',
  `beginner_like_num` int(11) DEFAULT '0' COMMENT '초보도우미 좋아요 설정',
  `expert_conn_num` int(11) DEFAULT '0' COMMENT '숙련도우미 도움횟수 설정',
  `expert_like_num` int(11) DEFAULT '0' COMMENT '숙련도우미 좋아요 설정',
  `master_conn_num` int(11) DEFAULT '0' COMMENT '마스터도우미 도움횟수 설정',
  `master_like_num` int(11) DEFAULT '0' COMMENT '마스터도우미 좋아요 설정',
  `vip_conn_num` int(11) DEFAULT '0' COMMENT 'VIP도우미 도움횟수 설정',
  `vip_like_num` int(11) DEFAULT '0' COMMENT 'VIP도우미 좋아요 설정',
  `sip_ip` varchar(16) NOT NULL COMMENT 'SIP 서버 주소',
  `sip_port` int(11) NOT NULL COMMENT 'SIP 서버 포트',
  `sip_codec` varchar(50) DEFAULT 'H264' COMMENT '영상 코덱',
  `sip_framerate` int(11) DEFAULT '15' COMMENT '영상 프레임레이트',
  `sip_bitrate` int(11) DEFAULT '300' COMMENT '영상 비트레이트',
  `sip_resolution` varchar(50) DEFAULT 'CIF' COMMENT '영상 해상도',
  `like_helper_num` int(11) DEFAULT '2' COMMENT '호분배시 선호 도우미 숫자',
  `helper_num` int(11) DEFAULT '2' COMMENT '호분배 도우미 숫자',
  `agent_num` int(11) DEFAULT '2' COMMENT '호분배시 상담원 숫자',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시스템에서 사용하는 여러 설정 정보 등을 관리한다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_system`
--

LOCK TABLES `t_system` WRITE;
/*!40000 ALTER TABLE `t_system` DISABLE KEYS */;
INSERT INTO `t_system` VALUES ('SYS-00000000001',0,0,0,0,0,0,0,0,0,'114.108.164.46',8000,'H264',15,300,'CIF',2,2,2);
/*!40000 ALTER TABLE `t_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_usernhelper`
--

DROP TABLE IF EXISTS `t_usernhelper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_usernhelper` (
  `idx` varchar(255) NOT NULL COMMENT '인덱스',
  `created` datetime DEFAULT NULL COMMENT '등록 일자',
  `user_fk` varchar(255) DEFAULT NULL COMMENT '시각장애인 아이디',
  `helper_fk` varchar(255) DEFAULT NULL COMMENT '도우미 아이디',
  `request_type` varchar(45) DEFAULT NULL COMMENT '요청 보낸 타입',
  `is_family` tinyint(4) DEFAULT NULL COMMENT '가족 상태 결정',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시각장애인과 가족 관계를 관리하는 테이블이다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_usernhelper`
--

LOCK TABLES `t_usernhelper` WRITE;
/*!40000 ALTER TABLE `t_usernhelper` DISABLE KEYS */;
INSERT INTO `t_usernhelper` VALUES ('FAMILY-00000000001','2015-11-12 17:17:38','USER-00000000003','HELPER-00000000007','USER',1),('FAMILY-00000000002','2015-11-12 17:49:43','USER-00000000002','HELPER-00000000004','USER',1),('FAMILY-00000000003','2015-11-12 21:26:09','USER-00000000003','HELPER-00000000008','HELPER',1);
/*!40000 ALTER TABLE `t_usernhelper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_usernhelper_like`
--

DROP TABLE IF EXISTS `t_usernhelper_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_usernhelper_like` (
  `idx` varchar(255) NOT NULL COMMENT '등록 일자',
  `created` datetime DEFAULT NULL,
  `user_fk` varchar(255) DEFAULT NULL COMMENT '시각장애인 아이디',
  `helper_fk` varchar(255) DEFAULT NULL COMMENT '도우미 아이디',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시각장애인과 선호 도우미 관계를 관리하는 테이블이다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_usernhelper_like`
--

LOCK TABLES `t_usernhelper_like` WRITE;
/*!40000 ALTER TABLE `t_usernhelper_like` DISABLE KEYS */;
INSERT INTO `t_usernhelper_like` VALUES ('LIKE-HLEPER-00000000001','2015-11-12 17:18:16','USER-00000000003','HELPER-00000000007'),('LIKE-HLEPER-00000000002','2015-11-12 18:27:17','USER-00000000002','HELPER-00000000004'),('LIKE-HLEPER-00000000003','2015-11-12 21:27:23','USER-00000000003','HELPER-00000000008');
/*!40000 ALTER TABLE `t_usernhelper_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_users`
--

DROP TABLE IF EXISTS `t_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_users` (
  `idx` varchar(255) NOT NULL COMMENT '인덱스',
  `created` datetime DEFAULT NULL COMMENT '등록 일자',
  `lastlogin` datetime DEFAULT NULL COMMENT '최근 로그인 시간',
  `user_id` varchar(255) DEFAULT NULL COMMENT '시작장애인 아이디',
  `user_pw` varchar(255) DEFAULT NULL COMMENT '시각장애인 비밀번호',
  `name` varchar(255) DEFAULT NULL COMMENT '시각장애인 이름',
  `nickname` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL COMMENT '시각장애인 전화번호',
  `email` varchar(255) DEFAULT NULL COMMENT '시각장애인 이메일',
  `sex` tinyint(4) DEFAULT NULL COMMENT '시각장애인 성별',
  `birth` varchar(11) DEFAULT NULL COMMENT '시각장애인 생년월일',
  `disabled_num` varchar(20) DEFAULT NULL COMMENT '장애인 등록번호',
  `device_sn` varchar(30) DEFAULT NULL,
  `device_uuid` varchar(255) DEFAULT NULL COMMENT '핸드폰 uuid',
  `user_group` varchar(255) DEFAULT NULL COMMENT '그룹 정보',
  `user_status` tinyint(4) DEFAULT NULL COMMENT '상태',
  `register_status` tinyint(4) DEFAULT NULL COMMENT '기기등록 상태\n',
  `admin_fk` varchar(255) NOT NULL COMMENT '관리자 FK',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시각장애인 정보를 관리한다.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_users`
--

LOCK TABLES `t_users` WRITE;
/*!40000 ALTER TABLE `t_users` DISABLE KEYS */;
INSERT INTO `t_users` VALUES ('USER-00000000001','2015-11-13 16:48:07',NULL,NULL,NULL,'김효철','김효철장애인','01079428591',NULL,1,NULL,NULL,NULL,NULL,NULL,2,0,'ADMIN-00000000001'),('USER-00000000002','2015-11-13 16:48:07',NULL,NULL,NULL,'박종혁','박종혁장애인','01081810931',NULL,1,NULL,NULL,NULL,NULL,NULL,2,0,'ADMIN-00000000001'),('USER-00000000003','2015-11-13 16:48:08',NULL,NULL,NULL,'장석순','장석순장애인','01042650922',NULL,1,NULL,NULL,NULL,NULL,NULL,2,0,'ADMIN-00000000001'),('USER-00000000004','2015-11-13 16:48:09',NULL,NULL,NULL,'김석만','김석만장애인','01064773706',NULL,1,NULL,NULL,NULL,NULL,NULL,2,0,'ADMIN-00000000001'),('USER-00000000005','2015-11-13 16:48:10',NULL,NULL,NULL,'이성찬','이성찬장애인','01047540194',NULL,1,NULL,NULL,NULL,NULL,NULL,2,0,'ADMIN-00000000001'),('USER-00000000006','2015-11-13 16:48:10',NULL,NULL,NULL,'윤준열','윤준열장애인','01027774950',NULL,1,NULL,NULL,NULL,NULL,NULL,2,0,'ADMIN-00000000001'),('USER-00000000007','2015-11-13 16:48:11',NULL,NULL,NULL,'박석출','박석출장애인','01085708949',NULL,1,NULL,NULL,NULL,NULL,NULL,2,0,'ADMIN-00000000001'),('USER-00000000008','2015-11-13 16:48:11',NULL,NULL,NULL,'박종수','박종수장애인','01052440857',NULL,1,NULL,NULL,NULL,NULL,NULL,2,0,'ADMIN-00000000001');
/*!40000 ALTER TABLE `t_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-13 16:49:48
